import React  from "react";
import { AccountCircle } from "@mui/icons-material";
import { Tooltip } from "@mui/material";
import { useNavigate } from "react-router-dom";

import classes from "./ProfileButton.module.css";
import {useSelector} from "react-redux";
import {routeDirectory} from "../../../store/auth";

export default function ProfileButton() {
    const auth = useSelector((state: any) => state.auth);
    const navigate = useNavigate();

    const handleClick = () => {
        navigate(routeDirectory + "/");
    };

    return <Tooltip title="Профиль">
        <div className={classes.container} onClick={handleClick}>
                <AccountCircle className={classes.icon} />
                <span className={classes.phone}>{auth.userInfo?.phone}</span>
        </div>
    </Tooltip>;
}