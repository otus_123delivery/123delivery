import React, { useState } from "react";

import classes from "./PhotoUploader.module.css";
import {AttachFile} from "@mui/icons-material";
import {Tooltip} from "@mui/material";

export interface PhotoUploaderParameters {
    maxSize?: number;
    onUploaded(data: string | undefined):void;
    onError(): void;
}

export default function PhotoUploader(props: PhotoUploaderParameters) {
    const maxSize = props.maxSize === undefined ? 1024 * 1024 : props.maxSize;
    const [photo, setPhoto] = useState<string | undefined>(undefined);

    const showFile = async (e: any) => {
        e.preventDefault();
        if (e.target?.files.length !== 1) {
            return;
        }

        if (e.target.files[0].size > maxSize) {
            props.onError();
            return;
        }

        const reader = new FileReader();

        reader.onload = () =>  {
            if (!reader.result?.toString().startsWith("data:image/jpeg;base64,") && !reader.result?.toString().startsWith("data:image/png;base64,")) {
                props.onError();
                return;
            }
            const resultString = reader.result as string;
            setPhoto(resultString);

            props.onUploaded((resultString === undefined ?
                resultString
                : resultString.replace("data:image/jpeg;base64,", "")
                    .replace("data:image/png;base64,", "")));
        };

        reader.onerror = (error) => {
            props.onError();
            console.log('Error: ', error);
            setPhoto(undefined);
        }

        reader.readAsDataURL(e.target.files[0]);
    }

    return <div>
        {photo && <img className={classes.photo} src={photo} alt="Фото" />}
        <input type="file" name="file" id="file" className={classes.inputfile} onChange={showFile}/>
        <label className={classes.label} htmlFor="file">
            <Tooltip title="Выбрать фото">
                <AttachFile className={classes.uploadImage} />
            </Tooltip>
        </label>
    </div>;

}