export default function formatDate(date: Date | undefined, withSeconds?: boolean): string | undefined {
    if (!date) return undefined;

    let resultDate = new Date(date);
    console.log(resultDate);
    let result: string = `${resultDate.getDate()}/${withLeadingZero(resultDate.getMonth() + 1)}/${resultDate.getFullYear()} ${resultDate.getHours()}:${withLeadingZero(resultDate.getMinutes())}`;
    if (withSeconds) {
        result += `:${withLeadingZero(resultDate.getSeconds())}`;
    }

    return result;
}

function withLeadingZero(month: number) {
    return month > 9 ? month.toString() : "0" + month.toString();
}