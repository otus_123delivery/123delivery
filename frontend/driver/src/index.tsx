import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter} from "react-router-dom";

import './index.css';
import App from './App';
import {Provider} from "react-redux";
import {store} from "./store";
import {SnackbarProvider} from "notistack";

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <SnackbarProvider maxSnack={7}>
                <App/>
            </SnackbarProvider>
        </HashRouter>
    </Provider>,
    document.getElementById('root')
);
