import {apiGetRequest, apiPostRequest, ApiRequestResult} from "../services/api-service";
import {UserInfo} from "../shared/user-info.model";
import {createSlice} from "@reduxjs/toolkit";
import {uiManagerActions} from "./ui-manager";

export enum ShiftChangeOperation {
    Start,
    Resume,
    Finish,
    TakeBreak,
    TakeTechBreak
}

export enum ShiftState {
    NotFound = 0,
    Active = 1,
    Break = 2,
    TechBreak = 3,
    Unknown = 4
}

function mapToState(state: number): ShiftState {
    switch (state) {
        case 0:
            return ShiftState.NotFound;
        case 1:
            return ShiftState.Active;
        case 2:
            return ShiftState.Break;
        case 3:
            return ShiftState.TechBreak;
        case 4:
            return ShiftState.Unknown;
        default:
            throw new RangeError(state.toString());
    }
}

const initialState = getInitialState();

function getInitialState() {
    return {shiftState: ShiftState.Unknown}
}

export const shiftSlice = createSlice({
    name: "shift",
    initialState: initialState,
    reducers: {
        updateState(state: any, action: any) {
            state.shiftState = action.payload.shiftState;
        }
    }
});

export function updateShiftState(userInfo: UserInfo | undefined, abortSignal: AbortSignal) {
    return async (dispatch: any) => {
        try {
            const result = await apiGetRequest("/driver/shift", userInfo, abortSignal);

            if (result.status === ApiRequestResult.Success) {
                dispatch(shiftActions.updateState({shiftState: mapToState(result.data.state)}))
            } else if (result.status === ApiRequestResult.NotFound) {
                dispatch(shiftActions.updateState({shiftState: ShiftState.NotFound}));
            } else {
                dispatch(uiManagerActions.error({ message: "Не удалось обновить состояние смены."}));
            }
        } catch (error) {
            console.error(error);
            dispatch(uiManagerActions.error({ message: "Не удалось обновить состояние смены."}));
        }
    };
}

export function changeShiftState(operation: ShiftChangeOperation, userInfo: UserInfo | undefined, abortSignal: AbortSignal) {
    return async (dispatch: any) => {
        try {
            if (!userInfo) {
                return;
            }

            let apiUri;
            let resultState: ShiftState;
            switch (operation) {
                case ShiftChangeOperation.Start:
                    apiUri = "/driver/shift/start";
                    resultState = ShiftState.Active;
                    break;
                case ShiftChangeOperation.Resume:
                    apiUri = "/driver/shift/resume";
                    resultState = ShiftState.Active;
                    break;
                case ShiftChangeOperation.TakeBreak:
                    apiUri = "/driver/shift/break";
                    resultState = ShiftState.Break;
                    break;
                case ShiftChangeOperation.TakeTechBreak:
                    apiUri = "/driver/shift/tech-break";
                    resultState = ShiftState.TechBreak;
                    break;
                case ShiftChangeOperation.Finish:
                    apiUri = "/driver/shift/finish";
                    resultState = ShiftState.NotFound;
                    break;
                default:
                    throw new RangeError(operation);
            }

            const result = await apiPostRequest(apiUri, {}, userInfo, abortSignal);

            if (result.status === ApiRequestResult.Success) {
                dispatch(shiftActions.updateState({shiftState: resultState}));
            } else {
                console.log(result);
                dispatch(uiManagerActions.error({ message: "Не удалось изменить состояние смены." }));
            }
        } catch {
            dispatch(uiManagerActions.error({ message: "Не удалось изменить состояние смены." }));
        }
    };
}

export const shiftActions = shiftSlice.actions;
export const shiftReducer = shiftSlice.reducer;