import {configureStore} from "@reduxjs/toolkit";
import {authReducer} from "./auth";
import {uiManagerReducer} from "./ui-manager";
import {shiftReducer} from "./shift";
import {orderReducer} from "./order";

export const store = configureStore({
    reducer: {
        auth: authReducer,
        uiManager: uiManagerReducer,
        shift: shiftReducer,
        order: orderReducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});