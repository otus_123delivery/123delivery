import {UserInfo} from "../shared/user-info.model";
import {createSlice} from "@reduxjs/toolkit";
import {uiManagerActions} from "./ui-manager";

export const apiAddress = window.location.origin + "/api/v1";
export const routeDirectory = process.env.NODE_ENV === "production" ? "/driver" : "";

class AuthModel {
    constructor(public isLogged: boolean,
                public userInfo: UserInfo | undefined) {
    }
}

const initialState = getInitialState();

const authSlice = createSlice({
    name: "auth",
    reducers: {
        login(state: AuthModel, action: any) {
            state.isLogged = true;
            state.userInfo = action.payload;
        },
        logout(state: AuthModel) {
            state.isLogged = false;
            state.userInfo = undefined;
        }
    },
    initialState: initialState
});

function getInitialState(): AuthModel {
    const json = localStorage.getItem("driverInfo");

    if (!json) {
        return {isLogged: false, userInfo: undefined};
    }

    const userInfo = JSON.parse(json);
    if (userInfo && new Date(userInfo.expirationDate) > new Date()) {
        return { isLogged: true, userInfo: userInfo };
    } else {
        return {isLogged: false, userInfo: undefined};
    }
}

export const loginToServer = (phone: string, password: string): any => {
    return async (dispatch: any) => {
        try {
            const response = await fetch(apiAddress + "/driver/login",
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        phone: phone,
                        password: password
                    })
                });

            if (!response.ok) {
                dispatch(uiManagerActions.error({ message: "При входе произошла ошибка" }));
                return;
            }

            const data = await response.json();
            const userInfo = new UserInfo(phone, data.jwt, data.expirationDate);
            localStorage.setItem("driverInfo", JSON.stringify(userInfo));
            dispatch(authActions.login(userInfo));
        } catch {
            dispatch(uiManagerActions.error({ message: "При входе произошла ошибка" }));
        }
    };
};

export const logoutFromServer = () => {
    return (dispatch: any) => {
        localStorage.removeItem("driverInfo");
        dispatch(authActions.logout());
    };
};

export const authReducer = authSlice.reducer;
export const authActions = authSlice.actions;