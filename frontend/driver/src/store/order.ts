import {createSlice} from "@reduxjs/toolkit";
import {UserInfo} from "../shared/user-info.model";
import {apiGetRequest, apiPostRequest, ApiRequestResult} from "../services/api-service";
import {uiManagerActions} from "./ui-manager";

export enum OrderState {
    Unknown,
    NotFound,
    Picked,
    Performing,
    OnLocation
}

export enum OrderOperation {
    Pick,
    Perform,
    OnLocation,
    Cancel,
    Abandon
}

const initialState = {orderState: OrderState.Unknown, order: undefined};

const orderSlice = createSlice({
    name: "order",
    initialState: initialState,
    reducers: {
        updateOrderState(state: any, action: any) {
            state.orderState = action.payload.state;
            state.order = action.payload.order;
        }
    }
});

export const orderActions = orderSlice.actions;
export const orderReducer = orderSlice.reducer;

function convertStringToState(state: string): OrderState {
    switch (state) {
        case "Picked":
            return OrderState.Picked;
        case "Performing":
            return OrderState.Performing;
        case "OnLocation":
            return OrderState.OnLocation;
        default:
            throw new RangeError(state);
    }
}

export function updateOrderState(userInfo: UserInfo | undefined, abortSignal: AbortSignal) {
    return async (dispatch: any) => {
        try {
            const response = await apiGetRequest("/driver/orders/current", userInfo, abortSignal);
            if (response.status === ApiRequestResult.Success) {
                const order = response.data;
                dispatch(orderActions.updateOrderState({
                    state: convertStringToState(response.data.currentState),
                    order
                }));
            } else if (response.status === ApiRequestResult.NotFound) {
                dispatch(orderActions.updateOrderState({state: OrderState.NotFound, order: undefined}));
            } else {
                console.log("nf");
                dispatch(orderActions.updateOrderState({state: OrderState.Unknown, order: undefined}));
                dispatch(uiManagerActions.error({ message: "Не удалось получить состояние заказа."}));
            }
        } catch (error) {
            dispatch(uiManagerActions.error({ message: "Не удалось получить состояние заказа."}));
        }
    };
}

export function changeOrderState(userInfo: UserInfo | undefined, operation: OrderOperation, orderId: string, abortSignal: AbortSignal) {
    return async (dispatch: any) => {
        if (!userInfo) return;
        try {
            let queryAddress = "";
            let message: string;
            switch (operation) {
                case OrderOperation.Pick:
                    queryAddress = "/driver/orders/manage/take/";
                    message = "За вами успешно закреплён заказ.";
                    break;
                case OrderOperation.Perform:
                    queryAddress = "/driver/orders/manage/perform/";
                    message = "Приглашаем пассажиров.";
                    break;
                case OrderOperation.OnLocation:
                    queryAddress = "/driver/orders/manage/on-location/";
                    message = "Прибыли в пункт назначения.";
                    break;
                case OrderOperation.Cancel:
                    queryAddress = "/driver/orders/manage/cancel/";
                    message = "Заказ успешно отменён.";
                    break;
                case OrderOperation.Abandon:
                    queryAddress = "/driver/orders/manage/abandon/";
                    message = "Вы отказались от выполнения заказа.";
                    break;
                default:
                    throw new RangeError(operation);
            }

            const response = await apiPostRequest(queryAddress + orderId, {}, userInfo, abortSignal);
            if (response.status !== ApiRequestResult.Success) {
                dispatch(uiManagerActions.error({ message: "Не удалось изменить состояние заказа." }));
            }
            dispatch(uiManagerActions.success({ message }));
            dispatch(updateOrderState(userInfo, abortSignal));
        } catch (error) {
            dispatch(uiManagerActions.error({ message: "Не удалось изменить состояние заказа." }));
        }
    };
}

