import {Box, Button, Container, Paper, Step, StepLabel, Stepper, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import DriverForm, {DriverModel} from "./driver-form/DriverForm";
import VehicleForm, {VehicleFormModel} from "./vehicle-form/VehicleForm";
import {LoadingContainer} from "../../components/ui/loading-container/LoadingContainer";
import {apiAddress} from "../../store/auth";

function registerDriver(driver: DriverModel, vehicle?: VehicleFormModel): Promise<boolean> {
    let vehicleProp = {...vehicle};
    let request = {...driver, vehicle: vehicleProp === undefined ? null : vehicle };
    console.log(JSON.stringify(request));
    return new Promise<boolean>((resolve, reject) => {
        fetch(apiAddress + "/driver/register", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(request)
        })
        .then(result => {
            if (result.ok && result.status === 200) {
                resolve(true);
            } else {
                resolve(false);
            }
        })
        .catch(error => {
            reject(error);
        });
    });
}

const steps = ['Данные о водителе', 'Данные об автомобиле'];

export default function Register() {
    const [activeStep, setActiveStep] = useState(0);
    const [validationState, setValidationState] = useState(false);
    const [isVehicleSelected, setIsVehicleSelected] = useState<boolean>(false);
    const [isNextDisabled, setIsNextDisabled] = useState(false);
    const [driver, setDriver] = useState<any>();
    const [vehicle, setVehicle] = useState<any>();
    const [isProcessing, setIsProcessing] = useState<boolean>(false);
    const [isRegisterSuccess, setIsRegisterSuccess] = useState<boolean>(false);

    const handleNext = () => {
        if (activeStep === 0) {
            setActiveStep(activeStep + 1);
        } else {
            setIsProcessing(true);
            setActiveStep(activeStep + 1);
            registerDriver(driver, isVehicleSelected ? vehicle : undefined).then(result => {
                    console.log("promise result: " + result);
                    setIsRegisterSuccess(result);
                    setIsProcessing(false);
                },
                error => {
                    console.error(error);
                    setIsRegisterSuccess(false);
                    setIsProcessing(false);
                })
        }
    };

    const handleBack = () => setActiveStep(activeStep - 1);
    const driverChanged = (driver: DriverModel) => setDriver(driver);
    const vehicleChanged = (vehicle: VehicleFormModel) => setVehicle(vehicle);
    const formValidationStateChanged = (state: boolean) => setValidationState(state);
    const skipChanged = (state: boolean) => setIsVehicleSelected(state);

    const getStepContent = (step: number) => {
        switch (step) {
            case 0:
                return <DriverForm formStateChanged={formValidationStateChanged}
                                   onDriverChanged={driverChanged}/>;
            case 1:
                return <VehicleForm onSkipChanged={skipChanged}
                                    formStateChanged={formValidationStateChanged}
                                    onVehicleChanged={vehicleChanged}/>;
            default:
                throw new Error('Unknown step');
        }
    }

    useEffect(() => setIsNextDisabled(activeStep === 0 ? !validationState : (isVehicleSelected ? !validationState : false)),
        [validationState, isVehicleSelected]); // eslint-disable-line react-hooks/exhaustive-deps

    return <><Container component="main" maxWidth="sm" sx={{mb: 4}}>
        <Paper variant="outlined" sx={{my: {xs: 3, md: 6}, p: {xs: 2, md: 3}}}>
            <Typography component="h1" variant="h4" align="center">
                Регистрация
            </Typography>
            <Stepper activeStep={activeStep} sx={{pt: 3, pb: 5}}>
                {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                ))}
            </Stepper>
            <>
                {activeStep === steps.length
                    ? (<LoadingContainer isLoading={isProcessing}>
                        <Typography align="center" variant="h5"
                                    gutterBottom>{isRegisterSuccess ? "Вы зарегистрированы" : "Произошла ошибка"}</Typography>
                        <Typography align="center" variant="subtitle1">
                            {isRegisterSuccess
                                ? "Для активации вашего аккаунта требуется подтверждение. Диспетчеры рассмотрят ваш аккаунт в ближайшее время."
                                : "Не удалось выполнить регистрацию. Обратитесь в службу поддержки."}
                        </Typography>
                        {!isRegisterSuccess && <Button onClick={() => setActiveStep(0)}>Попробовать снова</Button>}
                    </LoadingContainer>)
                    : (<>
                        {getStepContent(activeStep)}
                        <Box sx={{display: 'flex', justifyContent: 'flex-end'}}>
                            {activeStep !== 0 && (
                                <Button onClick={handleBack} sx={{mt: 3, ml: 1}}>
                                    Назад
                                </Button>
                            )}
                            <Button
                                disabled={isNextDisabled}
                                variant="contained"
                                onClick={handleNext}
                                sx={{mt: 3, ml: 1}}>
                                {activeStep === steps.length - 1 ? 'Регистрация' : 'Далее'}
                            </Button>
                        </Box>
                    </>)}
            </>
        </Paper>
    </Container></>;
}