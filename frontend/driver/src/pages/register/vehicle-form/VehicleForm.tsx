import {useEffect, useState} from "react";
import {
    Box,
    Checkbox,
    FormControl,
    FormControlLabel,
    InputLabel,
    MenuItem,
    Select, SelectChangeEvent,
    TextField,
    Typography
} from "@mui/material";
import {useForm} from "react-hook-form";
import PhotoUploader from "../../../components/ui/photo-uploader/PhotoUploader";

import classes from "./VehicleForm.module.css";

type VehicleFormFields = {
    photo: string;
    brand : string;
    model: string;
    licensePlate : string;
    vehicleClass: number;
};

export class VehicleFormModel {
    constructor(public photo: string,
                public brand : string,
                public model: string,
                public licensePlate : string,
                public vehicleClass: number) { }
}

export default function VehicleForm(props:
    {
        onSkipChanged: (state: boolean) => void,
        formStateChanged: (state: boolean) => void,
        onVehicleChanged: (vehicle: VehicleFormModel) => void
    }) {

    const [notSelected, setNotSelected] = useState<boolean>(false);
    const {
        register,
        watch,
        setValue,
        trigger,
        formState: {
            touchedFields,
            errors
        },

    } = useForm<VehicleFormFields>({ mode: "onTouched", defaultValues: { vehicleClass: 1 } });

    const { photo, brand, model, licensePlate, vehicleClass } = watch();

    useEffect(() => props.onSkipChanged(!notSelected), [notSelected]); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        props.onVehicleChanged(new VehicleFormModel(photo, brand, model, licensePlate, vehicleClass));
        trigger().then(result => {
            props.formStateChanged(result);
        });
    }, [photo, model, brand, licensePlate]); // eslint-disable-line react-hooks/exhaustive-deps

    function handleUploadedPhoto(data: string | undefined) {
        if (data !== undefined) {
            setValue("photo", data);
        }
    }

    const handleUploadPhotoError = ()=> alert("Указан некорректный файл. Выберите фотографию в формате JPEG либо PNG размером не более 512 КБ");

    const handleChange = (event: SelectChangeEvent) => setValue("vehicleClass", +event.target.value);

    return <div>
        <Typography align="center" variant="h6" gutterBottom>
            Данные автомобиля
        </Typography>
        <FormControlLabel className={classes.skip} control={<Checkbox value={notSelected} onChange={() => setNotSelected(!notSelected)} />} label="Автомобиль будет назначен позже" />
        {!notSelected && <div style={{ textAlign: "center"}}>
            <form onSubmit={() => {}}>
                <div className={classes.container}>
                    <Box>
                        <PhotoUploader maxSize={1024 * 512} onUploaded={handleUploadedPhoto}
                                       onError={handleUploadPhotoError}/>
                    </Box>
                    <input type="hidden" {...register("photo", {required: true})} />
                    {!photo && <p className="bg-warning text-center m-3">Загрузите фото</p>}
                </div>
                <div className={classes.container}>
                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("brand", {required: true, minLength: 2 })}
                                error={touchedFields.brand && errors.brand != null}
                                helperText={touchedFields.brand && errors.brand && "Введите корректную марку"}
                                label="Марка"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("model", {required: true, minLength: 2})}
                                error={touchedFields.model && errors.model != null}
                                helperText={touchedFields.model && errors.model && "Введите корректную модель"}
                                label="Модель"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("licensePlate", {required: true, pattern: /^[а-яА-Я]{1}[0-9]{3}[а-яА-Я]{2}[0-9]{2,3}$/})}
                                error={touchedFields.licensePlate && errors.licensePlate != null}
                                helperText={touchedFields.licensePlate && errors.licensePlate && "Введите корректный госномер"}
                                label="Госномер"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div>
                        <FormControl sx={{ m: 1, minWidth: 230 }}>
                            <InputLabel id="demo-simple-select-helper-label">Класс автомобиля</InputLabel>
                            <Select
                                labelId="demo-simple-select-helper-label"
                                id="demo-simple-select-helper"
                                value={`${vehicleClass}`}
                                onChange={handleChange}
                                label="Класс">
                                <MenuItem value={1}>Эконом</MenuItem>
                                <MenuItem value={2}>Комфорт</MenuItem>
                                <MenuItem value={3}>Бизнес</MenuItem>
                            </Select>
                        </FormControl>
                    </div>

                </div>
            </form>
        </div>}
    </div>;
}