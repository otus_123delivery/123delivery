import classes from "./DriverForm.module.css";
import {useForm} from "react-hook-form";
import {Box, TextField} from "@mui/material";
import PhotoUploader from "../../../components/ui/photo-uploader/PhotoUploader";
import {DesktopDatePicker, LocalizationProvider} from "@mui/lab";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {useEffect} from "react";

export class DriverModel {
    constructor(
        public firstname: string,
        public lastname: string,
        public birthDate: Date,
        public phone: string,
        public password: string,
        public confirmation: string,
        public passport: string,
        public driverLicense: string,
        public photo: string) { }
}

export default function DriverForm(props: { formStateChanged: (state: boolean) => void, onDriverChanged: (driver: DriverModel) => void }) {

    const {
        register,
        watch,
        setValue,
        trigger,
        formState: {
            errors, touchedFields
        }
    } = useForm<DriverModel>({defaultValues: { birthDate: new Date("2000-01-01")}, mode: "onTouched"});

    const {phone, firstname, lastname, birthDate, password, confirmation, passport, photo, driverLicense } = watch();

    const handleUploadedPhoto = (data: string | undefined) => {
        if (data !== undefined) {
            setValue("photo", data);
        }
    }

    const handleUploadPhotoError = () => alert("Указан некорректный файл. Выберите фотографию в формате JPEG либо PNG размером не более 512 КБ");

    useEffect(() => {
        props.onDriverChanged(new DriverModel(firstname, lastname, birthDate,
            phone, password, confirmation, passport, driverLicense, photo));

        trigger().then(result => {
            props.formStateChanged(result);
        });
    }, [phone, firstname, lastname, birthDate, password, confirmation, passport, photo, driverLicense]); // eslint-disable-line react-hooks/exhaustive-deps

    return <div style={{ textAlign: "center"}}>
            <form>
                <div className={classes.container}>
                    <Box>
                        <PhotoUploader maxSize={1024 * 512} onUploaded={handleUploadedPhoto}
                                       onError={handleUploadPhotoError}/>
                    </Box>
                    <input type="hidden" {...register("photo", {required: true})} />
                    {!photo && <p className="bg-warning text-center m-3">Загрузите фото</p>}
                </div>

                <div className={classes.container}>
                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("phone", {required: true, pattern: /^\+7[0-9]{10}$/})}
                                error={touchedFields.phone && errors.phone != null}
                                helperText={touchedFields.phone && errors.phone && "Введите корректный телефон"}
                                label="Телефон"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("firstname", {
                                    required: true,
                                    pattern: {
                                        value: /^[a-zA-Zа-яА-я]{2,15}$/,
                                        message: "Некорректный формат имени. Имя может содержать только буквы."
                                    }
                                })}
                                error={touchedFields.firstname && errors.firstname != null}
                                helperText={touchedFields.firstname && errors.firstname && "Введите имя"}
                                label="Имя"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("lastname", {
                                    required: true,
                                    pattern: {
                                        value: /^[a-zA-Zа-яА-я]{2,15}$/,
                                        message: "Некорректный формат фамилии. Фамилия может содержать только буквы."
                                    }
                                })}
                                error={touchedFields.lastname && errors.lastname != null}
                                helperText={touchedFields.lastname && errors.lastname && "Введите фамилию"}
                                label="Фамилия"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group} style={{ margin: "auto", width: "230px"}}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DesktopDatePicker
                                renderInput={(props) => <TextField {...props} />}
                                label={errors.birthDate ? errors.birthDate.message : "Дата рождения"}
                                {...register("birthDate", {
                                    validate: value =>
                                        value.getFullYear() > 1920 && value.getFullYear() < 2013 || "Выбрана недопустимая дата"
                                })}
                                minDate={new Date('1920-01-01')}
                                maxDate={new Date('2013-01-01')}
                                value={birthDate}
                                onChange={(newValue: any) => {
                                    setValue("birthDate", newValue);
                                }}/>
                        </LocalizationProvider>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("passport", {required: true, pattern: /^[0-9]{4}\s?[0-9]{6}$/ })}
                                error={touchedFields.passport && errors.passport != null}
                                helperText={touchedFields.passport && errors.passport && "Введите паспортные данные"}
                                label="Паспорт РФ"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("driverLicense", {required: true, pattern: /^[0-9]{4}\s?[0-9]{6}/})}
                                error={touchedFields.driverLicense && errors.driverLicense != null}
                                helperText={touchedFields.driverLicense && errors.driverLicense && "Введите номер ВУ"}
                                label="Водительское удостоверение"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("password", {
                                    required: "Введите пароль",
                                    minLength: {
                                        value: 7,
                                        message: "Минимальная длина 7 символов"
                                    },
                                    validate: value =>
                                        value === confirmation || "Пароли не совпадают"
                                })}
                                error={touchedFields.password && errors.password != null}
                                helperText={touchedFields.password && errors.password && errors.password.message}
                                type="password"
                                label="Пароль"
                                variant="filled"/>
                        </Box>
                    </div>

                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("confirmation", {
                                    required: "Введите подтверждение пароля",
                                    minLength: {
                                        value: 7,
                                        message: "Минимальная длина 7 символов"
                                    },
                                    validate: value =>
                                        value === password || "Пароли не совпадают"
                                })}
                                error={touchedFields.confirmation && errors.confirmation != null}
                                helperText={touchedFields.confirmation && errors.confirmation && errors.confirmation.message}
                                type="password"
                                label="Подтверждение"
                                variant="filled"/>
                        </Box>
                    </div>
                </div>
            </form>
    </div>;
}