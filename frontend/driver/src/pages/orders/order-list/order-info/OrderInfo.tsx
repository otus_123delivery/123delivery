import {NavLink, useNavigate, useParams} from "react-router-dom";
import React, {useCallback, useEffect, useState} from "react";
import {ArrowBackRounded} from "@mui/icons-material";
import {
    Button,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Tooltip
} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";

import {apiGetRequest, ApiRequestResult} from "../../../../services/api-service";
import classes from "./OrderInfo.module.css";
import {LoadingContainer} from "../../../../components/ui/loading-container/LoadingContainer";
import {AppError} from "../../../../components/ui/app-error/AppError";
import {routeDirectory} from "../../../../store/auth";
import {changeOrderState, OrderOperation, OrderState} from "../../../../store/order";
import formatDate from "../../../../shared/format-date";

export interface Order {
    orderId: string;
    currentState: string;
    cost: number;
    customerCarTypePreference: number;
    fromLatitude: number;
    fromLongitude: number;
    toLatitude: number;
    toLongitude: number;
    createdDate: Date;
    pickedDate?: Date;
    performingStartedDate?: Date;
    arrivingDate?: Date;
}

export function decodeCarClass(customerCarTypePreference: number | undefined): string {
    if (!customerCarTypePreference) return "";

    switch (customerCarTypePreference) {
        case 1:
            return "Эконом";
        case 2:
            return "Люкс";
        case 3:
            return "Бизнес";
        default:
            throw new RangeError(`customerCarTypePreference: ${customerCarTypePreference}`);
    }
}

export function OrderInfo() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {id} = useParams();
    const {userInfo} = useSelector((state: any) => state.auth);
    const orderState = useSelector((state: any) => state.order.orderState);
    const [order, setOrder] = useState<Order | undefined>(undefined);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(false);
    const abortController = new AbortController();

    const callback = useCallback(async (abortSignal: AbortSignal) => {
        setIsLoading(true);
        setError(false);
        try {
            const response = await apiGetRequest("/driver/orders/" + id, userInfo, abortSignal);
            setIsLoading(false);
            if (response.status === ApiRequestResult.Success) {
                setOrder(response.data);
            } else {
                setError(true);
            }
        } catch (error) {
            setError(true);
            setIsLoading(false);
            console.error(error);
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (!userInfo) return;
        callback(abortController.signal);

        return () => abortController.abort();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (orderState === OrderState.NotFound || orderState === OrderState.Unknown) return;
        navigate(routeDirectory + "/orders");
    }, [orderState]);

    const takeOrder = () => {
        if (!userInfo || id == undefined) return;
        dispatch(changeOrderState(userInfo, OrderOperation.Pick, id, abortController.signal));
    };

    return <>
        <div style={{textAlign: "center"}}>
            <NavLink to={routeDirectory + "/orders"} role="button">
                <Tooltip title="Назад">
                    <ArrowBackRounded sx={{fontSize: "45px", marginBottom: "15px"}}/>
                </Tooltip>
            </NavLink>
            <h1 className={classes.title}>Информация о заказе</h1>
        </div>

        {error ? <AppError/>
            : <LoadingContainer isLoading={isLoading}>
                <div className={classes.tableContainer}>
                    <TableContainer component={Paper}>
                        <Table>
                            <TableBody>
                                <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component="th" scope="row">Создан</TableCell>
                                    <TableCell align="right">{formatDate(order?.createdDate)}</TableCell>
                                </TableRow>
                                <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component="th" scope="row">Откуда (широта)</TableCell>
                                    <TableCell align="right">{order?.fromLatitude}</TableCell>
                                </TableRow>
                                <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component="th" scope="row">Откуда (долгота)</TableCell>
                                    <TableCell align="right">{order?.fromLongitude}</TableCell>
                                </TableRow>
                                <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component="th" scope="row">Куда (широта)</TableCell>
                                    <TableCell align="right">{order?.toLatitude}</TableCell>
                                </TableRow>
                                <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component="th" scope="row">Куда (долгота)</TableCell>
                                    <TableCell align="right">{order?.toLongitude}</TableCell>
                                </TableRow>
                                <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell component="th" scope="row">Желаемый класс автомобиля</TableCell>
                                    <TableCell
                                        align="right">{decodeCarClass(order?.customerCarTypePreference)}</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <div className={classes.actionPanel}>
                        <Button variant="contained" onClick={takeOrder}>Взять заказ</Button>
                    </div>
                </div>
            </LoadingContainer>}
    </>;
}