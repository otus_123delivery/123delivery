import {useDispatch, useSelector} from "react-redux";
import {Button, Paper, Table, TableBody, TableCell, TableContainer, TableRow} from "@mui/material";
import React, {useEffect} from "react";

import {changeOrderState, OrderOperation, OrderState} from "../../../../store/order";
import classes from "./ManageOrder.module.css";
import {decodeCarClass} from "../order-info/OrderInfo";
import formatDate from "../../../../shared/format-date";

function getActionText(orderState: OrderState) {
    switch (orderState) {
        case OrderState.Picked:
            return "Пригласить";
        case OrderState.Performing:
            return "Завершить";
        default:
            return "";
    }
}

function decodeOrderStateToString(orderState: OrderState) {
    switch (orderState) {
        case OrderState.Performing:
            return "Выполняется";
        case OrderState.Picked:
            return "Ожидание прибытия автомобиля";
        case OrderState.OnLocation:
            return "Прибыл в место назначения";
        default:
            return "";
    }
}

export function ManageOrder() {
    const { auth: { userInfo }, order: { orderState, order} } = useSelector((state: any) => state);
    const dispatch = useDispatch();
    const abortController = new AbortController();

    function cancelHandler() {
        const operation = orderState === OrderState.Picked ? OrderOperation.Abandon : OrderOperation.Cancel;
        dispatch(changeOrderState(userInfo, operation, order.orderId, abortController.signal));
    }

    function actionHandler() {
        switch (orderState) {
            case OrderState.Performing:
                dispatch(changeOrderState(userInfo, OrderOperation.OnLocation, order.orderId, abortController.signal));
                break;
            case OrderState.Picked:
                dispatch(changeOrderState(userInfo, OrderOperation.Perform, order.orderId, abortController.signal));
                break;
        }
    }

    useEffect(() => {
        return () => {
            abortController.abort();
        };
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    return <div>
        <div className="tableContainer">
            <TableContainer component={Paper}>
                <Table>
                    <TableBody>
                        <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                            <TableCell component="th" scope="row">Создан</TableCell>
                            <TableCell align="right">{formatDate(order?.createdDate)}</TableCell>
                        </TableRow>
                        <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                            <TableCell component="th" scope="row">Состояние</TableCell>
                            <TableCell align="right">{decodeOrderStateToString(orderState)}</TableCell>
                        </TableRow>
                        <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                            <TableCell component="th" scope="row">Откуда (широта)</TableCell>
                            <TableCell align="right">{order?.fromLatitude}</TableCell>
                        </TableRow>
                        <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                            <TableCell component="th" scope="row">Откуда (долгота)</TableCell>
                            <TableCell align="right">{order?.fromLongitude}</TableCell>
                        </TableRow>
                        <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                            <TableCell component="th" scope="row">Куда (широта)</TableCell>
                            <TableCell align="right">{order?.toLatitude}</TableCell>
                        </TableRow>
                        <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                            <TableCell component="th" scope="row">Куда (долгота)</TableCell>
                            <TableCell align="right">{order?.toLongitude}</TableCell>
                        </TableRow>
                        <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                            <TableCell component="th" scope="row">Желаемый класс автомобиля</TableCell>
                            <TableCell
                                align="right">{decodeCarClass(order?.customerCarTypePreference)}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
        <div className={classes.actionPanel}>
            <Button className={classes.action} variant="contained" onClick={cancelHandler}>Отменить</Button>
            <Button className={classes.action} variant="contained" onClick={actionHandler}>{getActionText(orderState)}</Button>
        </div>
    </div>;
}