import {useNavigate} from "react-router-dom";
import {Table, TableBody, TableCell, TableHead, TableRow} from "@mui/material";

import classes from "./OrderList.module.css";
import {OrderShort} from "../Orders";
import {routeDirectory} from "../../../store/auth";
import formatDate from "../../../shared/format-date";

export function OrderList(props: { orders: Array<OrderShort> }) {
    const navigate = useNavigate();

    const openOrder = (isActiveOrder: boolean, orderId: string) => navigate(routeDirectory + "/orders/" + orderId);

    return <>
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>Создан</TableCell>
                    <TableCell align="right">Стоимость</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {props.orders.map((order: OrderShort) => (
                    <TableRow key={order.orderId}
                              className={classes.tableRow}
                              onClick={() => openOrder(false, order.orderId)}>
                        <TableCell align="center">{formatDate(order.createdDate)}</TableCell>
                        <TableCell align="center">{order.cost}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </>;
}