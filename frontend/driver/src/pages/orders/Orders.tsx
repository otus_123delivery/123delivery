import {OrderList} from "./order-list/OrderList";
import {useCallback, useEffect, useState} from "react";

import {apiGetRequest, ApiRequestResult} from "../../services/api-service";
import AppCard from "../../components/ui/app-card/AppCard";
import {LoadingContainer} from "../../components/ui/loading-container/LoadingContainer";
import classes from "./Orders.module.css";
import {useDispatch, useSelector} from "react-redux";
import {OrderState, updateOrderState} from "../../store/order";
import {ManageOrder} from "./order-list/manage-order/ManageOrder";
import {uiManagerActions} from "../../store/ui-manager";

export interface OrderShort {
    orderId: string;
    cost: number;
    createdDate: Date;
}

export default function Orders() {
    const dispatch = useDispatch();
    const { order, auth : { userInfo } } = useSelector((state: any) => state);
    const [orderList, setOrderList] = useState<Array<OrderShort>>([]);
    const [isLoading, setIsLoading] = useState(false);

    const callback = useCallback(async (abortSignal: AbortSignal) => {
        setIsLoading(true);
        try {
            const response = await apiGetRequest("/driver/orders/available", userInfo, abortSignal);
            setIsLoading(false);
            if (response.status === ApiRequestResult.Success) {
                setOrderList(response.data);
            }
        } catch (error) {
            dispatch(uiManagerActions.error({  message: "Не удалось загрузить доступные заказы." }));
            console.error(error);
        } finally {
            setIsLoading(false);
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (!userInfo) return;

        const abortController = new AbortController();

        if (order.orderState !== OrderState.NotFound) {
            dispatch(updateOrderState(userInfo, abortController.signal));
            return;
        }

        //Обновление списка заказов при загрузке.
        callback(abortController.signal);
        //Автоматическое обновление списка заказов.
        const intervalId = setInterval(() => {
            console.log("updating orders list...");
            callback(abortController.signal);
        }, 10000);

        return () => {
            clearInterval(intervalId);
            abortController.abort();
        }
    }, [order.orderState]); // eslint-disable-line react-hooks/exhaustive-deps

    return <AppCard>
        <h1 className={classes.title}>{order.order ? "Текущий заказ" : "Заказы"}</h1>
        <LoadingContainer isLoading={isLoading}>
            {order.order ? <ManageOrder />
                : <OrderList orders={orderList}/>}
        </LoadingContainer>
    </AppCard>;
}