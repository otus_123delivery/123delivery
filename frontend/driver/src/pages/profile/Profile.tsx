import React, {useCallback, useEffect, useState} from "react";
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import DateRangeIcon from '@mui/icons-material/DateRange';
import DriveFileRenameOutlineIcon from '@mui/icons-material/DriveFileRenameOutline';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import DoDisturbOnIcon from '@mui/icons-material/DoDisturbOn';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import classes from "./Profile.module.css"
import {apiGetRequest} from "../../services/api-service";
import AppCard from "../../components/ui/app-card/AppCard";
import {LoadingContainer} from "../../components/ui/loading-container/LoadingContainer";
import {DocumentScannerRounded, WorkOffRounded, WorkRounded} from "@mui/icons-material";
import {Button, Tooltip} from "@mui/material";
import {changeShiftState, ShiftChangeOperation, ShiftState} from "../../store/shift";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";

function shiftStateToString(shiftState: ShiftState): any {
    switch (shiftState) {
        case ShiftState.Active:
            return <h4 className="text-center"><CheckCircleIcon style={{color: "green"}}/> Смена открыта</h4>;
        case ShiftState.Break:
            return <h4 className="text-center"><ErrorOutlineIcon style={{color: "orange"}}/> Перерыв</h4>;
        case ShiftState.NotFound:
            return <h4 className="text-center"><DoDisturbOnIcon style={{color: "darkred"}}/> Смена закрыта</h4>;
        case ShiftState.TechBreak:
            return <h4 className="text-center"><ErrorOutlineIcon style={{color: "orange"}}/> Технический перерыв</h4>;
        case ShiftState.Unknown:
            return <h4 className="text-center"><ErrorOutlineIcon style={{color: "red"}}/> Ошибка</h4>;
        default:
            throw new RangeError(shiftState);
    }
}

function Profile() {
    const auth = useSelector((state: any) => state.auth);
    const shiftState = useSelector((state: any) => state.shift.shiftState);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const [profile, setProfile] = useState<any>(null);
    const [isLoading, setIsLoading] = useState(false);
    const abortController = new AbortController();

    const handleChangeShiftState = (operation: ShiftChangeOperation) => {
        dispatch(changeShiftState(operation, auth.userInfo, abortController.signal));
    };

    const callback = useCallback( async (abortSignal: AbortSignal) => {
        setIsLoading(true);
        try {
            const response = await apiGetRequest("/driver/profile", auth.userInfo, abortSignal);
            setProfile(response.data);
        } catch (error: any) {
            console.error(error);
            setProfile(null);
        }
        finally {
            setIsLoading(false);
        }
    }, [auth.userInfo]);

    useEffect(() => {
        if (!auth.isLogged) {
            navigate("/login")
            return;
        }
        callback(abortController.signal);
        return () => abortController.abort();
    }, [auth]);// eslint-disable-line react-hooks/exhaustive-deps

    return <div>
        <h1 className="text-center m-2 p-2">Профиль</h1>

        <LoadingContainer isLoading={isLoading}>
            {profile && <AppCard className={classes.card}>
                    <div className={classes.imageContainer}>
                        <img className={classes.image}
                             src={"data:image/png;base64, " + profile.photo}
                             alt="Фото"/>
                    </div>
                    <div className={classes.container}>
                        <div className={classes.section}>
                            <Tooltip title="Имя">
                                <h3 className={classes.text}>
                                    <DriveFileRenameOutlineIcon className={classes.icon}/>{profile.firstname}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Фамилия">
                                <h3 className={classes.text}>
                                    <DriveFileRenameOutlineIcon className={classes.icon}/>{profile.lastname}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Телефон">
                                <h3 className={classes.text}>
                                    <PhoneAndroidIcon className={classes.icon}/>{profile.phone}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Дата рождения">
                                <h3 className={classes.text}>
                                    <DateRangeIcon
                                        className={classes.icon}/>{new Date(profile.birthDate).toLocaleDateString("ru-RU")}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Паспорт">
                                <h3 className={classes.text}>
                                    <DocumentScannerRounded className={classes.icon}/>{profile.passport}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Водительское удостоверение">
                                <h3>
                                    <DateRangeIcon className={classes.icon}/>{profile.driverLicense}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            {profile.canWork ? <h3 className={classes.text}>
                                    <WorkRounded className={classes.icon}/>Активен
                                </h3>
                                : <h3 className={classes.text}>
                                    <WorkOffRounded className={classes.icon}/>Неактивен
                                </h3>
                            }
                        </div>
                    </div>
                    <div className={classes.section}>
                        {shiftStateToString(shiftState)}
                        {shiftState === ShiftState.Active &&
                        <div className={classes.controlButtons}>
                            <Button style={{margin: 4}}
                                    onClick={() => handleChangeShiftState(ShiftChangeOperation.TakeTechBreak)}
                                    variant="contained">Тех. перерыв</Button>
                            <Button style={{margin: 4}}
                                    onClick={() => handleChangeShiftState(ShiftChangeOperation.TakeBreak)}
                                    variant="contained">Перерыв</Button>
                            <Button style={{margin: 4}}
                                    onClick={() => handleChangeShiftState(ShiftChangeOperation.Finish)}
                                    variant="contained">Завершить</Button>
                        </div>}
                        {(shiftState === ShiftState.Break || shiftState === ShiftState.TechBreak) &&
                        <div className={classes.controlButtons}>
                            <Button style={{margin: 4}}
                                    onClick={() => handleChangeShiftState(ShiftChangeOperation.Resume)}
                                    variant="contained">Продолжить</Button>
                            <Button style={{margin: 4}}
                                    onClick={() => handleChangeShiftState(ShiftChangeOperation.Finish)}
                                    variant="contained">Завершить</Button>
                        </div>}
                        {shiftState === ShiftState.NotFound &&
                        <div className={classes.controlButtons}>
                            <Button style={{margin: 4}}
                                    onClick={() => handleChangeShiftState(ShiftChangeOperation.Start)} variant="contained">Начать
                                смену</Button>
                        </div>}
                    </div>
                </AppCard>}
        </LoadingContainer>
    </div>;
}

export default Profile;
