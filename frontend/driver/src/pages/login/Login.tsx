import React, { useEffect } from 'react';
import { Box, Button, TextField } from "@mui/material";
import { useForm } from "react-hook-form";

import classes from "../register/Register.module.css";
import AppCard from "../../components/ui/app-card/AppCard";
import {loginToServer} from "../../store/auth";
import {useDispatch} from "react-redux";

type LoginFormFields = {
    phone: string;
    password: string;
};

export default function Login() {
    const dispatch = useDispatch();

    const {
        register,
        handleSubmit,
        formState: {
            errors,
            touchedFields
        },
        watch,
        trigger
    } = useForm<LoginFormFields>();

    const { phone, password } = watch();

    const onSubmit = (data: LoginFormFields) => dispatch(loginToServer(data.phone, data.password));

    const handleError = (error: any) => console.log(error);

    useEffect(() => {
        const identifier = setTimeout(()=> trigger().catch(handleError), 100);

        return ()=> clearTimeout(identifier);
    }, [touchedFields.phone, touchedFields.password, phone, password, trigger]);

    return <div>
        <h1 className={classes.title}>Вход</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
            <AppCard>
                <div className={classes.group}>
                    <Box>
                        <TextField
                            {...register("phone", { required: true, pattern: /^\+7[0-9]{10}$/ })}
                            error={touchedFields.phone && errors.phone != null}
                            helperText={touchedFields.phone && errors.phone && "Введите корректный телефон"}
                            label="Телефон"
                            variant="filled" />
                    </Box>
                </div>
                <div className={classes.group}>
                    <Box>
                        <TextField
                            {...register("password", { required: true, minLength: 7 })}
                            error={touchedFields.password && errors.password != null}
                            helperText={touchedFields.password && errors.password && "Введите корректный пароль"}
                            type="password"
                            label="Пароль"
                            variant="filled" />
                    </Box>
                </div>
                <Box textAlign='center'>
                    <Button variant="contained" type="submit">Войти</Button>
                </Box>
            </AppCard>
        </form>
    </div>;
}