export interface LoginResponse {
  jwt: string;
  expirationDate: Date;
}
