import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'orderState'})
export class TransformOrderStatePipe implements PipeTransform {
  transform(state: string): string {
    switch (state)
    {
      case "Created":
        return "Создан";
      case "Picked":
        return "Назначен автомобиль";
      case "Performing":
        return "Выполняется";
      case "OnLocation":
        return "Прибыл в место назначения";
      case "Paid":
        return "Оплачен";
      case "CancelledByCustomer":
        return "Отменён";
      case "CancelledByDriver":
        return "Отменён водителем";
      case "Finished":
        return "Завершён";
      default:
        return "-";
    }
  }
}
