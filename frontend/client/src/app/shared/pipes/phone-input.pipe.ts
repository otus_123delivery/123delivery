import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'phoneInput'})
export class PhoneInputPipe implements PipeTransform {
  transform(value: string | undefined, ...args: any[]): any {
    if (value === undefined || value === null) {
      return value;
    }

    return "+" + value.replace(/[^0-9]/g, "");
  }
}
