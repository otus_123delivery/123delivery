export interface CurrentOrder {
  cost: number;
  customerCarTypePreference: number;
  currentState: string;

  fromLatitude: number;
  fromLongitude: number;
  toLatitude: number;
  toLongitude: number;

  driverName:string;

  carLicencePlate:string;
  carType:number;
}
