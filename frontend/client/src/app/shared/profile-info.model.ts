export class ProfileInfoModel {
  constructor(public phone: string,
              public firstname: string,
              public lastname: string) {
  }
}
