export interface ArchivedOrder {
  orderId: string;
  cost: number;
  currentState: string;

  fromLatitude: number;
  fromLongitude: number;
  toLatitude: number;
  toLongitude: number;

  driverName: string;
  carLicencePlate: string;
  carType: number;

  createdDate: string;
  finishDate: string;
  cancelDate: string;
}
