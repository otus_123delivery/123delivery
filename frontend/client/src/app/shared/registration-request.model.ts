export class RegistrationRequest {
  constructor(public phone: string,
              public firstname: string,
              public lastname: string,
              public password: string) {
  }
}
