export class MapPosition {
  constructor(public latitude: number,
              public longitude: number) {
  }
}
