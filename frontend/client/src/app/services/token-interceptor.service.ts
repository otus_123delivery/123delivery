import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

import { AuthService } from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = request.headers.get('Anonymous');
    if (headers != null && headers !== undefined) {
      const newHeaders = request.headers.delete('Anonymous')
      const newRequest = request.clone({ headers: newHeaders });
      return next.handle(newRequest);
    }
    else {
      const authInfo = this.authService.getAuthInfo();
      if (authInfo == null ||  !authInfo.token || new Date() > authInfo.expires) {
        this.router.navigateByUrl("/auth")
          .catch(error => console.error(error));
      }

      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${authInfo?.token}`
        }
      });
      return next.handle(request);
    }
  }
}
