import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { take } from "rxjs/operators";

import { ProfileInfoModel } from "../shared/profile-info.model";
import { CurrentOrder } from "../shared/current-order.model";
import {CalculatedRoute} from "../shared/calculated-route";
import {ArchivedOrder} from "../shared/archived-order.model";
import {NewOrderModel} from "../shared/new-order.model";

@Injectable({
  providedIn: 'root'
})
export class AppApiService {

  constructor(private readonly httpClient: HttpClient) { }

  getProfileInfo(): Observable<ProfileInfoModel> {
    return this.httpClient.get<ProfileInfoModel>("/api/v1/customer/profile")
      .pipe(take(1));
  }

  getOrderHistory(): Observable<ArchivedOrder[]> {
    return this.httpClient.get<ArchivedOrder[]>("/api/v1/customer/order-history")
      .pipe(take(1));
  }

  getCurrentOrder(): Observable<CurrentOrder> {
    return this.httpClient.post<CurrentOrder>("/api/v1/customer/order/current", {})
      .pipe(take(1));
  }

  cancelCurrentOrder(): Observable<any> {
    return this.httpClient.post("/api/v1/customer/order/cancel", {})
      .pipe(take(1));
  }

  makeNewOrder(request: NewOrderModel): Observable<any> {
    return this.httpClient.post("/api/v1/customer/order/new-order", request)
      .pipe(take(1));
  }

  calculateOrderCost(route: CalculatedRoute): Observable<CalculationResponse> {
    const request = {
      fromLatitude: route.from.latitude,
      fromLongitude: route.from.longitude,
      toLatitude: route.to.latitude,
      toLongitude: route.to.longitude
    };

    return this.httpClient.post<CalculationResponse>("/api/v1/customer/order/calculate", request)
      .pipe(take(1));
  }
}

export interface CalculationResponse {
  success: boolean;
  cost: number;
}
