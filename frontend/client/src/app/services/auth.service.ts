import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {BehaviorSubject, Observable, of, throwError} from "rxjs";

import {UserInfo} from "../shared/auth-info.model";
import {RegistrationRequest} from "../shared/registration-request.model";
import {LoginRequest} from "../shared/login-request.model";
import {LoginResponse} from "../shared/login-response.model";
import {catchError, map, take} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authInfo: BehaviorSubject<UserInfo | null> = new BehaviorSubject<UserInfo | null>(null);

  constructor(private readonly httpClient: HttpClient) {
    const authInfoObject = this.getAuthInfo();
    if (authInfoObject) {
      this.authInfo.next(authInfoObject);
    }
  }

  login(phone: string, password: string): Observable<boolean> {
    const request = new LoginRequest(phone, password);
    return this.httpClient.post<LoginResponse>("/api/v1/customer/login", request, {headers: {'Anonymous': 'yes'}})
      .pipe(take(1), map((response: any) => {
        const authObject = new UserInfo(phone, response.jwt, response.expirationDate);
        localStorage.setItem("authInfo", JSON.stringify(authObject));
        this.authInfo.next(authObject);
        return true;
      }), catchError(() => of(false)));
  }

  logout() {
    localStorage.removeItem("authInfo");
    this.authInfo.next(null);
  }

  register(registrationRequest: RegistrationRequest): Observable<string[]> {
    return this.httpClient.post<string[]>("/api/v1/customer/register",
      registrationRequest,
      {headers: {'Anonymous': 'yes'}})
      .pipe(take(1),
        map(() => []),
        catchError((err: any) => throwError(Array.isArray(err.error) ? err.error : ["Unknown"])));
  }

  checkPhoneAvailability(phone: string): Observable<boolean | undefined> {
    return this.httpClient.get("/api/v1/auth/phone",
      {observe: 'response', headers: {'Anonymous': 'yes', 'phone': phone}})
      .pipe(take(1), map((response: HttpResponse<any>) => {
        return response.status == 200;
      }), catchError((err: any) => of(err.status == 400 ? false : undefined)));
  }

  getAuthInfo(): UserInfo | null {
    const authInfo = localStorage.getItem("authInfo");
    if (!authInfo) {
      return null;
    } else {
      return JSON.parse(authInfo);
    }
  }

  isLogged(): boolean {
    const authInfo = this.getAuthInfo();
    const result = authInfo !== null ? authInfo.token !== ''
      && new Date(authInfo.expires) > new Date() : false;
    if (!result) {
      this.authInfo.next(null);
    }
    return result;
  }
}
