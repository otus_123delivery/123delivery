import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from "./services/auth-guard.service";
import {NotFoundComponent} from "./not-found/not-found.component";

const routes: Routes = [
  { path: '', redirectTo: 'profile', pathMatch: 'full' },
  { path: 'order', loadChildren: ()=> import('./modules/order/order.module').then(m => m.OrderModule), canActivate: [AuthGuardService] },
  { path: 'profile', loadChildren: ()=> import('./modules/profile/profile.module').then(m => m.ProfileModule), canActivate: [AuthGuardService] },
  { path: 'auth', loadChildren: ()=> import('./modules/auth/auth.module').then(m => m.AuthModule) },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
