import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {
  @Output() sideNavToggled = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  toggleSideNav() {
    this.sideNavToggled.emit();
  }
}
