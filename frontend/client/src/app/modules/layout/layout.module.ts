import {NgModule} from "@angular/core";
import {MatListModule} from "@angular/material/list";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatIconModule} from "@angular/material/icon";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatRippleModule} from "@angular/material/core";
import {MatButtonModule} from "@angular/material/button";

import {HeaderComponent} from "./header/header.component";
import {LeftMenuComponent} from "./left-menu/left-menu.component";
import {AppRoutingModule} from "../../app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LayoutComponent} from "./layout.component";

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    LeftMenuComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    MatListModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatRippleModule,
    MatButtonModule
  ],
  exports: [
    LayoutComponent
  ]
})
export class LayoutModule {
}
