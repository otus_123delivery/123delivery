import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";

import { AuthService } from "../../../services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Output() sideNavToggled: EventEmitter<any> = new EventEmitter();
  isLoggedIn = false;
  pageName: string = "profile";
  private authChanged?: Subscription;
  private onNavigated?: Subscription;
  userPhone: string | undefined;

  constructor(private readonly authService: AuthService,
              private readonly router: Router) {
  }

  ngOnInit(): void {
    this.authChanged = this.authService.authInfo.subscribe(authObject => {
      this.isLoggedIn = authObject != null && authObject.token !== '' && new Date(authObject.expires) > new Date();
      if (this.isLoggedIn) {
        this.userPhone = authObject?.phone;
      }
    });

    this.onNavigated = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.pageName = this.decodeUrlName(event.url);
      }
    });
  }

  ngOnDestroy() {
    this.authChanged?.unsubscribe();
    this.onNavigated?.unsubscribe();
  }

  decodeUrlName(url: string) {
    switch (url)
    {
      case "/order":
      case "/":
        return "- Новый заказ";
      case "/history":
        return "- История заказов";
      case "/profile":
        return "- Профиль";
      default:
        return "";
    }
  }

  logout() {
    this.authService.logout();
    if (!this.isLoggedIn) {
      this.router.navigateByUrl("/auth/sign-in");
    }
  }

  register() {
    if (!this.isLoggedIn) {
      this.router.navigateByUrl("/auth/sign-up");
    }
  }

  toggleSideNav() {
    this.sideNavToggled.emit();
  }
}
