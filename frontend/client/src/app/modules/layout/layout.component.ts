import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";

import {AuthService} from "../../services/auth.service";
import {SignalRService} from "../../services/signal-r.service";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {
  sidenavOpened: boolean = false;
  isWideScreen: boolean = false;
  // @ts-ignore
  authInfoChangedSubscriber: Subscription;
  isUserLogged: boolean = false;

  constructor(private readonly authService: AuthService,
              private readonly signalRService: SignalRService) {
  }

  ngOnInit() {
    this.isUserLogged = this.authService.isLogged();
    this.authInfoChangedSubscriber = this.authService.authInfo.subscribe(authInfo => {
      this.isUserLogged = authInfo !== null;
      this.onResize(window);
    });

    this.onResize(window);
  }

  ngOnDestroy() {
    this.authInfoChangedSubscriber?.unsubscribe();
  }

  //Проверка ширины окна и открытие/закрытие левого меню в зависимости от ширины окна
  @HostListener('window:resize', ['$event.target'])
  onResize(window: Window) {
    this.isWideScreen = window.innerWidth >= 992;
    if (!this.isUserLogged) {
      this.sidenavOpened = false;
      return;
    }
  }

  //Управление состоянием sidenav
  toggleSideNav(state: boolean) {
    this.sidenavOpened = state;
  }

  //Когда произошел выбор из левого навигационного меню
  leftMenuSelected() {
    if (!this.isWideScreen) {
      this.toggleSideNav(!this.sidenavOpened);
    }
  }

}

