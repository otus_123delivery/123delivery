import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {from, Subscription} from "rxjs";
import {take} from "rxjs/operators";

import {ScriptLoaderService, ScriptType} from "../../../../services/script-loader.service";
import {MapPosition} from "../../../../shared/map-position.model";
import {CalculatedRoute} from "../../../../shared/calculated-route";

declare var ymaps: any;

@Component({
  selector: 'app-yandex-map-router',
  templateUrl: './yandex-map-router.component.html',
  styleUrls: ['./yandex-map-router.component.scss']
})
export class YandexMapRouterComponent implements OnInit, OnDestroy {
  @Input() fromGeo?: boolean;
  @Input() centerPosition?: MapPosition;
  @Input() fromPosition?: MapPosition;
  @Input() defaultZoom?: number;
  @Output() pointsSelected: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() routeCalculated: EventEmitter<CalculatedRoute> = new EventEmitter<CalculatedRoute>();
  isLoading: boolean = false;
  yMapSubscription?: Subscription;
  multiRouteSubscription?: Subscription;

  constructor(private scriptLoader: ScriptLoaderService) {
  }

  ngOnInit(): void {
    this.validateInputParameters();

    this.isLoading = true;
    this.yMapSubscription = this.scriptLoader.loadService(ScriptType.YandexMaps)
      .subscribe(() => {
        this.setupMapControl();
        this.isLoading = false;
      });
  }

  ngOnDestroy() {
    if (this.yMapSubscription) {
      this.yMapSubscription.unsubscribe();
    }
    if (this.multiRouteSubscription) {
      this.multiRouteSubscription.unsubscribe();
    }
  }

  validateInputParameters() {
    if (!this.centerPosition) {
      this.centerPosition = {
        latitude: 55.75334733766462,
        longitude: 37.62281202873023
      };
    }
    if (!this.fromPosition) {
      this.fromPosition = {
        latitude: 55.75334733766462,
        longitude: 37.62281202873023
      };
    }

    if (!this.defaultZoom) {
      this.defaultZoom = 14;
    }
  }

  setupMapControl() {
    ymaps.ready(() => {
      const myMap = new ymaps.Map('map', {
        center: [this.centerPosition?.latitude, this.centerPosition?.longitude],
        zoom: this.defaultZoom,
        controls: ['routeButtonControl']
      });

      const control = myMap.controls.get('routeButtonControl');

      control.routePanel.options.set({
        // Запрещаем показ кнопки, позволяющей менять местами начальную и конечную точки маршрута.
        allowSwitch: false,
        // Включим определение адреса по координатам клика.
        // Адрес будет автоматически подставляться в поле ввода на панели, а также в подпись метки маршрута.
        reverseGeocoding: true,
        // Зададим виды маршрутизации, которые будут доступны пользователям для выбора.
        types: {auto: true}
      });

      // Зададим координаты пункта отправления.
      if (this.fromGeo) {
        control.routePanel.geolocate('from');
      } else {
        control.routePanel.state.set({
          from: [this.fromPosition?.latitude, this.fromPosition?.longitude]
        });
      }

      // Откроем панель для построения маршрутов.
      control.state.set('expanded', true);

      const multiRouteObservable = from(control.routePanel.getRouteAsync());

      this.multiRouteSubscription = multiRouteObservable.pipe(take(1))
        .subscribe((multiroute: any) => {
          multiroute.model.events.add('requestsuccess', () => this.processRouteCalculated(multiroute.getActiveRoute()));
          multiroute.model.events.add('requestfail', this.processRouteFail.bind(this));
          multiroute.model.events.add('requestchange', this.processRouteChanged.bind(this));
        });

    });
  }

  processRouteCalculated(activeRoute: any) {
    if (!activeRoute) return;

    const points = activeRoute.properties.get('boundedBy');
    const duration = activeRoute.properties.get('durationInTraffic');
    const distance = activeRoute.properties.get('distance');

    this.routeCalculated
      .emit({
        from: {latitude: points[0][0], longitude: points[0][1]},
        to: {latitude: points[1][0], longitude: points[1][1]},
        distance: distance.text,
        duration: duration.text
      });
  }

  processRouteFail() {
    console.log("Fail!!");
  }

  processRouteChanged(referencePoints: any) {
    if (!referencePoints?.originalEvent?.referencePoints) return;

    const points = referencePoints.originalEvent.referencePoints;
    this.pointsSelected?.emit(points[0] && points[1]);
  }

}
