import {Component, EventEmitter, NgZone, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from "rxjs";

import {CalculatedRoute} from "../../../shared/calculated-route";
import {AppApiService} from "../../../services/app-api.service";
import {NewOrderModel} from "../../../shared/new-order.model";

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit, OnDestroy {
  @Output('orderCreated') createdOrderEmitter: EventEmitter<any> = new EventEmitter();
  vehicleClass: number = 1;
  calculationSubscription?: Subscription;
  makeNewOrderSubscription?: Subscription;
  isCalculating: boolean = false;
  isCalculated: boolean = false;
  costMessage: string = "";
  distanceMessage: string = "";
  durationMessage: string = "";
  isLoading: boolean = false;
  isPlacingOrder: boolean = false;
  calculatedRoute?: CalculatedRoute;

  constructor(public zone: NgZone,
              private readonly apiService: AppApiService) { }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    if (this.calculationSubscription) {
      this.calculationSubscription.unsubscribe();
    }

    if (this.makeNewOrderSubscription) {
      this.makeNewOrderSubscription.unsubscribe();
    }
  }

  onPointsSelected(isBothSelected: boolean) {
    this.zone.run(() => {
      this.isCalculating = isBothSelected;
      this.isCalculated = false;
    });
  }

  onRouteCalculated(route: CalculatedRoute): void {
    this.calculationSubscription = this.apiService.calculateOrderCost(route)
      .subscribe(calculation => {
        this.calculatedRoute = route;
        this.zone.run(() => {
          this.isCalculating = false;
          this.isCalculated = true;
          this.distanceMessage = route.distance;
          this.costMessage = calculation.cost.toString();
          this.durationMessage = route.duration;
        });
      });
  }

  makeAnOrder() {
    if (this.calculatedRoute === undefined) return;

    this.isPlacingOrder = true;
    const newOrder = new NewOrderModel(this.calculatedRoute.from.latitude, this.calculatedRoute.from.longitude,
      this.calculatedRoute.to.latitude, this.calculatedRoute.to.longitude, this.vehicleClass, undefined);

    this.makeNewOrderSubscription?.unsubscribe();
    this.makeNewOrderSubscription = this.apiService.makeNewOrder(newOrder)
      .subscribe(() => {
          setTimeout(() => {
            this.createdOrderEmitter.emit();
            this.isPlacingOrder = false;
          }, 3000);
        },
        error => {
          this.isPlacingOrder = false;
          console.error(error);
        });
  }
}
