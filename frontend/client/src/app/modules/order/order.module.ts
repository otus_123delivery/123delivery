import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {FormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {RouterModule, Routes} from "@angular/router";

import {NewOrderComponent} from "./new-order/new-order.component";
import {CurrentOrderComponent} from "./current-order/current-order.component";
import {OrderComponent} from "./order.component";
import {HistoryComponent} from "./history/history.component";
import {ApplicationPipesModule} from "../application-pipes.module";
import {AuthGuardService} from "../../services/auth-guard.service";
import { YandexMapRouterComponent } from './new-order/yandex-map/yandex-map-router.component';
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";

const routes: Routes = [
  { path: '', component: OrderComponent, canActivate: [AuthGuardService] },
  { path: 'history', component: HistoryComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  declarations: [
    OrderComponent,
    NewOrderComponent,
    CurrentOrderComponent,
    HistoryComponent,
    YandexMapRouterComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ApplicationPipesModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatProgressBarModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule
  ]
})
export class OrderModule { }
