import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from "rxjs";

import {CurrentOrder} from "../../../shared/current-order.model";
import {AppApiService} from "../../../services/app-api.service";

@Component({
  selector: 'app-current-order',
  templateUrl: './current-order.component.html',
  styleUrls: ['./current-order.component.scss']
})
export class CurrentOrderComponent implements OnDestroy {
  @Input() currentOrder: CurrentOrder | undefined;
  @Output('cancelOrder') cancelEmitter: EventEmitter<any> = new EventEmitter();
  isCancelling: boolean = false;
  cancelCurrentOrderSubscription?: Subscription;

  constructor(private apiService: AppApiService) {
  }

  ngOnDestroy() {
    this.cancelCurrentOrderSubscription?.unsubscribe();
  }

  cancelOrder() {
    this.isCancelling = true;
    this.cancelCurrentOrderSubscription?.unsubscribe();
    this.cancelCurrentOrderSubscription = this.apiService.cancelCurrentOrder().subscribe(() => {
        this.cancelEmitter.emit();
        this.isCancelling = false;
      },
      error => {
        console.error(error);
        this.isCancelling = false;
      })
  }

  getVehicleClassName(classType: number): string {
    switch (classType) {
      case 1:
        return "Эконом";
      case 2:
        return "Комфорт";
      case 3:
        return "Бизнес";
      default:
        return "Не назначен";
    }
  }

}
