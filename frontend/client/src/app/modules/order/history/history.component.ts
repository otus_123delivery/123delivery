import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";

import {AppApiService} from "../../../services/app-api.service";
import {ArchivedOrder} from "../../../shared/archived-order.model";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {
  orderHistory: Array<ArchivedOrder> = [];
  displayedColumns: string[] = ['createdDate', 'currentState', 'carLicencePlate', 'cost'];
  errorOccurred: boolean = false;
  isLoading: boolean = false;
  private orderHistorySubscription?: Subscription;

  constructor(private apiService: AppApiService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.errorOccurred = false;
    this.orderHistorySubscription = this.apiService.getOrderHistory().subscribe((response: Array<ArchivedOrder>) => {
        this.isLoading = false;
        this.orderHistory = response;
      },
      error => {
        this.isLoading = false;
        this.errorOccurred = true;
        this.orderHistory = [];
        console.error(error);
      });
  }

  ngOnDestroy() {
    this.orderHistorySubscription?.unsubscribe();
  }
}
