import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  isProcessing: boolean = false;
  responseErrors: string[] = [];
  loginSubscription?: Subscription;
  authForm: FormGroup = this.initAuthForm();

  constructor(private readonly authService: AuthService,
              private readonly router: Router,
              private readonly route: ActivatedRoute) {
  }

  ngOnInit(): void {
    if (this.route.snapshot.params.hasOwnProperty("phone")) {
      this.authForm.controls["phone"].setValue(this.route.snapshot.params["phone"]);
    }
  }

  ngOnDestroy() {
    this.loginSubscription?.unsubscribe();
  }

  initAuthForm(): FormGroup {
    return new FormGroup({
      'phone': new FormControl(null, [Validators.required, Validators.pattern("^\\+7[0-9]{10}")]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(7)])
    });
  }

  onSubmit() {
    if (!this.authForm.valid) return;
    this.isProcessing = true;

    this.loginSubscription?.unsubscribe();
    this.loginSubscription = this.authService.login(this.authForm.value.phone, this.authForm.value.password).subscribe(result => {
      this.isProcessing = false;
      if (result) {
        this.router.navigateByUrl("/");
      } else {
        this.responseErrors = ['При входе произошла ошибка.'];
      }
    });
  }
}
