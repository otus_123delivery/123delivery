import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {Observable, Subscription} from "rxjs";
import {map, take} from "rxjs/operators";
import {RegistrationRequest} from "../../../shared/registration-request.model";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  signupForm: FormGroup = this.initSignUpForm();
  isProcessing: boolean = false;
  asyncValidation: boolean = false;
  isCheckingNumber: boolean = false;

  responseErrors: string[] = [];
  registerSubscription?: Subscription;

  constructor(private readonly authService: AuthService,
              private readonly router: Router) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.registerSubscription?.unsubscribe();
  }

  initSignUpForm(): FormGroup {
    return new FormGroup({
      'phone': new FormControl(null, [Validators.required, Validators.pattern("^\\+7[0-9]{10}")],
        this.forbiddenPhonesAsync.bind(this)),
      'firstname': new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-Zа-яА-я]{2,15}$")]),
      'lastname': new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-Zа-яА-я]{2,15}$")]),
      'passwords': new FormGroup({
        'password': new FormControl(null, [Validators.required, Validators.minLength(7)]),
        'confirmation': new FormControl(null, [Validators.required, Validators.minLength(7)])
      }, [this.validatePasswords])
    });
  }

  //Проверка доступности номера телефона для нового пользователя.
  forbiddenPhonesAsync(control: AbstractControl): Promise<ValidationErrors | null>
    | Observable<ValidationErrors | null> {
    this.asyncValidation = true;
    this.isCheckingNumber = true;

    return this.authService.checkPhoneAvailability(control.value)
      .pipe(take(1), map((result: boolean | undefined) => {
        this.asyncValidation = false;
        this.isCheckingNumber = false;

        if (result === undefined) return {'serverError': true};

        return result ? null : {'phoneForbidden': true};
      }));
  }

  getPhoneError(): string {
    if (this.signupForm.get('phone')?.errors?.['phoneForbidden']) {
      return "Введён недопустимый номер";
    }

    if (this.signupForm.get('phone')?.errors?.['serverError']) {
      return "Сервер недоступен";
    }

    return "Введите корректный телефон";
  }

  validatePasswords(control: AbstractControl): { [s: string]: boolean } | null {
    if (control.value.password === control.value.confirmation) {
      return null;
    } else {
      return {'passwordNotMatch': true};
    }
  }

  onSubmit() {
    if (!this.signupForm.valid) return;
    const request = new RegistrationRequest(
      this.signupForm.value.phone,
      this.signupForm.value.firstname,
      this.signupForm.value.lastname,
      this.signupForm.value.passwords.password);
    this.isProcessing = true;

    this.registerSubscription?.unsubscribe();
    this.registerSubscription = this.authService.register(request)
      .subscribe((result: string[]) => {
          this.isProcessing = false;
          this.responseErrors = result;
          this.router.navigateByUrl(`/auth/sign-in/${this.signupForm.value.phone}`);
        },
        (errors: string[]) => {
          this.isProcessing = false;
          this.responseErrors = this.decodeErrors(errors);
        });
  }

  decodeErrors(errors: string[]): string[] {
    return errors.map(errorCode => {
      switch (errorCode) {
        case "DuplicateUserName":
          return "Пользователь с таким телефоном уже существует.";
        case "PasswordRequiresDigit":
          return "Пароль должен содержать цифры.";
        case "PasswordRequiresLower":
          return "Пароль должен содержать буквы нижнего регистра.";
        case "PasswordRequiresNonAlphanumeric":
          return "Пароль должен содержать символы.";
        case "PasswordRequiresUpper":
          return "Пароль должен содержать буквы верхнего регистра.";
        default:
          return "Произошла ошибка";
      }
    });
  }

}
