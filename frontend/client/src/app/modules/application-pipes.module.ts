import {NgModule} from "@angular/core";

import {TransformOrderStatePipe} from "../shared/pipes/transform-order-state.pipe";
import {MatSidenavModule} from "@angular/material/sidenav";
import {PhoneInputPipe} from "../shared/pipes/phone-input.pipe";

@NgModule({
  declarations: [
    TransformOrderStatePipe,
    PhoneInputPipe
  ],
  imports: [
    MatSidenavModule
  ],
  exports: [TransformOrderStatePipe, PhoneInputPipe]
})
export class ApplicationPipesModule { }
