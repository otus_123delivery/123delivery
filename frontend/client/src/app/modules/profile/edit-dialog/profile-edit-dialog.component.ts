import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: 'dialog-overview-example-dialog',
  template: `<h1 mat-dialog-title>Изменение ФИО</h1>
    <div mat-dialog-content>
      <mat-form-field appearance="fill">
        <mat-label>Имя</mat-label>
        <input matInput [(ngModel)]="data.firstname">
      </mat-form-field>
      <mat-form-field appearance="fill">
        <mat-label>Фамилия</mat-label>
        <input matInput [(ngModel)]="data.lastname">
      </mat-form-field>
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()" cdkFocusInitial>Отмена</button>
      <button mat-button (click)="closeDialog(data)">Изменить</button>
    </div>`,
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  closeDialog(result: any) {
    this.dialogRef.close(result);
  }
}

export interface DialogData {
  firstname: string;
  lastname: string;
}
