import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";

import {ProfileComponent} from "./profile.component";
import {DialogOverviewExampleDialog} from "./edit-dialog/profile-edit-dialog.component";
import {AuthGuardService} from "../../services/auth-guard.service";

const routes: Routes = [
  {path: '', component: ProfileComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  declarations: [
    ProfileComponent,
    DialogOverviewExampleDialog
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule
  ]
})
export class ProfileModule {
}
