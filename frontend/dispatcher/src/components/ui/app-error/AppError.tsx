import classes from "./AppError.module.css";

export function AppError(props: { message?: string }) {

    return <>
        <h1 className={classes.errorText}>{props.message === undefined ? "Произошла ошибка" : props.message}</h1>
    </>;
}