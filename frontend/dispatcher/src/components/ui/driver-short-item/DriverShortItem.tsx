import {Button, Card, CardActions, CardContent, Switch, Typography} from "@mui/material";
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import React, {useState} from "react";

import classes from "./DriverShortItem.module.css";
import {DriverInfoShort} from "../../../pages/drivers/Drivers";

export function DriverShortItem(props: {
    driverInfo: DriverInfoShort,
    changeStateHandler: (id: string, vehicleId: string, state: boolean) => Promise<boolean>,
    moreHandler: (driverId: string) => void,
    stateChangedCallback: (success: boolean) => void
}) {
    const {driverInfo, changeStateHandler, moreHandler, stateChangedCallback} = props;
    const [isActive, setIsActive] = useState(driverInfo.canWork);
    const [isProcessing, setIsProcessing] = useState(false);


    const driverStateChanged = (state: any) => {
        if (changeStateHandler) {
            setIsProcessing(true);
            changeStateHandler(driverInfo.id, driverInfo.vehicleId, !isActive)
                .then(result => {
                    if (result) {
                        setIsActive(!isActive);
                    }
                    setIsProcessing(false);
                    stateChangedCallback(result);
                })
                .catch(error => {
                    stateChangedCallback(false);
                    console.error(error);
                    setIsProcessing(false);
                });
        }
    };

    return <>
        <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Typography  variant="h5" color="text.secondary" gutterBottom>
                    {driverInfo.firstname + " " + driverInfo.lastname}
                </Typography>
                <div className={classes.flexContainer}>
                    <Typography variant="h5" component="div">{driverInfo.phone}</Typography>
                    <DirectionsCarIcon color={driverInfo.vehicleId ? "primary" : "disabled"} />
                </div>
            </CardContent>
            <CardActions>
                <div className={classes.flexContainer}>
                    <Switch checked={isActive} disabled={!driverInfo.vehicleId || isProcessing} onChange={driverStateChanged} />
                    <Button variant="contained" size="small" onClick={() => moreHandler(driverInfo.id)}>Подробнее</Button>
                </div>
            </CardActions>
        </Card>
    </>;
}