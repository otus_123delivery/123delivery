import {CircularProgress} from "@mui/material";
import React from "react";

export function LoadingContainer(props: { children: any | undefined, isLoading: boolean }) {
    return <>
        {props.isLoading
            ? <div className="d-flex justify-content-center m-5"><CircularProgress variant="indeterminate"/></div>
            : props.children}
    </>;
}