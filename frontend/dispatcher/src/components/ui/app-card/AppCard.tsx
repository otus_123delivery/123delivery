import React from "react";
import { Card as MaterialCard } from "@mui/material";

import classes from "./AppCard.module.css";

export default function AppCard(props: any) {

    return <MaterialCard className={classes.card}>
            {props.children}
        </MaterialCard>;
}