import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom"
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { NavLink } from "react-router-dom";
import { AppRegistration, Article, AttachMoney, Group, Login, Logout } from "@mui/icons-material";
import { Button, Tooltip } from "@mui/material";

import AuthContext, {routeDirectory} from "../../store/auth";
import ProfileButton from "../ui/profile-button/ProfileButton";

const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
    open?: boolean;
}>(({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    }),
}));

interface AppBarProps extends MuiAppBarProps {
    open?: boolean;
}

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

export default function Layout(props: any) {
    const authContext = useContext(AuthContext);

    const navigate = useNavigate();
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const [isLogged, setIsLogged] = useState(authContext.userInfo !== null);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const toLogin = () => {
        navigate(routeDirectory +"/login");
    };

    const toRegister = () => {
      navigate(routeDirectory + "/register");
    };

    useEffect(() => {
        setIsLogged(authContext.userInfo !== undefined);
        if (authContext.userInfo === undefined) {
            setOpen(false);
            navigate(routeDirectory + "/login");
        }
    }, [authContext.userInfo]);

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position="fixed" open={open}>
                <Toolbar>
                    <IconButton
                        disabled={!isLogged}
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{ mr: 2, ...(open && { display: 'none' }) }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
                        Диспетчер
                    </Typography>
                    {isLogged && <ProfileButton />}
                    {!isLogged ? <Tooltip title="Регистрация"><Button color="inherit" onClick={toRegister}><AppRegistration /></Button></Tooltip> : null}
                    {!isLogged ? <Tooltip title="Войти"><Button color="inherit" onClick={toLogin}><Login /></Button></Tooltip> : null}

                    {isLogged ? <Tooltip title="Выйти"><Button color="inherit" onClick={()=> authContext.logout()}><Logout /></Button></Tooltip> : null}
                </Toolbar>
            </AppBar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="persistent"
                anchor="left"
                open={open}>
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    <ListItem button component={NavLink} to={routeDirectory + "/orders"}>
                        <ListItemIcon>
                            <Article />
                        </ListItemIcon>
                        <ListItemText primary="Заказы" />
                    </ListItem>

                    <ListItem button component={NavLink} to={routeDirectory + "/tariffs"}>
                        <ListItemIcon>
                            <AttachMoney />
                        </ListItemIcon>
                        <ListItemText primary="Тарифы" />
                    </ListItem>

                    <ListItem button component={NavLink} to={routeDirectory + "/drivers"}>
                        <ListItemIcon>
                            <Group />
                        </ListItemIcon>
                        <ListItemText primary="Водители" />
                    </ListItem>
                </List>
            </Drawer>
            <Main open={open}>
                <DrawerHeader />
                {props.children}
            </Main>
        </Box>
    );
}
