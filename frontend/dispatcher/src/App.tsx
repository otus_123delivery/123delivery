import React, {useContext} from 'react';
import { Route, Routes } from "react-router-dom";

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Layout from "./components/layout/Layout";
import Profile from "./pages/profile/Profile";
import Orders from "./pages/orders/Orders";
import Tariffs from "./pages/tariffs/Tariffs";
import Drivers from "./pages/drivers/Drivers";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import AuthContext, {routeDirectory} from "./store/auth";
import {OrderInfo} from "./pages/orders/order-info/OrderInfo";
import EditDriver from "./pages/drivers/edit-driver/EditDriver";

function App() {
    const authContext = useContext(AuthContext);

    return <>
        <Layout>
            <Routes>
                {authContext.userInfo === undefined &&
                <><Route path={routeDirectory + "/login"} element={<Login />} />
                <Route path={routeDirectory + "/register"} element={<Register />} /></>}

                {authContext.userInfo !== undefined &&
                (<><Route path={routeDirectory + "/"} element={<Profile />} />
                <Route path={routeDirectory + "/orders"} element={<Orders />} />
                <Route path={routeDirectory + "/orders/:id/:isActive"} element={<OrderInfo />} />
                <Route path={routeDirectory + "/tariffs"} element={<Tariffs />} />
                <Route path={routeDirectory + "/drivers"} element={<Drivers />} />
                <Route path={routeDirectory + "/drivers/:id"} element={<EditDriver />} /></>)}
            </Routes>
        </Layout>
    </>;
}

export default App;
