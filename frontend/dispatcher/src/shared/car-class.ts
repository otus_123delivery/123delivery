export function carClass(carClass: string): string {
    switch (carClass)
    {
        case "1":
            return "Эконом";
        case "2":
            return "Комфорт";
        case "3":
            return "Бизнес";
        default:
            return "";
    }
}