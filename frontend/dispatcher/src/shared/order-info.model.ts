export class OrderInfoModel {
    constructor(
        public orderId: string,
        public currentState: string,
        public customerId: string,
        public customerPhone: string,
        public customerCarTypePreference: number,
        public cost: number,
        public fromLatitude: number,
        public fromLongitude: number,
        public toLatitude: number,
        public toLongitude: number,
        public driverId: string,
        public driverPhone: string,
        public driverName: string,
        public carId: string,
        public carLicencePlate: string,
        public carType: string,
        public createdDate: Date,
        public pickedDate: Date | undefined,
        public performingStartedDate: Date | undefined,
        public arrivingDate: Date | undefined,
        public paymentDate: Date | undefined,
        public finishDate: Date | undefined,
        public cancelDate: Date | undefined
    ) { }
}