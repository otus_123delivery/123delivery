import React, {useEffect, useState} from "react";
import {Box, Button, TextField} from "@mui/material";
import {useForm, ValidateResult} from "react-hook-form";
import {useNavigate} from "react-router-dom";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {DesktopDatePicker} from "@mui/lab";

import classes from "./Register.module.css";
import AppCard from "../../components/ui/app-card/AppCard";
import PhotoUploader from "../../components/ui/photo-uploader/PhotoUploader";
import {apiAddress, routeDirectory} from "../../store/auth";

type RegisterFormFields = {
    phone: string;
    firstname: string;
    lastname: string;
    birthDate: Date;
    password: string;
    confirmation: string;
    photo: string;
};

const getCodeDescription = (description: string) => {
    switch (description) {
        case "DuplicateUserName":
            return "Пользователь с таким телефоном уже существует.";
        case "PasswordRequiresDigit":
            return "Пароль должен содержать цифры.";
        case "PasswordRequiresLower":
            return "Пароль должен содержать буквы нижнего регистра.";
        case "PasswordRequiresNonAlphanumeric":
            return "Пароль должен содержать символы.";
        case "PasswordRequiresUpper":
            return "Пароль должен содержать буквы верхнего регистра.";
        default:
            return "Произошла ошибка";
    }
};

export default function Register() {
    const {
        register,
        handleSubmit,
        trigger,
        watch,
        setValue,
        formState: {
            errors, touchedFields
        }
    } = useForm<RegisterFormFields>({mode: "onTouched", defaultValues: {birthDate: new Date("2000-01-01")}});

    const { birthDate, password, confirmation, photo } = watch();
    const navigate = useNavigate();
    const handleError = (error: any) => console.error(error);

    const [errorCodes, setErrorCodes] = useState<string[]>([]);
    const [isPhoneChecking, setIsPhoneChecking] = useState<boolean>(false);

    const handleUploadedPhoto = (data: string | undefined) => {
        if (data !== undefined) {
            setValue("photo", data);
        }
    }

    function getPhoneAvailability(phone: string): Promise<ValidateResult | Promise<ValidateResult>> {
        setIsPhoneChecking(true);
        return new Promise<ValidateResult | Promise<ValidateResult>>((resolve, reject) => {
            fetch(apiAddress + "/auth/phone", {
                headers: { "phone": phone }
            })
            .then(result => {
                setIsPhoneChecking(false);
                resolve(result.status === 200 || "Недопустимый номер телефона");
            })
            .catch(error => {
                setIsPhoneChecking(false);
                if (error.status === 400) {
                    resolve("Недопустимый номер телефона");
                } else {
                    reject(error);
                }
            });
        });
    }

    const handleUploadPhotoError = () => alert("Указан некорректный файл. Выберите фотографию в формате JPEG либо PNG размером не более 512 КБ");

    const onSubmit = (data: RegisterFormFields) => {
        setErrorCodes([]);
        fetch(apiAddress + "/dispatcher/register",
            {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then(response => {
                if (!response.ok) {
                    setErrorCodes([""]);
                    response.json()
                        .then((errors: string[]) => setErrorCodes(errors))
                        .catch(handleError);
                } else {
                    navigate(routeDirectory + "/login");
                }

            })
            .catch(handleError);
    }

    //Повторная валидация пароля и подтверждения после изменения пароля или подтверждения.
    useEffect(() => {
        trigger("password");
        trigger("confirmation");
    }, [password, confirmation]);

    return <div>
        <h1 className={classes.title}>Регистрация</h1>
        <AppCard>
            {errorCodes.map(code => <p className="bg-danger text-center p-2">{getCodeDescription(code)}</p>)}
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className={classes.container}>
                    <Box>
                        <PhotoUploader maxSize={1024 * 512} onUploaded={handleUploadedPhoto}
                                       onError={handleUploadPhotoError}/>
                    </Box>
                    <input type="hidden" {...register("photo", {required: true})} />
                    {!photo && <p className="bg-warning text-center m-3">Загрузите фото</p>}
                </div>
                <div className={classes.container}>
                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("phone", {
                                    validate: {
                                        phoneAvailability: async (value: string) => await getPhoneAvailability(value)
                                    },
                                    required: "Введите корректный номер",
                                    pattern: {
                                        value: /^\+7[0-9]{10}$/,
                                        message: "Введите корректный номер"
                                    }
                                })}
                                error={isPhoneChecking ? false : (touchedFields.phone && errors.phone != null)}
                                helperText={isPhoneChecking ? "Проверка номера..." : (touchedFields.phone && errors.phone && errors.phone.message)}
                                label="Телефон"
                                variant="filled"/>
                        </Box>
                    </div>
                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("firstname", {
                                    required: true,
                                    pattern: {
                                        value: /^[a-zA-Zа-яА-я]{2,15}$/,
                                        message: "Некорректный формат имени. Имя может содержать только буквы."
                                    }
                                })}
                                error={touchedFields.firstname && errors.firstname != null}
                                helperText={touchedFields.firstname && errors.firstname && "Введите имя"}
                                label="Имя"
                                variant="filled"/>
                        </Box>
                    </div>
                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {
                                    ...register("lastname", {
                                    required: true,
                                    pattern: {
                                        value: /^[a-zA-Zа-яА-я]{2,15}$/,
                                        message: "Некорректный формат фамилии. Фамилия может содержать только буквы."
                                    }
                                })}
                                error={touchedFields.lastname && errors.lastname != null}
                                helperText={touchedFields.lastname && errors.lastname && "Введите фамилию"}
                                label="Фамилия"
                                variant="filled"/>
                        </Box>
                    </div>
                    <div className={classes.group} style={{width: "230px"}}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DesktopDatePicker
                                renderInput={(props) => <TextField {...props} />}
                                label={errors.birthDate ? errors.birthDate.message : "Дата рождения"}
                                {...register("birthDate", {
                                    validate: value =>
                                        value.getFullYear() > 1920 && value.getFullYear() < 2013 || "Выбрана недопустимая дата"
                                })}
                                minDate={new Date('1920-01-01')}
                                maxDate={new Date('2013-01-01')}
                                value={birthDate}
                                onChange={(newValue: any) => {
                                    setValue("birthDate", newValue);
                                }}/>
                        </LocalizationProvider>
                    </div>
                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("password", {
                                    required: "Введите пароль",
                                    minLength: {
                                        value: 7,
                                        message: "Минимальная длина 7 символов"
                                    },
                                    validate: value =>
                                        value === confirmation || "Пароли не совпадают"
                                })}
                                error={touchedFields.password && errors.password != null}
                                helperText={touchedFields.password && errors.password && errors.password.message}
                                type="password"
                                label="Пароль"
                                variant="filled"/>
                        </Box>
                    </div>
                    <div className={classes.group}>
                        <Box>
                            <TextField
                                {...register("confirmation", {
                                    required: "Введите подтверждение пароля",
                                    minLength: {
                                        value: 7,
                                        message: "Минимальная длина 7 символов"
                                    },
                                    validate: value =>
                                        value === password || "Пароли не совпадают"
                                })}
                                error={touchedFields.confirmation && errors.confirmation != null}
                                helperText={touchedFields.confirmation && errors.confirmation && errors.confirmation.message}
                                type="password"
                                label="Подтверждение"
                                variant="filled"/>
                        </Box>
                    </div>
                </div>

                <Box textAlign='center'>
                    <Button variant="contained" type="submit">Регистрация</Button>
                </Box>
            </form>
        </AppCard>
    </div>;
}