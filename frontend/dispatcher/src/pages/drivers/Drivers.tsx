import React, {useContext, useEffect, useState} from "react";
import {Alert, Snackbar} from "@mui/material";

import classes from "./Driver.module.css";
import {DriverShortItem} from "../../components/ui/driver-short-item/DriverShortItem";
import AuthContext, {routeDirectory} from "../../store/auth";
import {UserInfo} from "../../shared/user-info.model";
import AppCard from "../../components/ui/app-card/AppCard";
import {apiGetRequest, apiPatchRequest, ApiRequestResult} from "../../services/api-service";
import {useNavigate} from "react-router-dom";
import {LoadingContainer} from "../../components/ui/loading-container/LoadingContainer";
import {AppError} from "../../components/ui/app-error/AppError";

export class DriverInfoShort {
    constructor(
        public readonly id: string,
        public firstname: string,
        public lastname: string,
        public readonly phone: string,
        public vehicleId: string,
        public canWork: boolean) { }
}

function loadDrivers(userInfo: UserInfo): Promise<Array<DriverInfoShort> | undefined> {
    return new Promise<Array<DriverInfoShort> | undefined>((resolve, reject) => {
        apiGetRequest("/dispatcher/carpool/drivers", userInfo)
            .then(response => {
                if (response.status === ApiRequestResult.Success) {
                    resolve(response.data);
                } else {
                    reject(response);
                }
            })
            .catch(error => reject(error));
    });
}

export default function Drivers() {
    const auth = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [drivers, setDrivers] = useState<Array<DriverInfoShort> | undefined>(undefined);
    const [snackbarIsOpen, setSnackbarIsOpen] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        if (auth.userInfo) {
            setIsLoading(true);
            loadDrivers(auth.userInfo)
                .then(drivers => {
                    setDrivers(drivers);
                    setIsError(false);
                    setIsLoading(false);
                })
                .catch(error => {
                    console.error(error);
                    setIsLoading(false);
                    setIsError(true);
                });
        } else {
            auth.logout();
            return;
        }
    }, [auth.userInfo]);

    const onChangeDriverStateHandled = (id: string, vehicleId: string, state: boolean): Promise<boolean> => {
        return new Promise<boolean>((resolve, reject) => {
            if (auth.userInfo) {
                apiPatchRequest("/dispatcher/carpool/driver", auth.userInfo, {}, {
                    driverId: id,
                    vehicleId: vehicleId,
                    canWork: state
                })
                    .then(response => resolve(response.status === ApiRequestResult.Success))
                    .catch(error => reject(error))
            } else {
                reject("You have no rights.");
            }
        });
    };

    const driverStateChanged = (success: boolean): void => {
        if (!success) setSnackbarIsOpen(true);
    };

    const onSnackbarClosed = () => {
        setSnackbarIsOpen(false);
    };

    const onMoreHandled = (driverId: string): void => navigate(routeDirectory + "/drivers/" + driverId);

    return <>
        <h1 className={classes.headline}>Водители</h1>
            {isError ? <AppError />
            : <LoadingContainer isLoading={isLoading}>

                    {drivers && drivers.length > 0 ? <ul className={classes.driverList}>{drivers.map(driver => <div key={driver.id} style={{margin: 10}}><DriverShortItem
                            changeStateHandler={onChangeDriverStateHandled}
                            stateChangedCallback={driverStateChanged}
                            moreHandler={onMoreHandled}
                            driverInfo={driver}/></div>)}</ul>
                        : <h3 className="text-center bg-warning">Ничего не найдено</h3>}
            </LoadingContainer>}
        <Snackbar open={snackbarIsOpen}
                  autoHideDuration={4000}
                  onClose={onSnackbarClosed}
                  anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
            <Alert severity="error" sx={{width: '100%'}}>
                Не удалось изменить состояние водителя
            </Alert>
        </Snackbar>
    </>;
}