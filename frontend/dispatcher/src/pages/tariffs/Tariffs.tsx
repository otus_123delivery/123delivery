import React, {useContext, useEffect, useState} from "react";
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import Paper from '@mui/material/Paper';

import AuthContext from "../../store/auth";
import {apiPostRequest} from "../../services/api-service";
import {LoadingContainer} from "../../components/ui/loading-container/LoadingContainer";
import {AppError} from "../../components/ui/app-error/AppError";
import formatDate from "../../shared/format-date";

export default function Tariffs() {
    const authContext = useContext(AuthContext);
    const [tariffs, setTariffs] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        if (authContext.userInfo === undefined) {
            authContext.logout();
            return;
        }

        setIsLoading(true);
        apiPostRequest("/dispatcher/tariffs/lookup", {}, authContext.userInfo)
            .then(result => {
                setTariffs(result.data);
                setIsError(false);
                setIsLoading(false);
            })
            .catch(result => {
                console.error(result);
                setIsError(true);
                setIsLoading(false);
            });
    }, []);

    return <div>
        <h1 className="text-center p-2 m-2">Тарифы</h1>
        {isError
            ? <AppError />
            : <LoadingContainer isLoading={isLoading}>
            <TableContainer component={Paper}>
                <Table sx={{minWidth: 650}} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Начало</TableCell>
                            <TableCell align="right">Окончание</TableCell>
                            <TableCell align="right">мин.цена (р)</TableCell>
                            <TableCell align="right">мин.расстояние (м)</TableCell>
                            <TableCell align="right">Базовый [коэф]</TableCell>
                            <TableCell align="right">Дневной [коэф]</TableCell>
                            <TableCell align="right">Ночной [коэф]</TableCell>
                            <TableCell align="right">Трафик [коэф]</TableCell>
                            <TableCell align="right">Кол-во водителей [коэф]</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tariffs.map((tariff: any) => (
                            <TableRow
                                key={tariff.id}
                                sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                <TableCell align="right">{formatDate(tariff.beginDate)}</TableCell>
                                <TableCell
                                    align="right">{tariff.endDate === null ? "бессрочно" : formatDate(tariff.endDate)}</TableCell>

                                <TableCell align="right">{tariff.minimalCost}</TableCell>
                                <TableCell align="right">{tariff.minimalPaidDistance}</TableCell>
                                <TableCell align="right">{tariff.baseRate}</TableCell>
                                <TableCell align="right">{tariff.dayRate}</TableCell>
                                <TableCell align="right">{tariff.nightRate}</TableCell>
                                <TableCell align="right">{tariff.trafficRate}</TableCell>
                                <TableCell align="right">{tariff.driversCountRate}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </LoadingContainer>}
    </div>;
}