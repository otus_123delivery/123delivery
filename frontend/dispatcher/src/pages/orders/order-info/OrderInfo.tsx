import {NavLink, useParams} from "react-router-dom";
import {useContext, useEffect, useState} from "react";
import {apiGetRequest, ApiRequestResult} from "../../../services/api-service";
import AuthContext, {routeDirectory} from "../../../store/auth";
import {Paper, Table, TableBody, TableCell, TableContainer, TableRow, Tooltip} from "@mui/material";
import {OrderInfoModel} from "../../../shared/order-info.model";

import classes from "./OrderInfo.module.css";
import {decodeCurrentState} from "../../../shared/state-decoder";
import {ArrowBackRounded} from "@mui/icons-material";
import {LoadingContainer} from "../../../components/ui/loading-container/LoadingContainer";
import formatDate from "../../../shared/format-date";
import {carClass} from "../../../shared/car-class";

export function OrderInfo() {
    const {id, isActive} = useParams();
    const {userInfo} = useContext(AuthContext);
    const [order, setOrder] = useState<OrderInfoModel | null>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    useEffect(() => {
        if (userInfo === undefined) return;

        setIsLoading(true);
        apiGetRequest("/dispatcher/orders/" + (isActive === "true" ? "active" : "archive") + `/${id}`, userInfo).then(result => {
            if (result.status === ApiRequestResult.Success) {
                setOrder(result.data);
            } else {
                console.log(result);
            }

            setIsLoading(false);
        })
            .catch(error => {
                console.error(error);
                setIsLoading(false);
            });
    }, []);

    if (order === null) return <></>;

    const renderIfNotNull = (rowName: string, data: any) => {
        if (data === null || data === "") return undefined;
        return <TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
            <TableCell component="th" scope="row">{rowName}</TableCell>
            <TableCell align="right">{data}</TableCell>
        </TableRow>;
    };

    return <>
        <div style={{textAlign: "center"}}>
            <NavLink to={routeDirectory + "/orders"}>
                <Tooltip title="Назад">
                    <ArrowBackRounded sx={{ fontSize: "45px", marginBottom: "15px" }} />
                </Tooltip>
            </NavLink>
            <h1 className={classes.title}>Информация о заказе</h1>
        </div>

        <LoadingContainer isLoading={isLoading}>
            <TableContainer component={Paper} className={classes.tableContainer}>
                <Table>
                    <TableBody>
                        {renderIfNotNull("Id заказа", order.orderId)}
                        {renderIfNotNull("Состояние", decodeCurrentState(order.currentState))}
                        {renderIfNotNull("Создан", formatDate(order.createdDate))}
                        {renderIfNotNull("Водитель взял заказ", formatDate(order.pickedDate))}
                        {renderIfNotNull("Посадка", formatDate(order.performingStartedDate))}
                        {renderIfNotNull("Прибыл", formatDate(order.arrivingDate))}
                        {renderIfNotNull("Оплачен", formatDate(order.paymentDate))}
                        {renderIfNotNull("Завершен", formatDate(order.finishDate))}
                        {renderIfNotNull("Отменён", formatDate(order.cancelDate))}
                        {renderIfNotNull("Стоимость", order.cost)}
                        {renderIfNotNull("Телефон клиента", order.customerPhone)}
                        {renderIfNotNull("Телефон водителя", order.driverPhone)}
                        {renderIfNotNull("Номер автомобиля", order.carLicencePlate)}
                        {renderIfNotNull("Откуда (широта)", order.fromLatitude)}
                        {renderIfNotNull("Откуда (долгота)", order.fromLongitude)}
                        {renderIfNotNull("Куда (широта)", order.toLatitude)}
                        {renderIfNotNull("Куда (долгота)", order.toLongitude)}
                        {renderIfNotNull("Тип ТС", carClass(order.carType.toString()))}
                        {renderIfNotNull("Желаемый тип ТС", carClass(order.customerCarTypePreference.toString()))}
                    </TableBody>
                </Table>
            </TableContainer>
        </LoadingContainer>
    </>;
}