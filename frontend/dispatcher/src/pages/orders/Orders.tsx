import React, {useContext, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {Box, Tab, Table, TableBody, TableCell, TableHead, TableRow, Tabs} from "@mui/material";

import classes from "./Orders.module.css";
import AppCard from "../../components/ui/app-card/AppCard";
import {apiGetRequest, ApiRequestResult} from "../../services/api-service";
import AuthContext, {routeDirectory} from "../../store/auth";
import {LoadingContainer} from "../../components/ui/loading-container/LoadingContainer";
import {AppError} from "../../components/ui/app-error/AppError";
import {decodeCurrentState} from "../../shared/state-decoder";
import formatDate from "../../shared/format-date";

interface OrderLookup {
    orderId: string;
    cost: number;
    currentState: string;
    driverName: string;
    carLicencePlate: string;
    createdDate: Date;
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{p: 3}}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function Orders() {
    const authContext = useContext(AuthContext);
    const navigate = useNavigate();

    const [orders, setOrders] = useState<OrderLookup[]>([]);
    const [isError, setIsError] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const [index, setIndex] = React.useState<number>(0);
    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setIndex(newValue);
    };

    useEffect(() => {
        if (authContext.userInfo === undefined) return;

        setIsError(false);
        setIsLoading(true);
        apiGetRequest(`/dispatcher/orders/${index === 0 ? "active" : "archive"}`, authContext.userInfo)
            .then((result: { data: any, status: ApiRequestResult }) => {
                if (result.status === ApiRequestResult.Success) {
                    setIsError(false);
                    setOrders(result.data);
                    setIsLoading(false);
                } else {
                    setOrders([]);
                    setIsError(true);
                    setIsLoading(false);
                }
            })
            .catch(error => {
                console.error(error);
                setOrders([]);
                setIsError(true);
                setIsLoading(false);
            });
    }, [index]);

    const openOrder = (isActiveOrder: boolean, orderId: string) => {
        navigate(routeDirectory + "/orders/" + orderId + "/" + isActiveOrder);
    }

    return <>
        <h1 className={classes.title}>Заказы</h1>
        <Box sx={{width: '100%'}}>
            <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                <Tabs value={index} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="Текущие Заказы" {...a11yProps(0)} />
                    <Tab label="Архивные Заказы" {...a11yProps(1)} />
                </Tabs>
            </Box>
            <TabPanel value={index} index={0}>
                <LoadingContainer isLoading={isLoading}>
                    {!isError
                        ? <AppCard>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Создан</TableCell>
                                        <TableCell>Состояние</TableCell>
                                        <TableCell>Стоимость</TableCell>
                                        <TableCell align="right">Номер автомобиля</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {orders.map((order: OrderLookup) => (
                                        <TableRow key={order.orderId}
                                                  className={classes.tableRow}
                                                  onClick={() => openOrder(true, order.orderId)}>
                                            <TableCell align="center">{formatDate(order.createdDate)}</TableCell>
                                            <TableCell align="center">{decodeCurrentState(order.currentState)}</TableCell>
                                            <TableCell align="center">{order.cost}</TableCell>
                                            <TableCell
                                                align="center">{order.carLicencePlate === "" ? "---" : order.carLicencePlate}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </AppCard>
                        : <AppError/>}
                </LoadingContainer>
            </TabPanel>
            <TabPanel value={index} index={1}>
                <LoadingContainer isLoading={isLoading}>
                    {!isError
                        ? <AppCard>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Создан</TableCell>
                                        <TableCell>Состояние</TableCell>
                                        <TableCell>Стоимость</TableCell>
                                        <TableCell align="right">Номер автомобиля</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {orders.map((order: OrderLookup) => (
                                        <TableRow key={order.orderId}
                                                  className={classes.tableRow}
                                                  onClick={() => openOrder(false, order.orderId)}>
                                            <TableCell align="center">{formatDate(order.createdDate)}</TableCell>
                                            <TableCell align="center">{decodeCurrentState(order.currentState)}</TableCell>
                                            <TableCell align="center">{order.cost}</TableCell>
                                            <TableCell
                                                align="center">{order.carLicencePlate === "" ? "---" : order.carLicencePlate}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </AppCard>
                        : <AppError/>}
                </LoadingContainer>
            </TabPanel>
        </Box>
    </>;
}