import React, { useContext, useEffect, useState } from "react";
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import DateRangeIcon from '@mui/icons-material/DateRange';
import DriveFileRenameOutlineIcon from '@mui/icons-material/DriveFileRenameOutline';

import classes from "./Profile.module.css"
import AuthContext from "../../store/auth";
import { apiGetRequest } from "../../services/api-service";
import AppCard from "../../components/ui/app-card/AppCard";
import {LoadingContainer} from "../../components/ui/loading-container/LoadingContainer";
import {AppError} from "../../components/ui/app-error/AppError";

function Profile() {
    const authContext = useContext(AuthContext);
    const [profile, setProfile] = useState<any>(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (authContext.userInfo !== undefined) {
            setIsLoading(true);
            apiGetRequest("/dispatcher/profile", authContext.userInfo)
                .then(result => {
                    setProfile(result.data);
                    setIsLoading(false);
                })
                .catch(result => {
                    console.error(result);
                    setProfile(null);
                    setIsLoading(false);
                });
        }
    }, [authContext])

    return <div>
        <h1 className="text-center m-2 p-2">Профиль</h1>

        <LoadingContainer isLoading={isLoading}>
            {profile !== null
                ? <AppCard className={classes.card}>
                    <div className={classes.container}>
                        <img className={classes.image}
                             src={"data:image/png;base64, " + profile.photo}
                             alt="Фото" />
                    </div>
                    <div className={classes.container}>
                        <div className={classes.section}>
                            <h3><DriveFileRenameOutlineIcon className={classes.icon} />{profile.firstname}</h3>
                        </div>
                        <div className={classes.section}>
                            <h3><DriveFileRenameOutlineIcon className={classes.icon} />{profile.lastname}</h3>
                        </div>
                        <div className={classes.section}>
                            <h3 className={classes.text}><PhoneAndroidIcon className={classes.icon} />{profile.phone}</h3>
                        </div>
                        <div className={classes.section}>
                            <h3><DateRangeIcon className={classes.icon} />{new Date(profile.birthDate).toLocaleDateString("ru-RU")}</h3>
                        </div>
                    </div>
                </AppCard>
                : <AppError />}
        </LoadingContainer>
    </div>;
}

export default Profile;
