import {UserInfo} from "../shared/user-info.model";
import {apiAddress} from "../store/auth";

export enum ApiRequestResult {
    Success = 1,
    AuthError,
    AuthExpired,
    Unauthorized,
    NotFound,
    UnknownError,
}

async function handleServerResponse(response: any): Promise<{ status: ApiRequestResult, data: any }> {
    if (response.ok) {
        const contentType = response.headers.get("content-type");
        if (contentType && contentType.indexOf("application/json") !== -1) {
            try {
                const data = await response.json();
                return {status: ApiRequestResult.Success, data};
            } catch (error) {
                return {status: ApiRequestResult.UnknownError, data: null};
            }
        } else {
            return {status: ApiRequestResult.Success, data: {}};
        }
    } else {
        switch (response.status) {
            case 401:
                return {status: ApiRequestResult.AuthError, data: null};
            case 403:
                return {status: ApiRequestResult.Unauthorized, data: null};
            case 404:
                return {status: ApiRequestResult.NotFound, data: null};
            default:
                return {status: ApiRequestResult.UnknownError, data: null};
        }
    }
}

export async function apiPostRequest(path: string, body: any, userInfo: UserInfo, abortSignal?: AbortSignal | null | undefined):
    Promise<{ data: any, status: ApiRequestResult }> {

    if (!userInfo) {
        return {status: ApiRequestResult.Unauthorized, data: null};
    }

    if (userInfo.expirationDate <= new Date()) {
        return {
            status: ApiRequestResult.AuthExpired,
            data: null
        };
    }

    try {
        const response = await fetch(apiAddress + path, {
            method: "POST",
            signal: abortSignal,
            body: JSON.stringify({body}),
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + userInfo.jwt
            }
        });
        return await handleServerResponse(response);
    } catch (error: any) {
        if (error && error.status && error.status === 404) {
            return {status: ApiRequestResult.NotFound, data: null};
        }

        return {status: ApiRequestResult.UnknownError, data: null};
    }
}

export async function apiGetRequest(path: string, userInfo: UserInfo | undefined, headers?: any, abortSignal?: AbortSignal | null | undefined):
    Promise<{ data: any, status: ApiRequestResult }> {
    if (!userInfo) {
        return {status: ApiRequestResult.Unauthorized, data: null};
    }

    if (userInfo.expirationDate <= new Date()) {
        return {
            status: ApiRequestResult.AuthExpired,
            data: null
        };
    }

    try {
        const response = await fetch(apiAddress + path, {
            method: "GET",
            signal: abortSignal,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + userInfo.jwt,
                ...headers
            }
        });
        return await handleServerResponse(response);
    } catch (error: any) {
        if (error && error.status && error.status === 404) {
            return {status: ApiRequestResult.NotFound, data: null};
        }

        return {status: ApiRequestResult.UnknownError, data: null};
    }
}

export async function apiPatchRequest(path: string, userInfo: UserInfo, body?: any, headers?: any, abortSignal?: AbortSignal | null | undefined):
    Promise<{ data: any, status: ApiRequestResult }> {
    try {
        const response = await fetch(apiAddress + path, {
            method: "PATCH",
            signal: abortSignal,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + userInfo.jwt,
                ...headers
            },
            body: body
        });
        return await handleServerResponse(response);
    } catch (error: any) {
        if (error && error.status && error.status === 404) {
            return {status: ApiRequestResult.NotFound, data: null};
        }

        return {status: ApiRequestResult.UnknownError, data: null};
    }
}