FROM node:14.18.1-buster-slim as build
WORKDIR /
RUN npm install -g @angular/cli@13.0.2

WORKDIR /client
COPY client/package.json .
RUN npm install

WORKDIR /driver
COPY driver/package.json .
RUN npm install

WORKDIR /dispatcher
COPY dispatcher/package.json .
RUN npm install

WORKDIR /
COPY . .

ARG APP_ENV=production
ENV APP_ENV ${APP_ENV}
WORKDIR /client
RUN ng build --base-href /client/ -c ${APP_ENV}

WORKDIR /driver
RUN npm run build:${APP_ENV}

WORKDIR /dispatcher
RUN npm run build:${APP_ENV}

FROM nginx as final

ENV WWW_DIRECTORY=/usr/share/nginx/html
COPY ./default.conf /etc/nginx/conf.d/default.conf

COPY --from=build /dashboard ${WWW_DIRECTORY}/
COPY --from=build /client/dist/client ${WWW_DIRECTORY}/client
COPY --from=build /driver/build ${WWW_DIRECTORY}/driver
COPY --from=build /dispatcher/build ${WWW_DIRECTORY}/dispatcher
