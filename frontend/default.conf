server {
    listen       80;
    listen  [::]:80;
    server_name  localhost host.docker.internal 127.0.0.1;

    access_log  /var/log/nginx/access.log  main;

    # frontend clients site
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
    
    # backend reverse proxy
    location /api {
        proxy_pass         http://webgateway;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_set_header   Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
    }
	
	# healthcheck reverse proxy
    location /healthcheck {
        proxy_pass         http://webgateway;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_set_header   Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
