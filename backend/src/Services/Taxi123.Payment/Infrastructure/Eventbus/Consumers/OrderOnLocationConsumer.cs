﻿using MassTransit;
using System.Threading.Tasks;
using Taxi123.EventBus.Contracts.Order;

namespace Taxi123.Payment.Infrastructure.Eventbus.Consumers
{
    public class OrderOnLocationConsumer :
        IConsumer<IOrderOnLocation>
    {
        private readonly IBus _bus;

        public OrderOnLocationConsumer(IBus bus)
        {
            _bus = bus;
        }

        public async Task Consume(ConsumeContext<IOrderOnLocation> context)
        {
            await _bus.Publish<IOrderPaid>(new
            {
                context.Message.OrderId,
                Cost = 1234
            });
        }
    }
}
