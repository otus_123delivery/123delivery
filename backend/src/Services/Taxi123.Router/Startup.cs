using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SharedTypes.Abstractions;
using SharedTypes.DataAccess;
using Taxi123.Router.Abstractions;
using Taxi123.Router.Config;
using Taxi123.Router.Infrastructure;
using Taxi123.Router.Mapping;
using Taxi123.Router.Models;
using Taxi123.Router.Services;

namespace Taxi123.Router
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddRedis(Configuration.GetConnectionString("Redis"))
                .AddNpgSql(Configuration["RouterSecret:RouterDbCs"]);

            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
            });

            services.AddDbContext<RouterCalculationDbContext>(cfg =>
            {
                cfg.UseNpgsql(Configuration["RouterSecret:RouterDbCs"]);
                cfg.UseLowerCaseNamingConvention();
            });

            services.AddAutoMapper(typeof(RouterMapping));

            services.AddOptions();
            services.Configure<RouterSecrets>(Configuration.GetSection(RouterSecrets.RouterSecret));

            services.AddHttpClient<IOrderCostCalculatorService, OrderCostCalculatorService>();

            services.AddTransient<IRepository<TariffScale>, 
                GenericEfRepository<TariffScale, RouterCalculationDbContext>>();
            services.AddTransient<IOrderCostCalculatorService, OrderCostCalculatorService>();
            services.AddTransient<ISmartCostCalculator, SmartCostCalculator>();
            services.AddTransient<ITariffManagement, TariffManagement>();

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration.GetConnectionString("Redis");
                options.InstanceName = "CostCalculator";
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapGrpcService<GrpcRouterCalculator>();
                endpoints.MapGrpcService<GrpcTariffManager>();
            });
        }
    }
}
