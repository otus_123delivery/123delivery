﻿using SharedTypes.Abstractions;
using System;

namespace Taxi123.Router.Models
{
    public class TariffScale
        : BaseEntity
    {
        /// <summary>
        /// Минимальная стоимость поездки.
        /// </summary>
        public decimal MinimalCost { get; set; }

        /// <summary>
        /// Минимальная дистанция, которую клиент обязательно оплачивает.
        /// </summary>
        public decimal MinimalPaidDistance { get; set; }

        /// <summary>
        /// Базовый тариф за метр.
        /// </summary>
        public decimal BaseRate { get; set; }

        /// <summary>
        /// Дневной коэффициент.
        /// </summary>
        public decimal DayRate { get; set; }

        /// <summary>
        /// Ночной коэффициент.
        /// </summary>
        public decimal NightRate { get; set; }

        /// <summary>
        /// Коэффициент для пробок.
        /// </summary>
        public decimal TrafficRate { get; set; }

        /// <summary>
        /// Коэффициент зависимости стоимости от количества водителей рядом.
        /// </summary>
        public decimal DriversCountRate { get; set; }

        /// <summary>
        /// Дата начала действия тарифа.
        /// </summary>
        public DateTime BeginDate { get; set; }

        /// <summary>
        /// Дата окончания действия тарифа.
        /// </summary>
        public DateTime? EndDate { get; set; }


        public virtual bool IsCorrectTariffScale()
        {
            if (MinimalCost < 0
                || MinimalPaidDistance < 0
                || BaseRate < 0
                || DayRate < 0
                || NightRate < 0
                || TrafficRate < 0
                || DriversCountRate < 0
                || BeginDate > EndDate)
            {
                return false;
            }

            return true;
        }
    }
}
