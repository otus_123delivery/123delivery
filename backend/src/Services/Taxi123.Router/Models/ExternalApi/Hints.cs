﻿using Newtonsoft.Json;

namespace Taxi123.Router.Models.ExternalApi
{
    public class Hints
    {
        [JsonProperty("visited_nodes.sum")]
        public int VisitedNodesSum { get; set; }

        [JsonProperty("visited_nodes.average")]
        public double VisitedNodesAverage { get; set; }
    }
}
