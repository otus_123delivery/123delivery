﻿using System.Collections.Generic;

namespace Taxi123.Router.Models.ExternalApi
{
    public class Info
    {
        public List<string> Copyrights { get; set; }
        public int Took { get; set; }
    }
}
