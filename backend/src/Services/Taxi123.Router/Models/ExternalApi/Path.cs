﻿namespace Taxi123.Router.Models.ExternalApi
{
    public class Path
    {
        public double Distance { get; set; }
        public double Weight { get; set; }
        public int Time { get; set; }
        public int Transfers { get; set; }
        public string Snapped_waypoints { get; set; }
    }
}
