﻿using System.Threading.Tasks;
using Taxi123.Router.DTO;

namespace Taxi123.Router.Abstractions
{
    public interface IOrderCostCalculatorService
    {
        Task<CostResponse> GetCostAsync(CostRequest request);
    }
}