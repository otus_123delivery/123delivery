﻿using System.Threading.Tasks;
using Taxi123.Router.Models.ExternalApi;

namespace Taxi123.Router.Abstractions
{
    public interface ISmartCostCalculator
    {
        public Task<int> CalculateCostAsync(Path path);
    }

}
