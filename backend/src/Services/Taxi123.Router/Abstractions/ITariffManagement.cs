﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Taxi123.Router.Models;

namespace Taxi123.Router.Abstractions
{
    public interface ITariffManagement
    {
        Task<IEnumerable<TariffScale>> GetAsync(DateTime? from = null, DateTime? to = null);
        Task<TariffScale> GetCurrentAsync();
        Task CreateTariffAsync(TariffScale tariff);
        Task DeleteTariffAsync(Guid id);
        Task UpdateTariffAsync(TariffScale tariff);
    }
}
