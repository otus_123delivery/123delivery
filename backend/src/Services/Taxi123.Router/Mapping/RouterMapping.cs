﻿using AutoMapper;
using Google.Protobuf.WellKnownTypes;
using GrpcRouter;
using System;
using System.Globalization;
using Taxi123.Router.Models;

namespace Taxi123.Router.Mapping
{
    public class RouterMapping : Profile
    {
        public RouterMapping()
        {
            CreateMap<GrpcRouter.CostRequest, DTO.CostRequest>();

            CreateMap<DTO.CostResponse, GrpcRouter.CostResponse>();

            CreateMap<GrpcRouter.TariffDescription, TariffScale>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => Guid.Parse(s.Id)))
                .ForMember(u => u.BeginDate, conf => conf.MapFrom(s => s.BeginDate.ToDateTime()))
                .ForMember(u => u.EndDate, conf => conf.MapFrom(new NullableDateTimeFromStringResolver()));

            CreateMap<TariffScale, GrpcRouter.TariffDescription>()
                .ForMember(d => d.BeginDate, conf => conf.MapFrom(s => s.BeginDate.ToUniversalTime().ToTimestamp()))
                .ForMember(d => d.EndDate, conf => conf.MapFrom(s => s.EndDate.HasValue ? s.EndDate.Value.ToString(CultureInfo.InvariantCulture) : ""));
        }
    }

    public class NullableDateTimeFromStringResolver : IValueResolver<GrpcRouter.TariffDescription, TariffScale, DateTime?>
    {
        public DateTime? Resolve(TariffDescription source, TariffScale destination, DateTime? destMember, ResolutionContext context) 
            => string.IsNullOrEmpty(source.EndDate) ? null : DateTime.Parse(source.EndDate);
    }
}
