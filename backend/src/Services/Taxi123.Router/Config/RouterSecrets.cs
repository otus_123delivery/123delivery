﻿namespace Taxi123.Router.Config
{
    public class RouterSecrets
    {
        public const string RouterSecret = "RouterSecret";
        public string RouterApiKey { get; set; }
        public string RouterDbCs { get; set; }
    }
}
