﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using Taxi123.Router.Infrastructure;

namespace Taxi123.Router.Extensions
{
    public static class RouterDatabaseInitializerExtension
    {
        public static IHost MigrateDatabase(this IHost host)
        {
            using var scope = host.Services.CreateScope();
            using var authDbContext = scope.ServiceProvider.GetRequiredService<RouterCalculationDbContext>();

            if (authDbContext.Database.GetPendingMigrations().Any())
                authDbContext.Database.Migrate();

            return host;
        }

        public static IHost ApplyRequiredDbData(this IHost host)
        {
            using var scope = host.Services.CreateScope();
            using var dbContext = scope.ServiceProvider.GetRequiredService<RouterCalculationDbContext>();
            if (!dbContext.TariffScales.Any())
            {
                dbContext.TariffScales.Add(DefaultRouterTariffData.DefaultTariff);
                dbContext.SaveChanges();
            }

            return host;
        }
    }
}