﻿using AutoMapper;
using Grpc.Core;
using GrpcRouter;
using System.Threading.Tasks;
using Taxi123.Router.Abstractions;

namespace Taxi123.Router.Services
{
    public class GrpcRouterCalculator : GrpcRouter.Router.RouterBase
    {
        private readonly IOrderCostCalculatorService _calculatorService;
        private readonly IMapper _mapper;

        public GrpcRouterCalculator(IOrderCostCalculatorService calculatorService,
            IMapper mapper)
        {
            _calculatorService = calculatorService;
            _mapper = mapper;
        }

        public override async Task<CostResponse> CalculateCost(CostRequest request, ServerCallContext context)
        {
            var result = await _calculatorService.GetCostAsync(_mapper.Map<DTO.CostRequest>(request));
            return _mapper.Map<CostResponse>(result);
        }
    }
}
