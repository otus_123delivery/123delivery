﻿using System;
using System.Threading.Tasks;
using Taxi123.Router.Abstractions;
using Taxi123.Router.Models.ExternalApi;

namespace Taxi123.Router.Services
{
    public class SmartCostCalculator : ISmartCostCalculator
    {
        private readonly ITariffManagement _tariffManagement;

        public SmartCostCalculator(ITariffManagement tariffManagement)
        {
            _tariffManagement = tariffManagement;
        }

        public async Task<int> CalculateCostAsync(Path path)
        {
            var currentDate = DateTime.UtcNow;
            var currentTariff = await _tariffManagement.GetCurrentAsync();

            var distance = Convert.ToDecimal(path.Distance);
            var averageSpeed = distance / path.Time * 3600;

            decimal finalCost = distance < currentTariff.MinimalPaidDistance
                ? currentTariff.MinimalCost 
                : currentTariff.MinimalCost + distance * currentTariff.BaseRate;

            //Дневная или ночная ставка
            if (currentDate.Hour >= 8 && currentDate.Hour < 23)
            {
                finalCost *= currentTariff.DayRate;
            }
            else
            {
                finalCost *= currentTariff.NightRate;
            }

            //TODO: Наложить на стоимость количество водителей вокруг.


            //Если скорость движения меньше 25 км/ч, то прибавляем стоимость за плотность трафика.
            if (averageSpeed < 25)
            {
                finalCost += (1 / averageSpeed) * currentTariff.TrafficRate;
            }

            return (int)Math.Ceiling(finalCost);
        }
    }

}
