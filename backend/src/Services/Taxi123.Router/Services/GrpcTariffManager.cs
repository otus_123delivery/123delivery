﻿using AutoMapper;
using Grpc.Core;
using GrpcRouter;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Taxi123.Router.Abstractions;
using Taxi123.Router.Models;

namespace Taxi123.Router.Services
{
    public class GrpcTariffManager : Tariff.TariffBase
    {
        private readonly ITariffManagement _tariffManagement;
        private readonly IMapper _mapper;
        private readonly ILogger<GrpcTariffManager> _logger;

        public GrpcTariffManager(ITariffManagement tariffManagement,
            IMapper mapper,
            ILogger<GrpcTariffManager> logger)
        {
            _tariffManagement = tariffManagement;
            _mapper = mapper;
            _logger = logger;
        }

        public override async Task<TariffResponse> AddTariff(TariffDescription request, ServerCallContext context)
        {
            try
            {
                request.Id = Guid.NewGuid().ToString();
                var tarrif = _mapper.Map<TariffScale>(request);

                await _tariffManagement.CreateTariffAsync(tarrif);
                return new TariffResponse { Success = true };
            }
            catch (TariffManagementException ex)
            {
                return new TariffResponse { Success = false, ErrorDescription = ex.Message };
            }
            catch (Exception ex)
            {
                _logger.LogError("GrpcTariffManager.AddTariff error: {@ex}", ex);
                return new TariffResponse { Success = false };
            }
        }

        public override async Task<TariffCollection> Get(TariffQuery request, ServerCallContext context)
        {
            try
            {
                DateTime? fromDate = string.IsNullOrEmpty(request.From) ? null : DateTime.Parse(request.From);
                DateTime? toDate = string.IsNullOrEmpty(request.To) ? null : DateTime.Parse(request.To);

                var tariffs = await _tariffManagement.GetAsync(fromDate, toDate);
                var result = new TariffCollection();
                result.Tariffs.AddRange(tariffs.Select(t => _mapper.Map<TariffDescription>(t)));
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("GrpcTariffManager.Get error: {@ex}", ex);
                return new TariffCollection();
            }
        }

        public override async Task<TariffResponse> UpdateTariff(TariffDescription request, ServerCallContext context)
        {
            try
            {
                await _tariffManagement.UpdateTariffAsync(_mapper.Map<TariffScale>(request));
                return new TariffResponse { Success = true };
            }
            catch (TariffManagementException ex)
            {
                return new TariffResponse { Success = false, ErrorDescription = ex.Message };
            }
            catch (Exception ex)
            {
                _logger.LogError("GrpcTariffManager.UpdateTariff error: {@ex}", ex);
                return new TariffResponse { Success = false };
            }
        }

        public override async Task<TariffResponse> DeleteTariff(TariffIdentity request, ServerCallContext context)
        {
            try
            {
                Guid id = Guid.Parse(request.Id);
                await _tariffManagement.DeleteTariffAsync(id);
                return new TariffResponse { Success = true };
            }
            catch (TariffManagementException ex)
            {
                return new TariffResponse { Success = false, ErrorDescription = ex.Message };
            }
            catch (Exception ex)
            {
                _logger.LogError("GrpcTariffManager.RemoveTariff error: {@ex}", ex);
                return new TariffResponse { Success = false };
            }
        }
    }
}
