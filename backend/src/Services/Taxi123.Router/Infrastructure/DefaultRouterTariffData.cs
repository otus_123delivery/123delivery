﻿using System;
using Taxi123.Router.Models;

namespace Taxi123.Router.Infrastructure
{
    public static class DefaultRouterTariffData
    {
        public static TariffScale DefaultTariff => new()
        {
            Id = Guid.Parse("A7345EEB-F93B-4A92-9AE7-CA704AC06AB5"),

            MinimalCost = 120,
            MinimalPaidDistance = 3000,

            BaseRate = 0.015M,
            DayRate = 1,
            NightRate = 0.8M,
            TrafficRate = 500,
            DriversCountRate = 0,

            BeginDate = new DateTime(2020, 1, 1),
            EndDate = null,
        };
    }
}
