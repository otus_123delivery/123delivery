namespace Taxi123.CarPool.Config
{
    public class MongoConfig : IMongoConfig
    {
        public const string MongoSection = "Mongo";

        public string CS { get; set; }
        public string DbName { get; set; }

        public string DriversCollectionName { get; set; }
        public string DriverShiftsActiveCollectionName { get; set; }
        public string DriverShiftsArchiveCollectionName { get; set; }
        public string VehiclesCollectionName { get; set; }
    }
}