namespace Taxi123.CarPool.Config
{
    public interface IMongoConfig
    {
        string CS { get; }
        string DbName { get; }
        string DriversCollectionName { get; }
        string DriverShiftsActiveCollectionName { get; }
        string DriverShiftsArchiveCollectionName { get; }
        string VehiclesCollectionName { get; }
    }
}