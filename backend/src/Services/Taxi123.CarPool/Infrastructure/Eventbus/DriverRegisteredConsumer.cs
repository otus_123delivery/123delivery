﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using SharedTypes.Abstractions;
using Taxi123.CarPool.Models.Domain;
using Taxi123.EventBus.Contracts.CarPool;

namespace Taxi123.CarPool.Infrastructure.Eventbus
{

    public class DriverRegisteredConsumer :
        IConsumer<IDriverRegistered>
    {
        private readonly ILogger<DriverRegisteredConsumer> _logger;
        private readonly IRepository<Driver> _driverRepository;

        public DriverRegisteredConsumer(ILogger<DriverRegisteredConsumer> logger,
            IRepository<Driver> driverRepository)
        {
            _logger = logger;
            _driverRepository = driverRepository;
        }

        public async Task Consume(ConsumeContext<IDriverRegistered> context)
        {
            var driver = new Driver
            {
                Id = context.Message.Id,
                Firstname = context.Message.Firstname,
                Lastname = context.Message.Lastname,
                BirthDate = context.Message.BirthDate,
                Passport = context.Message.Passport,
                DriverLicense = context.Message.DriverLicense,
                CanWork = context.Message.CanWork,
                Phone = context.Message.Phone,
                Photo = context.Message.Photo
            };

            var result = await _driverRepository.CreateAsync(driver);

            if (result)
            {
                _logger.LogInformation($"Driver registered: {driver.Id}");
            }
            else
            {
                _logger.LogError($"Couldn't register driver: {driver.Id}");
            }
        }
    }
}
