﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using SharedTypes.Abstractions;
using Taxi123.CarPool.Models.Domain;
using Taxi123.EventBus.Contracts.DriverShift;

namespace Taxi123.CarPool.Infrastructure.Eventbus
{
    public class ShiftFinalizedConsumer : IConsumer<IShiftFinalized>
    {
        private readonly IRepository<DriverShiftArchive> _repository;
        private readonly ILogger<ShiftFinalizedConsumer> _logger;

        public ShiftFinalizedConsumer(IRepository<DriverShiftArchive> repository,
            ILogger<ShiftFinalizedConsumer> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IShiftFinalized> context)
        {
            try
            {
                var result = await _repository.CreateAsync(MapMessageToModel(context.Message));
                if (!result) _logger.LogError($"Couldn't add an archive shift record with id {context.Message.ShiftId}");
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
            }
        }

        private static DriverShiftArchive MapMessageToModel(IShiftFinalized message) => new()
        {
            Id = message.CorrelationId,
            VehicleId = message.VehicleId,
            EndDate = message.EndDate,
            DriverId = message.DriverId,
            StartDate = message.StartDate,
        };
    }
}