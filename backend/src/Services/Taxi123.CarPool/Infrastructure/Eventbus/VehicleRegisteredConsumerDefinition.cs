﻿using MassTransit.Definition;
using Taxi123.EventBus.Constants;

namespace Taxi123.CarPool.Infrastructure.Eventbus
{
    public class VehicleRegisteredConsumerDefinition :
        ConsumerDefinition<VehicleRegisteredConsumer>
    {
        public VehicleRegisteredConsumerDefinition()
            => EndpointName = CarPoolEndpoints.VehicleRegistered;
    }
}
