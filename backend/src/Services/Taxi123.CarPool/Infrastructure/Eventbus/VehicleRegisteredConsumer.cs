﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using SharedTypes.Abstractions;
using Taxi123.CarPool.Models.Domain;
using Taxi123.EventBus.Contracts.CarPool;

namespace Taxi123.CarPool.Infrastructure.Eventbus
{
    public class VehicleRegisteredConsumer :
        IConsumer<IVehicleRegistered>
    {
        private readonly ILogger<VehicleRegisteredConsumer> _logger;
        private readonly IRepository<Vehicle> _vehicleRepository;
        private readonly IRepository<Driver> _driverRepository;

        public VehicleRegisteredConsumer(ILogger<VehicleRegisteredConsumer> logger,
            IRepository<Vehicle> vehicleRepository,
            IRepository<Driver> driverRepository)
        {
            _logger = logger;
            _vehicleRepository = vehicleRepository;
            _driverRepository = driverRepository;
        }

        public async Task Consume(ConsumeContext<IVehicleRegistered> context)
        {
            var vehicleInfo = context.Message;

            if (!await _vehicleRepository.AnyAsync(v => v.LicensePlate.Equals(vehicleInfo.LicensePlate)))
            {
                var vehicleId = Guid.NewGuid();
                var result = await _vehicleRepository.CreateAsync(new Vehicle
                {
                    Id = vehicleId,
                    Brand = vehicleInfo.Brand,
                    Model = vehicleInfo.Model,
                    LicensePlate = vehicleInfo.LicensePlate,
                    Photo = Convert.FromBase64String(vehicleInfo.Photo),
                    VehicleClass = (VehicleClass)vehicleInfo.VehicleClass
                });

                if (result && vehicleInfo.DriverId.HasValue)
                {
                    var driver = await _driverRepository.GetByIdOrNullAsync(vehicleInfo.DriverId.Value);
                    if (driver != null)
                    {
                        driver.VehicleId = vehicleId;
                        await _driverRepository.UpdateAsync(driver);
                    }
                }

                _logger.Log(result ? LogLevel.Information : LogLevel.Error,
                    $"Vehicle {vehicleInfo.LicensePlate} was {(result ? "" : "not ")}registered.");
            }
            else
            {
                _logger.LogError($"Автомобиль с госномером {vehicleInfo.LicensePlate}" +
                    $" уже существует. Driver id: {vehicleInfo.DriverId}");
            }
        }
    }
}
