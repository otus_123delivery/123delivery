using System.Threading.Tasks;
using MassTransit;
using Taxi123.EventBus.Contracts.Order;

namespace Taxi123.CarPool.Infrastructure.Eventbus
{
    public class OrderCancelledByCustomerConsumer :
        IConsumer<IOrderCancelledByCustomer>
    {
        public Task Consume(ConsumeContext<IOrderCancelledByCustomer> context)
        {
            //Send notification to the driver
            return Task.CompletedTask;
        }
    }
}