﻿using MassTransit.Definition;
using Taxi123.EventBus.Constants;

namespace Taxi123.CarPool.Infrastructure.Eventbus
{
    public class DriverRegisteredConsumerDefinition : ConsumerDefinition<DriverRegisteredConsumer>
    {
        public DriverRegisteredConsumerDefinition()
        {
            EndpointName = CarPoolEndpoints.DriverRegistered;
        }
    }
}
