using System;
using SharedTypes.Abstractions;

namespace Taxi123.CarPool.Models.Domain
{
    public class Driver : BaseEntity
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        
        public Guid? VehicleId { get; set; }
        public string DriverLicense { get; set; }
        public string Passport { get; set; }
        public byte[] Photo { get; set; }
        public bool CanWork { get; set; }
    }
}