﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcCarPool;
using MassTransit;
using Microsoft.Extensions.Logging;
using SharedTypes.Abstractions;
using Taxi123.CarPool.Infrastructure.Eventbus;
using Taxi123.CarPool.Models.Domain;
using Taxi123.EventBus.Contracts.DriverShift;

namespace Taxi123.CarPool.Services
{
    public class DriverShiftManagementService :
        DriverShiftManagement.DriverShiftManagementBase
    {
        private readonly IBus _bus;
        private readonly IRepository<Driver> _driverRepository;
        private readonly IRepository<DriverShiftSagaModel> _activeShiftRepository;
        private readonly ILogger<DriverShiftManagementService> _logger;

        public DriverShiftManagementService(IBus bus,
            IRepository<Driver> driverRepository,
            IRepository<DriverShiftSagaModel> activeShiftRepository,
            ILogger<DriverShiftManagementService> logger)
        {
            _bus = bus;
            _driverRepository = driverRepository;
            _activeShiftRepository = activeShiftRepository;
            _logger = logger;
        }

        public override async Task<ShiftChangeOperationResponse> StartShift(ShiftChangeOperationRequest request, 
            ServerCallContext context)
        {
            var result = new ShiftChangeOperationResponse { Result = false };

            try
            {
                var shiftId = Guid.NewGuid();
                var driverId = Guid.Parse(request.DriverId);

                var driver = await _driverRepository.GetByIdOrNullAsync(driverId);
                if (driver == null)
                {
                    _logger.LogError($"Driver not found: {driverId}");
                    return result;
                }

                if (await _activeShiftRepository.AnyAsync(s => s.DriverId == driverId))
                {
                    _logger.LogError($"Driver already started shift!");
                    return result;
                }

                if (!driver.CanWork || !driver.VehicleId.HasValue)
                {
                    _logger.LogError($"Driver phone: {driver.Phone}; id: {driver.Id}, vehicleId: {driver.VehicleId} can't work.");
                    return result;
                }

                var confirmation = await _bus.Request<IShiftCreated, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = shiftId,
                    DriverId = driver.Id,
                    VehicleId = driver.VehicleId
                });

                result.Result = confirmation.Message.ShiftId == shiftId;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return result;
            }
        }

        public override async Task<ShiftChangeOperationResponse> ResumeShift(ShiftChangeOperationRequest request, 
            ServerCallContext context)
        {
            var result = new ShiftChangeOperationResponse { Result = false };

            try
            {
                var driverId = Guid.Parse(request.DriverId);
                var shift = (await _activeShiftRepository.GetAsync(s => s.DriverId == driverId)).SingleOrDefault();

                if (shift == null) return result;

                var confirmation = await _bus.Request<IShiftActive, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = shift.Id
                });

                result.Result = confirmation.Message.ShiftId == shift.Id;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return result;
            }
        }

        public override async Task<ShiftChangeOperationResponse> FinishShift(ShiftChangeOperationRequest request, ServerCallContext context)
        {
            var result = new ShiftChangeOperationResponse { Result = false };

            try
            {
                var driverId = Guid.Parse(request.DriverId);
                var shift = (await _activeShiftRepository.GetAsync(s => s.DriverId == driverId)).SingleOrDefault();

                if (shift == null) return result;

                var confirmation = await _bus.Request<IShiftFinished, IShiftFinishedSuccessfully>(new
                {
                    ShiftId = shift.Id
                });

                result.Result = confirmation.Message.ShiftId == shift.Id;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return result;
            }
        }

        public override async Task<ShiftChangeOperationResponse> TakeBreak(ShiftChangeOperationRequest request, ServerCallContext context)
        {
            var result = new ShiftChangeOperationResponse { Result = false };

            try
            {
                var driverId = Guid.Parse(request.DriverId);
                var shift = (await _activeShiftRepository.GetAsync(s => s.DriverId == driverId)).SingleOrDefault();

                if (shift == null) return result;

                var confirmation = await _bus.Request<IShiftBreak, IShiftBreakSuccessfull>(new
                {
                    ShiftId = shift.Id
                });

                result.Result = confirmation.Message.ShiftId == shift.Id;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return result;
            }
        }

        public override async Task<ShiftChangeOperationResponse> TakeTechBreak(ShiftChangeOperationRequest request, ServerCallContext context)
        {
            var result = new ShiftChangeOperationResponse { Result = false };

            try
            {
                var driverId = Guid.Parse(request.DriverId);
                var shift = (await _activeShiftRepository.GetAsync(s => s.DriverId == driverId)).SingleOrDefault();

                if (shift == null) return result;

                var confirmation = await _bus.Request<IShiftTechBreak, IShiftTechBreakSuccessfull>(new
                {
                    ShiftId = shift.Id
                });

                result.Result = confirmation.Message.ShiftId == shift.Id;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return result;
            }
        }

        public override async Task<ShiftStateResponse> CurrentShift(ShiftChangeOperationRequest request, ServerCallContext context)
        {
            try
            {
                var driverId = Guid.Parse(request.DriverId);
                var shift = (await _activeShiftRepository.GetAsync(s => s.DriverId == driverId)).SingleOrDefault();

                if (shift == null) return new ShiftStateResponse { Result = ShiftState.NotFound };

                switch (shift.CurrentState)
                {
                    case "Active":
                        return new ShiftStateResponse { Result = ShiftState.Active };
                    case "Break":
                        return new ShiftStateResponse { Result = ShiftState.Break };
                    case "TechnicalBreak":
                        return new ShiftStateResponse { Result = ShiftState.TechBreak };
                    default:
                        return new ShiftStateResponse { Result = ShiftState.Unknown };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return new ShiftStateResponse { Result = ShiftState.Unknown };
            }
        }
    }
}
