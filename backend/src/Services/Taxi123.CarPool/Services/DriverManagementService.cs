﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcCarPool;
using SharedTypes.Abstractions;
using Taxi123.CarPool.Models.Domain;

namespace Taxi123.CarPool.Services
{
    public class DriverManagementService : DriverManagement.DriverManagementBase
    {
        private readonly IRepository<Driver> _driverRepository;
        private readonly IRepository<Vehicle> _vehicleRepository;

        public DriverManagementService(IRepository<Driver> driverRepository,
            IRepository<Vehicle> vehicleRepository)
        {
            _driverRepository = driverRepository;
            _vehicleRepository = vehicleRepository;
        }

        public override async Task<DriversResponse> GetDrivers(Empty request, ServerCallContext context)
        {
            var result = new DriversResponse();
            var drivers = await _driverRepository.GetAsync();
            result.Drivers.AddRange(drivers.Select(d => new DriverShort
            {
                DriverId = d.Id.ToString(),
                CanWork = d.CanWork,
                VehicleId = d.VehicleId.ToString(),
                Firstname = d.Firstname,
                Lastname = d.Lastname,
                Phone = d.Phone
            }));
            return result;
        }

        public override async Task<DriverResponse> GetDriverById(GetDriverRequest request, ServerCallContext context)
        {
            var driverId = Guid.Parse(request.DriverId);
            var driver = await _driverRepository.GetByIdOrNullAsync(driverId);

            if (driver == null) throw new RpcException(new Status(StatusCode.NotFound, "Not found!"));

            return new DriverResponse
            {
                DriverId = driver.Id.ToString(),
                CanWork = driver.CanWork,
                VehicleId = driver.VehicleId.ToString(),
                BirthDate = driver.BirthDate.ToString(CultureInfo.InvariantCulture),
                DriverLicense = driver.DriverLicense,
                Firstname = driver.Firstname,
                Lastname = driver.Lastname,
                Passport = driver.Passport,
                Phone = driver.Phone,
                Photo = Convert.ToBase64String(driver.Photo)
            };
        }

        public override async Task<OperationResult> ChangeDriverState(ChangeDriverStateRequest request, ServerCallContext context)
        {
            var result = new OperationResult { Success = false };
            Guid driverId = Guid.Parse(request.DriverId);

            if (!string.IsNullOrEmpty(request.VehicleId) && !Guid.TryParse(request.VehicleId, out _)) return result;

            Guid? vehicleId = string.IsNullOrEmpty(request.VehicleId) ? null : Guid.Parse(request.VehicleId);

            if (request.CanWork && !vehicleId.HasValue) return result;

            var driver = await _driverRepository.GetByIdOrNullAsync(driverId);

            if (driver == null || (vehicleId.HasValue && !await _vehicleRepository.AnyAsync(v => v.Id == vehicleId))) return result;

            driver.CanWork = request.CanWork;
            driver.VehicleId = vehicleId;
            var updateResult = await _driverRepository.UpdateAsync(driver);
            result.Success = updateResult;

            return result;
        }
    }
}
