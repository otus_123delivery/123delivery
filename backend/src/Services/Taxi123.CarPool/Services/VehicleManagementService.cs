﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcCarPool;
using SharedTypes.Abstractions;
using Taxi123.CarPool.Models.Domain;

namespace Taxi123.CarPool.Services
{
    public class VehicleManagementService : VehicleManagement.VehicleManagementBase
    {
        private readonly IRepository<Driver> _driverRepository;
        private readonly IRepository<Vehicle> _vehicleRepository;

        public VehicleManagementService(IRepository<Driver> driverRepository,
            IRepository<Vehicle> vehicleRepository)
        {
            _driverRepository = driverRepository;
            _vehicleRepository = vehicleRepository;
        }

        public override async Task<VehiclesResponse> GetVehicles(Empty request, ServerCallContext context)
        {
            var vehicles = await _vehicleRepository.GetAsync();
            var result = new VehiclesResponse();

            result.Vehicles.AddRange(vehicles.Select(v => new VehicleShort 
            { 
                VehicleId = v.Id.ToString(), 
                LicensePlate = v.LicensePlate 
            }));
            return result;
        }

        public override async Task<GetVehicleResponse> GetVehicle(GetVehicleRequest request, ServerCallContext context)
        {
            var vehicleId = Guid.Parse(request.VehicleId);
            var vehicle = await _vehicleRepository.GetByIdOrNullAsync(vehicleId);

            if (vehicle == null) throw new RpcException(new Status(StatusCode.NotFound, "Not found!"));

            return new GetVehicleResponse
            {
                Brand = vehicle.Brand,
                VehicleId = vehicle.Id.ToString(),
                LicensePlate = vehicle.LicensePlate,
                Model = vehicle.Model,
                Photo = Convert.ToBase64String(vehicle.Photo),
                VehicleClass = (int)vehicle.VehicleClass
            };
        }

        public override async Task<OperationResult> EditVehicle(EditVehicleRequest request, ServerCallContext context)
        {
            var result = new OperationResult { Success = false };
            var vehicleId = Guid.Parse(request.VehicleId);
            var vehicle = await _vehicleRepository.GetByIdOrNullAsync(vehicleId);

            if (vehicle == null) return result;

            vehicle.Brand = request.Brand;
            vehicle.LicensePlate = request.LicensePlate;
            vehicle.Model = request.Model;
            vehicle.Photo = Convert.FromBase64String(request.Photo);
            vehicle.VehicleClass = (VehicleClass)request.VehicleClass;

            result.Success = await _vehicleRepository.UpdateAsync(vehicle);

            return result;
        }

        public override async Task<OperationResult> DeleteVehicle(DeleteVehicleRequest request, ServerCallContext context)
        {
            var vehicleId = Guid.Parse(request.VehicleId);

            if (await _driverRepository.AnyAsync(d => d.VehicleId == vehicleId)) return new OperationResult { Success = false };

            var result = await _vehicleRepository.DeleteAsync(vehicleId);

            return new OperationResult { Success = result };
        }
    }
}
