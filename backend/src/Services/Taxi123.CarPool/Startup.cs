using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Taxi123.CarPool.Config;
using Taxi123.CarPool.Extensions;
using Taxi123.CarPool.Services;

namespace Taxi123.CarPool
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddMongoDb(Configuration["Mongo:CS"])
                .AddRabbitMQ(rabbitConnectionString: $"amqp://{Configuration["Rabbit:Host"]}");

            services.Configure<MongoConfig>(Configuration.GetSection(MongoConfig.MongoSection));
            services.AddTransient<IMongoConfig>(s => s.GetRequiredService<IOptions<MongoConfig>>().Value);

            services.ConfigureRepositories();
            services.AddEventBus(Configuration);
            services.AddGrpc();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapGrpcService<DriverShiftManagementService>();
                endpoints.MapGrpcService<DriverManagementService>();
                endpoints.MapGrpcService<VehicleManagementService>();
                endpoints.MapGrpcService<DriverOrderManagementService>();
            });
        }
    }
}
