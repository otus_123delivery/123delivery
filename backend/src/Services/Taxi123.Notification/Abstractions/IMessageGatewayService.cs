﻿using System.Threading.Tasks;

namespace Taxi123.Notification.Abstractions
{
    public interface IMessageGatewayService
    {
        Task SendMessageAsync(string phoneNumber, string message);
    }
}
