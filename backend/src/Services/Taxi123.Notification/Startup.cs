using HealthChecks.UI.Client;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Taxi123.Notification.Abstractions;
using Taxi123.Notification.Infrastructure.EventbusConsumers;
using Taxi123.Notification.Services;

namespace Taxi123.Notification
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddRabbitMQ(rabbitConnectionString: $"amqp://{Configuration["Rabbit:Host"]}");
            services.AddTransient<IMessageGatewayService, ConsoleMessageGateway>();

            services.AddMassTransit(cfg =>
            {
                cfg.AddConsumer<OrderPickedNotificationConsumer>();

                cfg.UsingRabbitMq((context, mqcfg) =>
                {
                    mqcfg.Host(Configuration["Rabbit:Host"]);
                    mqcfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            });
        }
    }
}
