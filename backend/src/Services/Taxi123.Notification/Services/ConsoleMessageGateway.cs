﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Taxi123.Notification.Abstractions;

namespace Taxi123.Notification.Services
{
    public class ConsoleMessageGateway : IMessageGatewayService
    {
        public Task SendMessageAsync(string phoneNumber, string message)
        {
            var separator = new string(Enumerable.Repeat('-', 30).ToArray());
            return Console.Out.WriteLineAsync($"{separator}\r\nTo {phoneNumber}: {message}");
        }
    }
}
