﻿using SharedTypes.Abstractions;
using System;

namespace Taxi123.Orders.Models
{
    public class Message
        : BaseEntity
    {
        public MessageAuthor AuthorType { get; set; }
        public DateTime Date { get; set; }
        public string MessageText { get; set; }

        public Order Order { get; set; }
    }
}