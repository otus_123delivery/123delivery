﻿namespace Taxi123.Orders.Models
{
    public enum MessageAuthor
    {
        Driver = 1,
        Customer
    }
}