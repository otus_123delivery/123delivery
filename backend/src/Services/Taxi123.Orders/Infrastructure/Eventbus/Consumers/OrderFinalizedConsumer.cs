﻿using MassTransit;
using Microsoft.Extensions.Logging;
using SharedTypes.Abstractions;
using System;
using System.Threading.Tasks;
using Taxi123.EventBus.Contracts.Order;
using Taxi123.Orders.Models;

namespace Taxi123.Orders.Infrastructure.Eventbus.Consumers
{
    public class OrderFinalizedConsumer : IConsumer<IOrderFinalized>
    {
        private readonly IRepository<Order> _repository;
        private readonly ILogger<OrderFinalizedConsumer> _logger;

        public OrderFinalizedConsumer(IRepository<Order> repository,
            ILogger<OrderFinalizedConsumer> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IOrderFinalized> context)
        {
            try
            {
                var result = await _repository.CreateAsync(MapMessageToModel(context.Message));
                if (!result) _logger.LogError($"Couldn't add message with id {context.Message.OrderId}");
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                throw;
            }
        }

        private static Order MapMessageToModel(IOrderFinalized message) => new Order
        {
            OrderId = message.OrderId,
            ArrivingDate = message.ArrivingDate,
            CancelDate = message.CancelDate,
            CarId = message.CarId,
            CarLicencePlate = message.CarLicencePlate,
            CarType = message.CarType,
            Cost = message.Cost,
            CreatedDate = message.CreatedDate,
            CurrentState = message.CurrentState,
            CustomerCarTypePreference = message.CustomerCarTypePreference,
            CustomerId = message.CustomerId,
            CustomerPhone = message.CustomerPhone,
            DriverId = message.DriverId,
            DriverName = message.DriverName,
            DriverPhone = message.DriverPhone,
            FinishDate = message.FinishDate,
            FromLatitude = message.FromLatitude,
            FromLongitude = message.FromLongitude,
            ToLatitude = message.ToLatitude,
            ToLongitude = message.ToLongitude,
            Id = message.OrderId,
            PaymentDate = message.PaymentDate,
            PerformingStartedDate = message.PerformingStartedDate,
            PickedDate = message.PickedDate
        };
        
    }
}
