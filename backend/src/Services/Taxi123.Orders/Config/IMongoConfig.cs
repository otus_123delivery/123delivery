﻿namespace Taxi123.Orders.Config
{
    public interface IMongoConfig
    {
        string CS { get; set; }
        string DbName { get; set; }
        string MessageDbName { get; set; }
    }
}