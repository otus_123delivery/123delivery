﻿namespace Taxi123.Orders.Config
{
    public class MongoConfig : IMongoConfig
    {
        public const string MongoSection = "Mongo";

        public string CS { get; set; }
        public string DbName { get; set; }
        public string MessageDbName { get; set; }
    }
}
