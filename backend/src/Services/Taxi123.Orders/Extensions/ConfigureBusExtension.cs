﻿using MassTransit;
using MassTransit.Definition;
using MassTransit.MongoDbIntegration.MessageData;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Taxi123.EventBus.Components.OrderSaga;
using Taxi123.EventBus.Contracts.Order;
using Taxi123.Orders.Infrastructure.Eventbus.Consumers;

namespace Taxi123.Orders.Extensions
{
    public static class ConfigureBusExtension
    {
        public static IServiceCollection ConfigureBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.TryAddSingleton(KebabCaseEndpointNameFormatter.Instance);

            services.AddMassTransit(cfg =>
            {
                cfg.AddSagaStateMachine<OrderStateMachine, OrderState>(typeof(OrderStateMachineDefinition))
                    .MongoDbRepository(r =>
                    {
                        r.Connection = configuration["Mongo:CS"];
                        r.DatabaseName = configuration["Mongo:DbName"];
                    });

                cfg.AddConsumer<OrderFinalizedConsumer>();

                cfg.AddRequestClient<IOrderCancelledByCustomer>(RequestTimeout.After(s: 10));
                cfg.AddRequestClient<IOrderPicked>(RequestTimeout.After(s: 10));

                cfg.UsingRabbitMq((context, mqcfg) =>
                {
                    mqcfg.Host(configuration["Rabbit:Host"]);
                    mqcfg.UseMessageData(new MongoDbMessageDataRepository(configuration["Mongo:CS"], 
                        configuration["Mongo:MessageDbName"]));
                    mqcfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();

            return services;
        }
    }
}
