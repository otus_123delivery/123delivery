﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using MassTransit;
using MassTransit.Testing;
using Taxi123.EventBus.Components.DriverShiftSaga;
using Taxi123.EventBus.Contracts.DriverShift;
using Xunit;

namespace Taxi123.CarPoolTests
{
    public class DriverShiftSagaStateMachineTests
    {
        [Fact]
        public async Task PassCreateMessage_ShouldCreateSaga()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid sagaId = NewId.NextGuid();
                Guid driverId = NewId.NextGuid();
                Guid vehicleId = NewId.NextGuid();

                var confirmation = await harness.Bus.Request<IShiftCreated, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = sagaId,
                    DriverId = driverId,
                    VehicleId = vehicleId
                });

                await Task.Delay(300);

                confirmation.Message.ShiftId.Should().Be(sagaId);
                harness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(sagaId, machine, machine.Active);
                instance.Should().NotBeNull();
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassMessages_ShouldBeFinalized()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid sagaId = NewId.NextGuid();
                Guid driverId = NewId.NextGuid();
                Guid vehicleId = NewId.NextGuid();

                await harness.Bus.Request<IShiftCreated, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = sagaId,
                    DriverId = driverId,
                    VehicleId = vehicleId
                });
                await Task.Delay(300);

                await harness.Bus.Publish<IShiftFinished>(new
                {
                    ShiftId = sagaId
                });
                await Task.Delay(300);

                harness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                harness.Consumed.Select<IShiftFinished>().Any().Should().BeTrue();

                sagaHarness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftFinished>().Any().Should().BeTrue();

                var instance = sagaHarness.Created.ContainsInState(sagaId, machine, machine.Finished);
                instance.Should().NotBeNull();
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassTechBreak_ShouldHaveBreakState()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid sagaId = NewId.NextGuid();
                Guid driverId = NewId.NextGuid();
                Guid vehicleId = NewId.NextGuid();

                await harness.Bus.Request<IShiftCreated, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = sagaId,
                    DriverId = driverId,
                    VehicleId = vehicleId
                });

                var confirmation = await harness.Bus.Request<IShiftBreak, IShiftBreakSuccessfull>(new
                {
                    ShiftId = sagaId
                });
                await Task.Delay(300);

                confirmation.Message.ShiftId.Should().Be(sagaId);
                harness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                harness.Consumed.Select<IShiftBreak>().Any().Should().BeTrue();

                sagaHarness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftBreak>().Any().Should().BeTrue();

                var instance = sagaHarness.Created.ContainsInState(sagaId, machine, machine.Break);
                instance.Should().NotBeNull();
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassMessagesFinishAfterBreak_ShouldBeFinalized()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid sagaId = NewId.NextGuid();
                Guid driverId = NewId.NextGuid();
                Guid vehicleId = NewId.NextGuid();

                await harness.Bus.Request<IShiftCreated, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = sagaId,
                    DriverId = driverId,
                    VehicleId = vehicleId
                });

                await harness.Bus.Request<IShiftBreak, IShiftBreakSuccessfull>(new
                {
                    ShiftId = sagaId
                });

                await harness.Bus.Request<IShiftFinished, IShiftFinishedSuccessfully>(new
                {
                    ShiftId = sagaId
                });
                await Task.Delay(300);

                harness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                harness.Consumed.Select<IShiftBreak>().Any().Should().BeTrue();
                harness.Consumed.Select<IShiftFinished>().Any().Should().BeTrue();

                sagaHarness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftBreak>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftFinished>().Any().Should().BeTrue();

                var instance = sagaHarness.Created.ContainsInState(sagaId, machine, machine.Finished);
                instance.Should().NotBeNull();
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassTechBreak_ShouldHaveTechBreakState()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid sagaId = NewId.NextGuid();
                Guid driverId = NewId.NextGuid();
                Guid vehicleId = NewId.NextGuid();

                await harness.Bus.Request<IShiftCreated, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = sagaId,
                    DriverId = driverId,
                    VehicleId = vehicleId
                });

                var confirmation = await harness.Bus.Request<IShiftTechBreak, IShiftTechBreakSuccessfull>(new
                {
                    ShiftId = sagaId
                });

                confirmation.Message.ShiftId.Should().Be(sagaId);
                harness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                harness.Consumed.Select<IShiftTechBreak>().Any().Should().BeTrue();

                sagaHarness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftTechBreak>().Any().Should().BeTrue();

                var instance = sagaHarness.Created.ContainsInState(sagaId, machine, machine.TechnicalBreak);
                instance.Should().NotBeNull();
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassMessagesFinishAfterTechBreak_ShouldBeFinalized()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid sagaId = NewId.NextGuid();
                Guid driverId = NewId.NextGuid();
                Guid vehicleId = NewId.NextGuid();

                await harness.Bus.Request<IShiftCreated, IShiftActivatedSuccessfully>(new
                {
                    ShiftId = sagaId,
                    DriverId = driverId,
                    VehicleId = vehicleId
                });

                await harness.Bus.Request<IShiftTechBreak, IShiftTechBreakSuccessfull>(new
                {
                    ShiftId = sagaId
                });

                await harness.Bus.Request<IShiftFinished, IShiftFinishedSuccessfully>(new
                {
                    ShiftId = sagaId
                });

                harness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                harness.Consumed.Select<IShiftTechBreak>().Any().Should().BeTrue();
                harness.Consumed.Select<IShiftFinished>().Any().Should().BeTrue();

                sagaHarness.Consumed.Select<IShiftCreated>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftTechBreak>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IShiftFinished>().Any().Should().BeTrue();

                var instance = sagaHarness.Created.ContainsInState(sagaId, machine, machine.Finished);
                instance.Should().NotBeNull();
            }
            finally
            {
                await harness.Stop();
            }
        }

        private (DriverShiftStateMachine,
            InMemoryTestHarness,
            StateMachineSagaTestHarness<DriverShiftState, DriverShiftStateMachine>)
            InitTestSaga()
        {
            var machine = new DriverShiftStateMachine();
            var harness = new InMemoryTestHarness();
            var sagaHarness = harness.StateMachineSaga<DriverShiftState, DriverShiftStateMachine>(machine);
            return (machine, harness, sagaHarness);
        }
    }
}