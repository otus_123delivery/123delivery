﻿using System;
using Microsoft.Extensions.DependencyInjection;
using IHost = Microsoft.Extensions.Hosting.IHost;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.Auth;

namespace Taxi123.WebGatewayTests
{
    public static class IntegrationTestsExtensions
    {
        public static string DefaultCustomerUser { get; } = "+79101234567";
        public static string DefaultDriverUser { get; } = "+79101234568";
        public static string DefaultDispatcherUser { get; } = "+79101234569";
        public static string DefaultPassword { get; } = "Z6u%dUw07dgF";

        public static IHost AddDefaultUsers(this IHost host)
        {
            using var scope = host.Services.CreateScope();
            var userManagement = scope.ServiceProvider.GetRequiredService<IUserManagement>();
            var result = userManagement.RegisterUserAsync(new ApplicationUser
            {
                UserName = DefaultCustomerUser
            }, DefaultPassword, ApplicationRoles.Customer).Result;

            if (!result.Item1) throw new InvalidOperationException("Invalid registration");

            result = userManagement.RegisterUserAsync(new ApplicationUser
            {
                UserName = DefaultDriverUser
            }, DefaultPassword, ApplicationRoles.Driver).Result;

            if (!result.Item1) throw new InvalidOperationException("Invalid registration");

            result = userManagement.RegisterUserAsync(new ApplicationUser
            {
                UserName = DefaultDispatcherUser
            }, DefaultPassword, ApplicationRoles.Dispatcher).Result;

            if (!result.Item1) throw new InvalidOperationException("Invalid registration");

            return host;
        }
    }
}