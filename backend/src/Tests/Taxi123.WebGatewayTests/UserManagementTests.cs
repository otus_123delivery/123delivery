using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.Config;
using Taxi123.WebGateway.Infrastructure;
using Taxi123.WebGateway.Services;
using Xunit;

namespace Taxi123.WebGatewayTests
{
    public class UserManagementTests
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly Fixture _fixture;

        public UserManagementTests()
        {
            _fixture = new Fixture();
            _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(b => _fixture.Behaviors.Remove(b));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            
            _serviceProvider = BuildServiceCollectionWithEfInMemory();
        }

        [Fact]
        public async Task LoginAsync_PassEmptyArguments_ReturnEmptyResult()
        {
            //Arrange
            var dbContext = _serviceProvider.GetService<AuthDbContext>();
            await dbContext.Database.EnsureCreatedAsync();
            var userManagement = _serviceProvider.GetService<IUserManagement>();

            //Act
            var result = await userManagement.LoginAsync("", "");

            //Assert
            result.Item1.Should().BeFalse();
            result.Item2.ExpirationDate.Should().Be(default);
            result.Item2.Jwt.Should().BeNull();
        }

        [Fact]
        public async Task LoginAsync_PassInvalidLogin_ReturnEmptyResult()
        {
            //Arrange
            var dbContext = _serviceProvider.GetService<AuthDbContext>();
            await dbContext.Database.EnsureCreatedAsync();
            var userManagement = _serviceProvider.GetService<IUserManagement>();

            //Act
            var result = await userManagement.LoginAsync("81233344332", "kjoniunhiuhui");

            //Assert
            result.Item1.Should().BeFalse();
            result.Item2.ExpirationDate.Should().Be(default);
            result.Item2.Jwt.Should().BeNull();
        }

        [Fact]
        public async Task LoginAsync_CustomerPassValidCredentials_ReturnValidResult()
        {
            //Arrange
            await InitializeInMemoryDataAsync(_serviceProvider);
            var userManagement = _serviceProvider.GetService<IUserManagement>();

            //Act
            var result = await userManagement.LoginAsync("+79203451433", "fake@Pas234word!");

            //Assert
            result.Item1.Should().BeTrue();
            result.Item2.ExpirationDate.Should().NotBe(default);
            result.Item2.Jwt.Should().NotBeNull();
        }

        [Fact]
        public async Task LoginAsync_DriverPassValidCredentials_ReturnValidResult()
        {
            //Arrange

            await InitializeInMemoryDataAsync(_serviceProvider);
            var userManagement = _serviceProvider.GetService<IUserManagement>();

            //Act
            var result = await userManagement.LoginAsync("+78005553535", "fake@Pas234word!");

            //Assert
            result.Item1.Should().BeTrue();
            result.Item2.ExpirationDate.Should().NotBe(default);
            result.Item2.Jwt.Should().NotBeNull();
        }

        [Fact]
        public async Task LoginAsync_PassDispatcherValidCredentials_ReturnValidResult()
        {
            //Arrange
            await InitializeInMemoryDataAsync(_serviceProvider);
            var userManagement = _serviceProvider.GetService<IUserManagement>();

            //Act
            var result = await userManagement.LoginAsync("+79113451233", "fake@Pas234word!");

            //Assert
            result.Item2.ExpirationDate.Should().NotBe(default);
            result.Item2.Jwt.Should().NotBeNull();
        }

        [Fact]
        public async Task CreateUserAsync_CustomerRegisterValidData_ReturnTrue()
        {
            //Arrange
            await InitializeInMemoryDataAsync(_serviceProvider);
            var userManagement = _serviceProvider.GetService<IUserManagement>();
            var customer = _fixture.Build<ApplicationUser>().With(c => c.UserName, GenerateRandomPhone)
                .Without(c => c.Id).Create();

            //Act
            var result = await userManagement.RegisterUserAsync(customer, "fake@Pas234word!", ApplicationRoles.Customer);

            //Assert
            result.Item1.Should().Be(true);
            result.Item2.Should().HaveCount(0);
        }

        [Fact]
        public async Task CreateUserAsync_CustomerRegisterInvalidData_ReturnFalse()
        {
            //Arrange
            await InitializeInMemoryDataAsync(_serviceProvider);
            var userManagement = _serviceProvider.GetService<IUserManagement>();
            var customer = _fixture.Build<ApplicationUser>().With(c => c.UserName, "")
                .Without(c => c.Id).Create();

            //Act
            var (succeeded, errors) = await userManagement.RegisterUserAsync(customer, "fake@Pas234word!", ApplicationRoles.Customer);

            //Assert
            succeeded.Should().Be(false);
            errors.Should().NotHaveCount(0);
        }

        [Fact]
        public async Task CreateUserAsync_DispatcherRegisterInvalidData_ReturnFalse()
        {
            //Arrange
            await InitializeInMemoryDataAsync(_serviceProvider);
            var userManagement = _serviceProvider.GetService<IUserManagement>();
            var customer = _fixture.Build<ApplicationUser>().With(c => c.UserName, "")
                .Without(c => c.Id).Create();

            //Act
            var (succeeded, errors) = await userManagement.RegisterUserAsync(customer, "fake@Pas234word!", ApplicationRoles.Dispatcher);

            //Assert
            succeeded.Should().Be(false);
            errors.Should().NotHaveCount(0);
        }

        [Fact]
        public async Task CreateUserAsync_DispatcherRegisterValidData_ReturnTrue()
        {
            //Arrange
            await InitializeInMemoryDataAsync(_serviceProvider);
            var userManagement = _serviceProvider.GetService<IUserManagement>();
            var customer = _fixture.Build<ApplicationUser>()
                .With(c => c.UserName, GenerateRandomPhone())
                .Without(c => c.Id).Create();

            //Act
            var (succeeded, errors) = await userManagement.RegisterUserAsync(customer, "fake@Pas234word!", ApplicationRoles.Dispatcher);

            //Assert
            succeeded.Should().Be(true);
            errors.Should().HaveCount(0);
        }

        private string GenerateRandomPhone()
        {
            var numberList = new List<int> { 8 };
            var random = new Random();
            for (int i = 1; i < 3; i++)
            {
                numberList.Add(random.Next(10000, 99999));
            }
            return numberList.Aggregate(string.Empty, (t, n) => string.IsNullOrEmpty(t) ? n.ToString() : $"{t}{n}");
        }

        private async Task InitializeInMemoryDataAsync(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();
            using var authDbContext = scope.ServiceProvider.GetRequiredService<AuthDbContext>();
            using var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole<Guid>>>();
            using var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            await authDbContext.Database.EnsureCreatedAsync();

            foreach (var roleName in ApplicationRoles.Roles)
            {
                if (!await roleManager.Roles.AnyAsync(r => r.Name.Equals(roleName)))
                {
                    var role = new IdentityRole<Guid>
                    {
                        Id = Guid.NewGuid(),
                        Name = roleName,
                        NormalizedName = roleName
                    };
                    await roleManager.CreateAsync(role);
                }

            }

            var customer = new ApplicationUser { UserName = "+79203451433" };
            await userManager.CreateAsync(customer, "fake@Pas234word!");
            await userManager.AddToRoleAsync(customer, ApplicationRoles.Customer);

            var driver = new ApplicationUser { UserName = "+78005553535" };
            await userManager.CreateAsync(driver, "fake@Pas234word!");
            await userManager.AddToRoleAsync(driver, ApplicationRoles.Driver);

            var someUser = new ApplicationUser { UserName = "+79113451233" };
            await userManager.CreateAsync(someUser, "fake@Pas234word!");
            await userManager.AddToRoleAsync(someUser, ApplicationRoles.Dispatcher);
        }

        private IServiceProvider BuildServiceCollectionWithEfInMemory()
        {
            var services = new ServiceCollection();

            services.AddLogging();
            services.AddTransient<IUserManagement, UserManagement>();

            var tokenOptions = Options.Create(new JwtConfig
            {
                Audience = "http://127.0.0.1:5000",
                Issuer = "http://127.0.0.1:5000",
                Key = "2ks@a6j%o8oi^gm8a&o8s*a2g3!!ha2ks@a6j%o8oi^"
            });

            services.AddSingleton(tokenOptions);

            services.AddAuthentication()
                .AddJwtBearer(cfg =>
                {
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = "http://127.0.0.1:5000",
                        ValidAudience = "http://127.0.0.1:5000",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("2ks@a6j%o8oi^gm8a&o8s*a2g3!!ha2ks@a6j%o8oi^"))
                    };
                });

            services.AddIdentity<ApplicationUser, IdentityRole<Guid>>(setup => setup.SignIn.RequireConfirmedEmail = false)
                .AddRoles<IdentityRole<Guid>>()
                .AddEntityFrameworkStores<AuthDbContext>();

            services.AddDbContext<AuthDbContext>(cfg =>
            {
                cfg.UseInMemoryDatabase("AuthDb1");
                cfg.UseLowerCaseNamingConvention();
            });

            return services.BuildServiceProvider();
        }
    }
}
