﻿using Automatonymous;
using MassTransit;
using System;
using Taxi123.EventBus.Contracts.DriverShift;

namespace Taxi123.EventBus.Components.DriverShiftSaga
{
    public class DriverShiftStateMachine : 
        MassTransitStateMachine<DriverShiftState>
    {
        public DriverShiftStateMachine()
        {
            Event(() => ShiftCreatedEvent, x => x.CorrelateById(m => m.Message.ShiftId));
            Event(() => ShiftActiveEvent, x => x.CorrelateById(m => m.Message.ShiftId));
            Event(() => DriverTookTechnicalBreakEvent, x => x.CorrelateById(m => m.Message.ShiftId));
            Event(() => DriverTookBreakEvent, x => x.CorrelateById(m => m.Message.ShiftId));
            Event(() => DriverFinishedShiftEvent, x => x.CorrelateById(m => m.Message.ShiftId));

            InstanceState(x => x.CurrentState);

            // Водитель начинает смену
            Initially(
                When(ShiftCreatedEvent)
                    .RespondAsync(context => context.Init<IShiftActivatedSuccessfully>(new
                    {
                        ShiftId = context.Instance.CorrelationId
                    }))
                    .Then(context =>
                    {
                        context.Instance.ShiftId = context.Data.CorrelationId;
                        context.Instance.DriverId = context.Data.DriverId;
                        context.Instance.VehicleId = context.Data.VehicleId;
                        context.Instance.StartDate = DateTime.UtcNow;
                    })
                    .TransitionTo(Active));

            // Водитель взял обеденный перерыв
            During(Active,
                When(DriverTookBreakEvent)
                    .RespondAsync(context =>
                        context.Init<IShiftBreakSuccessfull>(new { ShiftId = context.Instance.CorrelationId }))
                    .TransitionTo(Break));

            // Водитель взял техничейский перерыв(непредусмотренный перерыв)
            During(Active,
                When(DriverTookTechnicalBreakEvent)
                    .RespondAsync(context =>
                        context.Init<IShiftTechBreakSuccessfull>(new { ShiftId = context.Instance.CorrelationId }))
                    .TransitionTo(TechnicalBreak));

            // Переход в состояние "Активный", после перерыва (технического, обычного)
            DuringAny(
                When(ShiftActiveEvent)
                    // Подтверждение о присвоении активного состояния 
                    .RespondAsync(context => context.Init<IShiftActivatedSuccessfully>(new { ShiftId = context.Instance.CorrelationId }))
                    .TransitionTo(Active));

            DuringAny(
                When(DriverFinishedShiftEvent)
                    .RespondAsync(context => context.Init<IShiftFinishedSuccessfully>(new { ShiftId = context.Instance.CorrelationId }))
                    .Then(context => context.Instance.EndDate = DateTime.UtcNow)
                    .PublishAsync(context => context.Init<IShiftFinalized>(GenerateDriverInfo(context.Instance)))
                    .TransitionTo(Finished));

            SetCompleted(async instance => Finished.Equals(await this.GetState(instance)));
        }

        public State Active { get; private set; }
        public State TechnicalBreak { get; private set; }
        public State Break { get; private set; }
        public State Finished { get; private set; }

        public Event<IShiftCreated> ShiftCreatedEvent { get; private set; }
        public Event<IShiftActive> ShiftActiveEvent { get; private set; }
        public Event<IShiftBreak> DriverTookBreakEvent { get; private set; }
        public Event<IShiftTechBreak> DriverTookTechnicalBreakEvent { get; private set; }
        public Event<IShiftFinished> DriverFinishedShiftEvent { get; private set; }

        private static object GenerateDriverInfo(DriverShiftState instance) => new
        {
            ShiftId = instance.CorrelationId,
            instance.DriverId,
            instance.StartDate,
            instance.EndDate,
            instance.Version,
            instance.VehicleId,
        };
    }
}
