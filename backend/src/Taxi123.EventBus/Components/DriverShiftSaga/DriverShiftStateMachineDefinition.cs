﻿using GreenPipes;
using MassTransit;
using MassTransit.Definition;

namespace Taxi123.EventBus.Components.DriverShiftSaga
{
    public class DriverShiftStateMachineDefinition : 
        SagaDefinition<DriverShiftState>
    {
        protected override void ConfigureSaga(IReceiveEndpointConfigurator endpointConfigurator, 
            ISagaConfigurator<DriverShiftState> sagaConfigurator)
        {
            endpointConfigurator.UseMessageRetry(r => r.Intervals(500, 5000, 10000));
            endpointConfigurator.UseInMemoryOutbox();
        }
    }
}
