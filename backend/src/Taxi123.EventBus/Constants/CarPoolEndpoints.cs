﻿namespace Taxi123.EventBus.Constants
{
    public static class CarPoolEndpoints
    {
        public const string DriverRegistered = "driver-registered";
        public const string VehicleRegistered = "vehicle-registered";
    }
}
