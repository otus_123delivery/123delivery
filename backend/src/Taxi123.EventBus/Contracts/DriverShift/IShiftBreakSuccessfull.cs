﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftBreakSuccessfull
    {
        Guid ShiftId { get; }
    }
}