﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftActive
    {
        Guid ShiftId { get; }
    }
}