﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftFinalized
    {
        Guid CorrelationId { get; }
        Guid ShiftId { get; }
        Guid DriverId { get; }
        Guid VehicleId { get; }
        DateTime EndDate { get; }
        DateTime StartDate { get; }
    }
}