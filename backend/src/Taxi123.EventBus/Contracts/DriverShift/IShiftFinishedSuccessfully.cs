﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftFinishedSuccessfully
    {
        Guid ShiftId { get; }
    }
}