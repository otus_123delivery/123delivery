﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftCreated
    {
        Guid CorrelationId { get; }
        Guid ShiftId { get; }
        Guid DriverId { get; }
        Guid VehicleId { get; }
    }
}