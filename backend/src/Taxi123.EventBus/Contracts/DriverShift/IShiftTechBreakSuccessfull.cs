﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftTechBreakSuccessfull
    {
        Guid ShiftId { get; }
    }
}