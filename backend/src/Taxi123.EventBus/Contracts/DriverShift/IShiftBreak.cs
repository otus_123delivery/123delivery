﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftBreak
    {
        Guid ShiftId { get; }
    }
}