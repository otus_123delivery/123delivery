﻿using System;

namespace Taxi123.EventBus.Contracts.SignalR
{
    public interface ISimpleNotification
    {
        Guid UserId { get; }
        string Message { get; }
    }
}
