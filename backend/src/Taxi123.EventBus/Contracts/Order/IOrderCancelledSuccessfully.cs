﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderCancelledSuccessfully
    {
        Guid OrderId { get; }
    }
}
