﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderCancelledByCustomer
    {
        Guid OrderId { get; }
    }
}
