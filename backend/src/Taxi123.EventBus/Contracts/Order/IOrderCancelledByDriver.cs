﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderCancelledByDriver
    {
        Guid OrderId { get; }
    }
}
