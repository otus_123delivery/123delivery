﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderFinished
    {
        Guid OrderId { get; }
    }
}
