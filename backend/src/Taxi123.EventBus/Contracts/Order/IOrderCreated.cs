﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderCreated
    {
        Guid OrderId { get; }

        string CustomerPhone { get; }
        Guid CustomerId { get; }

        int CustomerCarTypePreference { get; }

        int Cost { get; }

        double FromLatitude { get; }
        double FromLongitude { get; }
        double ToLatitude { get; }
        double ToLongitude { get; }
    }
}
