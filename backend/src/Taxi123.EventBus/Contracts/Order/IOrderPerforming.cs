﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderPerforming
    {
        Guid OrderId { get; }
    }
}
