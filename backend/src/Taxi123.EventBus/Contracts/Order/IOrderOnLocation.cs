﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderOnLocation
    {
        Guid OrderId { get; }
    }
}
