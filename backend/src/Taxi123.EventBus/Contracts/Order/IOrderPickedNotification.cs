﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderPickedNotification
    {
        Guid OrderId { get; }

        string DriverName { get; }
        string CarLicencePlate { get; }
        int CarType { get; }
        string CustomerPhone { get; }
    }
}
