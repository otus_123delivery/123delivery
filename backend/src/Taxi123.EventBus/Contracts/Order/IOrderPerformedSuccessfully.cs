﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderPerformedSuccessfully
    {
        Guid OrderId { get; }
        Guid CustomerId { get; }
    }
}
