﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderPicked
    {
        Guid OrderId { get; }

        Guid DriverId { get; }
        string DriverPhone { get; }
        string DriverName { get; }

        Guid CarId { get; }
        string CarLicencePlate { get; }
        int CarType { get; }
    }
}
