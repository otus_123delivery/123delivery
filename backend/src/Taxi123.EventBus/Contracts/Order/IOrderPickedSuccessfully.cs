﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderPickedSuccessfully
    {
        Guid OrderId { get; }
    }
}
