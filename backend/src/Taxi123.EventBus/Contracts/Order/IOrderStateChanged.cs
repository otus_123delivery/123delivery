﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderStateChanged
    {
        Guid OrderId { get; }
        Guid? DriverId { get; }
        Guid CustomerId { get; }
        string CurrentState { get; }
    }
}
