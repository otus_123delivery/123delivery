﻿using System;

namespace Taxi123.EventBus.Contracts.CarPool
{
    public interface IDriverRegistered
    {
        public Guid Id { get; }
        public string Firstname { get; }
        public string Lastname { get; }
        public string Phone { get; }
        public DateTime BirthDate { get; }

        public Guid? VehicleId { get; }
        public string DriverLicense { get; }
        public string Passport { get; }
        public byte[] Photo { get; }
        public bool CanWork { get; }
    }
}
