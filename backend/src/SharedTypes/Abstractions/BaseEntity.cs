﻿using System;

namespace SharedTypes.Abstractions
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
