﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System;
using System.Diagnostics.CodeAnalysis;

namespace SharedTypes.DataAccess.Generators
{
    public class IdGenerator : ValueGenerator<Guid>
    {
        public override bool GeneratesTemporaryValues => false;

        public override Guid Next([NotNull] EntityEntry entry)
            => Guid.NewGuid();
    }
}
