﻿using MongoDB.Driver;
using SharedTypes.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SharedTypes.DataAccess
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(string connectionString, string dbName, string collectionName)
        {
            var database = new MongoClient(connectionString).GetDatabase(dbName);
            _collection = database.GetCollection<T>(collectionName);
        }


        public Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            return Task.Run(() =>
            {
                var query = filter == null 
                ? _collection.AsQueryable() 
                : _collection.AsQueryable().Where(filter);

                var result = orderBy != null
                    ? orderBy(query).ToList()
                    : query.ToList();

                return result.AsEnumerable();
            });
        }

        public async Task<T> GetByIdOrNullAsync(Guid id, string includeProperties = "")
        {
            var queryResult = await _collection.FindAsync(t => t.Id == id);
            return await queryResult.SingleOrDefaultAsync();
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> filter = null)
        {
            var queryResult = await _collection.FindAsync(filter);
            return await queryResult.AnyAsync();
        }

        public async Task<bool> CreateAsync(T entity)
        {
            try
            {
                await _collection.InsertOneAsync(entity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            var updateResult = await _collection.ReplaceOneAsync(x => x.Id == entity.Id, entity, new ReplaceOptions { IsUpsert = true });
            return updateResult.IsAcknowledged;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, id);
            var deleteResult = await _collection.DeleteOneAsync(filter);
            return deleteResult.DeletedCount == 1;
        }
    }
}
