﻿using GrpcCarPool;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Services
{
    public class VehicleManagementService : IVehicleManagementService
    {
        private VehicleManagement.VehicleManagementClient _grpcClient;
        private readonly ILogger<VehicleManagementService> _logger;

        public VehicleManagementService(VehicleManagement.VehicleManagementClient grpcClient,
            ILogger<VehicleManagementService> logger)
        {
            _grpcClient = grpcClient;
            _logger = logger;
        }

        public async Task<GetVehicleResponse> GetVehicleAsync(GetVehicleRequest request)
        {
            _logger.LogDebug("VehicleManagement grpc client GetVehicleAsync, request: {@request}", request);
            var response = await _grpcClient.GetVehicleAsync(request);
            _logger.LogDebug("VehicleManagement grpc client GetVehicleAsync response {@response}", response);
            return response;
        }

        public async Task<VehiclesResponse> GetVehiclesAsync(Empty request)
        {
            _logger.LogDebug("VehicleManagement grpc client GetVehiclesAsync");
            var response = await _grpcClient.GetVehiclesAsync(request);
            _logger.LogDebug("VehicleManagement grpc client GetVehiclesAsync response {@response}", response);
            return response;
        }

        public async Task<OperationResult> EditVehicleAsync(EditVehicleRequest request)
        {
            _logger.LogDebug("VehicleManagement grpc client EditVehicleAsync, request: {@request}", request);
            var response = await _grpcClient.EditVehicleAsync(request);
            _logger.LogDebug("VehicleManagement grpc client EditVehicleAsync response {@response}", response);
            return response;
        }

        public async Task<OperationResult> DeleteVehicleAsync(DeleteVehicleRequest request)
        {
            _logger.LogDebug("VehicleManagement grpc client DeleteVehicleAsync");
            var response = await _grpcClient.DeleteVehicleAsync(request);
            _logger.LogDebug("VehicleManagement grpc client DeleteVehicleAsync response {@response}", response);
            return response;
        }
    }
}
