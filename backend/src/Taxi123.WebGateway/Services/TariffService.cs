﻿using System;
using System.Threading.Tasks;
using GrpcRouter;
using Microsoft.Extensions.Logging;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Services
{
    public class TariffService : ITariffService
    {
        private readonly Tariff.TariffClient _tariffClient;
        private readonly ILogger<TariffService> _logger;

        public TariffService(Tariff.TariffClient tariffClient,
            ILogger<TariffService> logger)
        {
            _tariffClient = tariffClient;
            _logger = logger;
        }

        public async Task<(bool, string)> AddTariffAsync(TariffDescription tariff)
        {
            _logger.LogDebug("Tariff grpc client AddTariff, tariff: {@tariff}", tariff);
            var response = await _tariffClient.AddTariffAsync(tariff);
            _logger.LogDebug("Tariff grpc client AddTariff response {@response}", response);
            return (response.Success, response.Success ? "" : response.ErrorDescription);
        }

        public async Task<TariffCollection> GetTariffsAsync(string fromDate, string toDate)
        {
            var query = new TariffQuery { From = fromDate, To = toDate };
            _logger.LogDebug("Tariff grpc client GetTariffs, tariff: {@query}", query);
            var tariffs = await _tariffClient.GetAsync(query);
            _logger.LogDebug("Tariff grpc client GetTariffs response {@tariffs}", tariffs);
            return tariffs;
        }

        public async Task<(bool, string)> UpdateTariffAsync(TariffDescription tariffData)
        {
            _logger.LogDebug("Tariff grpc client UpdateTariff, tariffData: {@tariffData}", tariffData);
            var response = await _tariffClient.UpdateTariffAsync(tariffData);
            _logger.LogDebug("Tariff grpc client UpdateTariff response {@response}", response);
            return (response.Success, response.Success ? "" : response.ErrorDescription);
        }

        public async Task<(bool, string)> DeleteTariffAsync(Guid id)
        {
            var query = new TariffIdentity { Id = id.ToString() };
            _logger.LogDebug("Tariff grpc client DeleteTariff, tariffData: {@query}", query);
            var response = await _tariffClient.DeleteTariffAsync(query);
            _logger.LogDebug("Tariff grpc client DeleteTariff response {@response}", response);
            return (response.Success, response.Success ? "" : response.ErrorDescription);
        }
    }
}
