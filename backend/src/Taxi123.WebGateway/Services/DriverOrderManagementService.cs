using System.Collections.Generic;
using System.Threading.Tasks;
using GrpcCarPool;
using GrpcOrders;
using Microsoft.Extensions.Logging;
using Taxi123.WebGateway.Abstractions;
using System.Linq;

namespace Taxi123.WebGateway.Services
{
    public class DriverOrderManagementService :
        IDriverOrderManagementService
    {
        private readonly ILogger<DriverOrderManagementService> _logger;
        private readonly DriverOrderManagement.DriverOrderManagementClient _client;
        private readonly IOrderManagerService _orderManager;

        public DriverOrderManagementService(ILogger<DriverOrderManagementService> logger,
            DriverOrderManagement.DriverOrderManagementClient client,
            IOrderManagerService orderManager)
        {
            _logger = logger;
            _client = client;
            _orderManager = orderManager;
        }

        public async Task<OperationResult> PickOrderAsync(PickOrderRequest request)
        {
            _logger.LogDebug("Driver Order Management client PickOrderAsync request: {@request}", request);
            var response = await _client.PickOrderAsync(request);
            _logger.LogDebug("Driver Order Management client PickOrderAsync response: {@response}", response);
            return response;
        }

        public async Task<OperationResult> ChangeOrderStateAsync(ChangeOrderStateRequest request)
        {
            _logger.LogDebug("Driver Order Management client ChangeOrderStateAsync request: {@request}", request);
            var response = await _client.ChangeOrderStateAsync(request);
            _logger.LogDebug("Driver Order Management client ChangeOrderStateAsync response: {@response}", response);
            return response;
        }

        public async Task<IEnumerable<CurrentOrderLookupResponse>> GetAvailableOrdersAsync()
        {
            _logger.LogDebug("Order Manager client GetCurrentOrdersAsync request: {@request}");
            var allOrders = await _orderManager.GetCurrentOrdersAsync();
            _logger.LogDebug("Order Manager client GetCurrentOrdersAsync response: {@allOrders}", allOrders);
            var ordersSlice = allOrders.CurrentOrders.Where(o => o.CurrentState == "Created");
            return ordersSlice;
        }

        public async Task<CurrentOrderResponse> GetOrderByIdAsync(CurrentOrderRequest request)
        {
            _logger.LogDebug("Order Manager client GetCurrentOrdersAsync request: {@request}");
            var response = await _orderManager.GetCurrentOrderByIdAsync(request);
            _logger.LogDebug("Order Manager client GetCurrentOrdersAsync response: {@response}", response);
            return response;
        }
    }
}