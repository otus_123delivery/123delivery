﻿using System.Threading.Tasks;
using GrpcRouter;
using Microsoft.Extensions.Logging;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Services
{
    public class RouterService : IRouterService
    {
        private readonly Router.RouterClient _routerClient;
        private readonly ILogger<RouterService> _logger;

        public RouterService(Router.RouterClient routerClient, 
            ILogger<RouterService> logger)
        {
            _routerClient = routerClient;
            _logger = logger;
        }

        public async Task<CostResponse> CalculateRouteAsync(CostRequest route)
        {
            _logger.LogDebug("Router grpc client created, coordinates: {@route}", route);
            var response = await _routerClient.CalculateCostAsync(route);
            _logger.LogDebug("Router grpc response {@response}", response);

            return response;
        }
    }
}
