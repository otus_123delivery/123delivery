﻿using GrpcCarPool;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Services
{
    public class DriverShiftManagementService : IDriverShiftManagementService
    {
        private readonly DriverShiftManagement.DriverShiftManagementClient _grpcClient;
        private readonly ILogger<DriverShiftManagementService> _logger;

        public DriverShiftManagementService(DriverShiftManagement.DriverShiftManagementClient grpcClient,
            ILogger<DriverShiftManagementService> logger)
        {
            _grpcClient = grpcClient;
            _logger = logger;
        }

        public async Task<ShiftChangeOperationResponse> StartShiftAsync(ShiftChangeOperationRequest request)
        {
            _logger.LogDebug("Driver Shift Management client StartShiftAsync request: {@request}", request);
            var response = await _grpcClient.StartShiftAsync(request);
            _logger.LogDebug("Driver Shift Management client StartShiftAsync response: {@response}", response);
            return response;
        }

        public async Task<ShiftChangeOperationResponse> ResumeShiftAsync(ShiftChangeOperationRequest request)
        {
            _logger.LogDebug("Driver Shift Management client ResumeShiftAsync request: {@request}", request);
            var response = await _grpcClient.ResumeShiftAsync(request);
            _logger.LogDebug("Driver Shift Management client ResumeShiftAsync response: {@response}", response);
            return response;
        }

        public async Task<ShiftChangeOperationResponse> FinishShiftAsync(ShiftChangeOperationRequest request)
        {
            _logger.LogDebug("Driver Shift Management client FinishShiftAsync request: {@request}", request);
            var response = await _grpcClient.FinishShiftAsync(request);
            _logger.LogDebug("Driver Shift Management client FinishShiftAsync response: {@response}", response);
            return response;
        }

        public async Task<ShiftChangeOperationResponse> TakeBreakAsync(ShiftChangeOperationRequest request)
        {
            _logger.LogDebug("Driver Shift Management client TakeBreak request: {@request}", request);
            var response = await _grpcClient.TakeBreakAsync(request);
            _logger.LogDebug("Driver Shift Management client TakeBreak response: {@response}", response);
            return response;
        }

        public async Task<ShiftChangeOperationResponse> TakeTechBreakAsync(ShiftChangeOperationRequest request)
        {
            _logger.LogDebug("Driver Shift Management client TakeBreakAsync request: {@request}", request);
            var response = await _grpcClient.TakeTechBreakAsync(request);
            _logger.LogDebug("Driver Shift Management client TakeBreakAsync response: {@response}", response);
            return response;
        }

        public async Task<ShiftStateResponse> CurrentShiftAsync(ShiftChangeOperationRequest request)
        {
            _logger.LogDebug("Driver Shift Management client CurrentShiftAsync request: {@request}", request);
            var response = await _grpcClient.CurrentShiftAsync(request);
            _logger.LogDebug("Driver Shift Management client CurrentShiftAsync response: {@response}", response);
            return response;
        }
    }
}
