﻿using GrpcCarPool;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Services
{
    public class DriverManagementService : IDriverManagementService
    {
        private readonly DriverManagement.DriverManagementClient _grpcClient;
        private readonly ILogger<DriverManagementService> _logger;

        public DriverManagementService(DriverManagement.DriverManagementClient grpcClient,
            ILogger<DriverManagementService> logger)
        {
            _grpcClient = grpcClient;
            _logger = logger;
        }

        public async Task<OperationResult> ChangeDriverStateAsync(ChangeDriverStateRequest request)
        {
            _logger.LogDebug("DriverManagementService grpc client ChangeDriverStateAsync, request: {@request}", request);
            var response = await _grpcClient.ChangeDriverStateAsync(request);
            _logger.LogDebug("DriverManagementService grpc client ChangeDriverStateAsync response {@response}", response);
            return response;
        }

        public async Task<DriverResponse> GetDriverByIdAsync(GetDriverRequest request)
        {
            _logger.LogDebug("DriverManagementService grpc client GetDriverByIdAsync, request: {@request}", request);
            var response = await _grpcClient.GetDriverByIdAsync(request);
            _logger.LogDebug("DriverManagementService grpc client GetDriverByIdAsync response {@response}", response);
            return response;
        }

        public async Task<DriversResponse> GetDriversAsync(Empty request)
        {
            _logger.LogDebug("DriverManagementService grpc client GetDriversAsync");
            var response = await _grpcClient.GetDriversAsync(request);
            _logger.LogDebug("DriverManagementService grpc client GetDriversAsync response {@response}", response);
            return response;
        }
    }
}
