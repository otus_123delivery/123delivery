﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Taxi123.WebGateway.Auth;

namespace Taxi123.WebGateway.Infrastructure
{
    public class AuthDbContext
        : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>
    {
        public AuthDbContext() { }
        public AuthDbContext(DbContextOptions<AuthDbContext> options)
            : base(options) { }
    }
}
