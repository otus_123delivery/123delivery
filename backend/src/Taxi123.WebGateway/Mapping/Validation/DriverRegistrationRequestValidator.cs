﻿using FluentValidation;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mapping.Validation
{
    public class DriverRegistrationRequestValidator
        : AbstractValidator<DriverRegistrationRequest>
    {
        public DriverRegistrationRequestValidator()
        {
            RuleFor(r => r.Firstname).Matches(@"^[a-zA-Zа-яА-я]{2,15}$");
            RuleFor(r => r.Lastname).Matches(@"^[a-zA-Zа-яА-я]{2,15}$");
            RuleFor(r => r.Phone).Matches(@"^\+7[0-9]{10}$");
            RuleFor(r => r.Password).MinimumLength(7);
            RuleFor(r => r.DriverLicense).NotEmpty();
            RuleFor(r => r.Passport).NotEmpty();
            RuleFor(r => r.Photo).NotEmpty();
        }
    }

}
