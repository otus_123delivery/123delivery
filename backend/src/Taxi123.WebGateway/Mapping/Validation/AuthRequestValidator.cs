﻿using FluentValidation;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mapping.Validation
{
    public class AuthRequestValidator
        : AbstractValidator<AuthRequest>
    {
        public AuthRequestValidator()
        {
            RuleFor(r => r.Phone).Matches(@"^\+7[0-9]{10}$");
            RuleFor(x => x.Password).MinimumLength(7);
        }
    }

}
