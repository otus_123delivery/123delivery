﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.Infrastructure;

namespace Taxi123.WebGateway.Extensions
{
    public static class MigrationManager
    {
        public static IHost MigrateDatabase(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                using var authDbContext = scope.ServiceProvider.GetRequiredService<AuthDbContext>();
                if (authDbContext.Database.GetPendingMigrations().Any())
                    authDbContext.Database.Migrate();
            }
            return host;
        }

        public static IHost ApplyRequiredAuthDbData(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                using var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole<Guid>>>();
                foreach (var roleName in ApplicationRoles.Roles)
                {
                    if (!roleManager.Roles.Any(r => r.Name.Equals(roleName)))
                    {
                        var role = new IdentityRole<Guid>
                        {
                            Id = Guid.NewGuid(),
                            Name = roleName,
                            NormalizedName = roleName
                        };
                        roleManager.CreateAsync(role).Wait();
                    }
                }
            }
            return host;
        }
    }
}
