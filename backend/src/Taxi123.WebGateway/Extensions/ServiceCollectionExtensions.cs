﻿using System;
using System.Text;
using MassTransit;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.Config;
using Taxi123.WebGateway.Infrastructure;
using Taxi123.WebGateway.Services;

namespace Taxi123.WebGateway.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddApplicationAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddIdentity<ApplicationUser, IdentityRole<Guid>>(setup => setup.SignIn.RequireConfirmedEmail = false)
                .AddRoles<IdentityRole<Guid>>()
                .AddEntityFrameworkStores<AuthDbContext>();

            services.AddAuthentication()
                .AddJwtBearer(cfg =>
                {
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = configuration["Tokens:Issuer"],
                        ValidAudience = configuration["Tokens:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]))
                    };
                });
        }

        public static void AddAuthDbContext(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<AuthDbContext>(cfg =>
            {
                cfg.UseSqlServer(configuration.GetConnectionString("AuthDb"));
                cfg.UseLowerCaseNamingConvention();
            });
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddOpenApiDocument(options =>
                {
                    options.Title = "123 Taxi API Doc";
                    options.Version = "1.0";

                    options.OperationProcessors.Add(new OperationSecurityScopeProcessor("auth"));
                    options.DocumentProcessors.Add(new SecurityDefinitionAppender("auth", new OpenApiSecurityScheme
                    {
                        Type = OpenApiSecuritySchemeType.Http,
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Scheme = "bearer",
                        BearerFormat = "jwt"
                    }));
                });
        }

        public static void AddGrpcServices(this IServiceCollection services)
        {
            services.AddTransient<GrpcExceptionInterceptor>();
            services.AddScoped<IRouterService, RouterService>();
            services.AddScoped<ITariffService, TariffService>();
            services.AddScoped<IOrderManagerService, OrderManagerService>();
            services.AddScoped<IDriverManagementService, DriverManagementService>();
            services.AddScoped<IVehicleManagementService, VehicleManagementService>();
            services.AddScoped<IDriverShiftManagementService, DriverShiftManagementService>();
            services.AddScoped<IDriverOrderManagementService, DriverOrderManagementService>();

            services.AddGrpcClient<GrpcRouter.Router.RouterClient>((provider, options) =>
            {
                string routerUri = provider.GetRequiredService<IOptions<UrlsConfig>>().Value.GrpcRouter;
                options.Address = new Uri(routerUri);
            })
            .AddInterceptor<GrpcExceptionInterceptor>();

            services.AddGrpcClient<GrpcRouter.Tariff.TariffClient>((provider, options) =>
            {
                string routerUri = provider.GetRequiredService<IOptions<UrlsConfig>>().Value.GrpcRouter;
                options.Address = new Uri(routerUri);
            })
            .AddInterceptor<GrpcExceptionInterceptor>();

            services.AddGrpcClient<GrpcOrders.OrderManager.OrderManagerClient>((provider, options) =>
            {
                string routerUri = provider.GetRequiredService<IOptions<UrlsConfig>>().Value.GrpcOrders;
                options.Address = new Uri(routerUri);
            })
            .AddInterceptor<GrpcExceptionInterceptor>();

            services.AddGrpcClient<GrpcCarPool.DriverManagement.DriverManagementClient>((provider, options) =>
            {
                string routerUri = provider.GetRequiredService<IOptions<UrlsConfig>>().Value.GrpcCarPool;
                options.Address = new Uri(routerUri);
            })
            .AddInterceptor<GrpcExceptionInterceptor>();

            services.AddGrpcClient<GrpcCarPool.VehicleManagement.VehicleManagementClient>((provider, options) =>
            {
                string routerUri = provider.GetRequiredService<IOptions<UrlsConfig>>().Value.GrpcCarPool;
                options.Address = new Uri(routerUri);
            })
            .AddInterceptor<GrpcExceptionInterceptor>();

            services.AddGrpcClient<GrpcCarPool.DriverShiftManagement.DriverShiftManagementClient>((provider, options) =>
            {
                string routerUri = provider.GetRequiredService<IOptions<UrlsConfig>>().Value.GrpcCarPool;
                options.Address = new Uri(routerUri);
            })
            .AddInterceptor<GrpcExceptionInterceptor>();

            services.AddGrpcClient<GrpcCarPool.DriverOrderManagement.DriverOrderManagementClient>((provider, options) =>
            {
                string routerUri = provider.GetRequiredService<IOptions<UrlsConfig>>().Value.GrpcCarPool;
                options.Address = new Uri(routerUri);
            })
            .AddInterceptor<GrpcExceptionInterceptor>();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IUserManagement, UserManagement>();
        }

        public static void AddEventbus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((_, cfg) =>
                {
                    cfg.Host(configuration["Rabbit:Host"]);
                });
            });
        }
    }
}
