using System;
using System.Reflection;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using HealthChecks.UI.Client;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Taxi123.WebGateway.Config;
using Taxi123.WebGateway.Extensions;
using Taxi123.WebGateway.Mapping;
using Taxi123.WebGateway.Mapping.Converters;

namespace Taxi123.WebGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            IsInDocker = Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") == "true";
        }

        public IConfiguration Configuration { get; }
        public bool IsInDocker { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            if (IsInDocker)
            {
                services.AddHealthChecksUI(setup =>
                    {
                        setup.SetEvaluationTimeInSeconds(90);
                        setup.SetMinimumSecondsBetweenFailureNotifications(60);
                        setup.SetHeaderText("Taxi 123 - Health checks");
                        setup.AddHealthCheckEndpoint("webgateway", $"http://127.0.0.1/health");
                        setup.AddHealthCheckEndpoint("router", "http://router:8080/health");
                        setup.AddHealthCheckEndpoint("orders", "http://orders:8080/health");
                        setup.AddHealthCheckEndpoint("carpool", "http://carpool:8080/health");
                        setup.AddHealthCheckEndpoint("notification", "http://notification/health");
                        setup.AddHealthCheckEndpoint("payment", "http://payment/health");
                        setup.AddHealthCheckEndpoint("signalr", "http://signalr/health");
                    })
                    .AddInMemoryStorage();

                services.AddHealthChecks()
                    .AddSqlServer(Configuration.GetConnectionString("AuthDb"))
                    .AddRabbitMQ(rabbitConnectionString: $"amqp://{Configuration["Rabbit:Host"]}");
            }

            services.AddMediatR(typeof(Startup));
            services.AddControllers()
                .AddJsonOptions(cfg => cfg.JsonSerializerOptions.Converters.Add(new UTCDateTimeConverter()))
                .AddFluentValidation(s => s.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));
            services.AddOptions().Configure<JwtConfig>(Configuration.GetSection(JwtConfig.JwtSection));
            services.Configure<UrlsConfig>(Configuration.GetSection(UrlsConfig.UrlsConfigSection));
            services.AddServices();
            services.AddGrpcServices();
            services.AddAutoMapper(typeof(WebGatewayProfile));
            services.AddApplicationAuthentication(Configuration);
            services.AddAuthDbContext(Configuration);
            services.AddSwagger();
            services.AddEventbus(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                if (IsInDocker)
                {
                    endpoints.MapHealthChecksUI(cfg =>
                    {
                        cfg.UIPath = "/healthcheck";
                        cfg.ResourcesPath = "/healthcheck/ui";
                    });

                    endpoints.MapHealthChecks("/health", new HealthCheckOptions
                    {
                        Predicate = _ => true,
                        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                    });
                }

                endpoints.MapGet("/",
                        context =>
                        {
                            context.Response.Redirect("./swagger/index.html", permanent: false);
                            return Task.CompletedTask;
                        });

                endpoints.MapControllers();
            });
        }
    }
}
