﻿using GrpcCarPool;
using System.Threading.Tasks;

namespace Taxi123.WebGateway.Abstractions
{
    public interface IVehicleManagementService
    {
        Task<OperationResult> DeleteVehicleAsync(DeleteVehicleRequest request);
        Task<OperationResult> EditVehicleAsync(EditVehicleRequest request);
        Task<GetVehicleResponse> GetVehicleAsync(GetVehicleRequest request);
        Task<VehiclesResponse> GetVehiclesAsync(Empty request);
    }
}