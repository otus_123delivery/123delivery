﻿using GrpcOrders;
using System.Threading.Tasks;

namespace Taxi123.WebGateway.Abstractions
{
    public interface IOrderManagerService
    {
        Task<NewOrderResponse> PlaceNewOrderAsync(NewOrderRequest request);
        Task<CancelByCustomerResponse> CancelOrderByCustomerAsync(CancelByCustomerRequest request);
        Task<CurrentOrderResponse> GetCurrentOrderAsync(CustomerOrderRequest request);
        Task<CustomerOrderHistoryResponse> GetOrderHistoryAsync(CustomerOrderRequest request);
        Task<CurrentOrdersResponse> GetCurrentOrdersAsync();
        Task<OrdersArchiveResponse> GetOrdersArchiveAsync(OrdersArchiveRequest request);
        Task<ArchiveOrder> GetArchiveOrderByIdAsync(ArchiveOrderRequest request);
        Task<CurrentOrderResponse> GetCurrentOrderByIdAsync(CurrentOrderRequest request);
        Task<CurrentOrderResponse> GetCurrentOrderByDriverAsync(CurrentOrderByDriverRequest request);
    }
}
