﻿using System.Threading.Tasks;
using GrpcRouter;

namespace Taxi123.WebGateway.Abstractions
{
    public interface IRouterService
    {
        Task<CostResponse> CalculateRouteAsync(CostRequest route);
    }
}
