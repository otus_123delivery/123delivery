﻿using System;
using System.Threading.Tasks;

namespace Taxi123.WebGateway.Abstractions
{
    public interface ITariffService
    {
        Task<(bool, string)> AddTariffAsync(GrpcRouter.TariffDescription tariff);
        Task<GrpcRouter.TariffCollection> GetTariffsAsync(string fromDate, string toDate);
        Task<(bool, string)> UpdateTariffAsync(GrpcRouter.TariffDescription tariffData);
        Task<(bool, string)> DeleteTariffAsync(Guid id);
    }
}