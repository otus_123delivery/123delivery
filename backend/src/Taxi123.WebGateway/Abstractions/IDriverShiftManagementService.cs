﻿using GrpcCarPool;
using System.Threading.Tasks;

namespace Taxi123.WebGateway.Abstractions
{
    public interface IDriverShiftManagementService
    {
        Task<ShiftStateResponse> CurrentShiftAsync(ShiftChangeOperationRequest request);
        Task<ShiftChangeOperationResponse> FinishShiftAsync(ShiftChangeOperationRequest request);
        Task<ShiftChangeOperationResponse> ResumeShiftAsync(ShiftChangeOperationRequest request);
        Task<ShiftChangeOperationResponse> StartShiftAsync(ShiftChangeOperationRequest request);
        Task<ShiftChangeOperationResponse> TakeBreakAsync(ShiftChangeOperationRequest request);
        Task<ShiftChangeOperationResponse> TakeTechBreakAsync(ShiftChangeOperationRequest request);
    }
}
