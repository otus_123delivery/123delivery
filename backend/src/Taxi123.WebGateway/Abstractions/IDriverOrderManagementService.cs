using System.Collections.Generic;
using System.Threading.Tasks;
using GrpcCarPool;
using GrpcOrders;

namespace Taxi123.WebGateway.Abstractions
{
    public interface IDriverOrderManagementService
    {
        Task<OperationResult> PickOrderAsync(PickOrderRequest request);
        Task<OperationResult> ChangeOrderStateAsync(ChangeOrderStateRequest request);
        Task<IEnumerable<CurrentOrderLookupResponse>> GetAvailableOrdersAsync();
        Task<CurrentOrderResponse> GetOrderByIdAsync(CurrentOrderRequest request);
    }
}