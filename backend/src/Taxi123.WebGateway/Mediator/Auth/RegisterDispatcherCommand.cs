﻿using MediatR;
using System;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class RegisterDispatcherCommand : IRequest<(bool, IEnumerable<string>)>
    {
        public RegisterDispatcherCommand(DispatcherRegistrationRequest request) 
            => Data = request ?? throw new ArgumentNullException(nameof(request));
        public DispatcherRegistrationRequest Data { get; }
    }
}
