﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mediator.Auth
{

    public class LoginCommandHandler : IRequestHandler<LoginCommand, (bool,AuthInformationResponse)>
    {
        private readonly IUserManagement _userManagement;

        public LoginCommandHandler(IUserManagement userManagement)
        {
            _userManagement = userManagement;
        }

        public async Task<(bool, AuthInformationResponse)> Handle(LoginCommand request, 
            CancellationToken cancellationToken)
        {
            var result = await _userManagement
                    .LoginAsync(request.Data.Phone, request.Data.Password);

            return result.Item1 && result.Item2.Role.Equals(request.Role)
                ? (true, result.Item2)
                : (false, null);
        }
    }
}
