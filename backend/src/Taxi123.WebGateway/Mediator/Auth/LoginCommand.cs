﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class LoginCommand : IRequest<(bool, AuthInformationResponse)>
    {
        public LoginCommand(AuthRequest authRequest, string role)
        {
            Data = authRequest ?? throw new ArgumentNullException(nameof(authRequest));
            Role = role;
        }

        public AuthRequest Data { get; }
        public string Role { get; }
    }
}
