﻿using AutoMapper;
using MassTransit;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.EventBus.Constants;
using Taxi123.EventBus.Contracts.CarPool;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.Auth;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class RegisterDriverCommandHandler :
        IRequestHandler<RegisterDriverCommand, (bool, IEnumerable<string>)>
    {
        private readonly IUserManagement _userManagement;
        private readonly IMapper _mapper;
        private readonly IBus _bus;

        public RegisterDriverCommandHandler(IUserManagement userManagement,
            IMapper mapper,
            IBus bus)
        {
            _userManagement = userManagement;
            _mapper = mapper;
            _bus = bus;
        }

        public async Task<(bool, IEnumerable<string>)> Handle(RegisterDriverCommand request,
            CancellationToken cancellationToken)
        {
            var user = _mapper.Map<ApplicationUser>(request.Data);
            var result = await _userManagement.RegisterUserAsync(user,
                request.Data.Password, ApplicationRoles.Driver);

            if (!result.Item1) return result;

            var driverRegisteredEp = await _bus.GetSendEndpoint(new Uri($"queue:{CarPoolEndpoints.DriverRegistered}"));
            await driverRegisteredEp.Send<IDriverRegistered>(new
            {
                user.Id,
                user.Firstname,
                user.Lastname,
                Phone = user.UserName,
                user.BirthDate,
                user.DriverLicense,
                user.Passport,
                user.Photo,
                user.CanWork
            }, cancellationToken);

            if (request.Data.Vehicle == null) return result;

            var driverId = await _userManagement.GetUserIdAsync(request.Data.Phone);
            var vehicle = request.Data.Vehicle;
            var vehicleRegisteredEp = await _bus.GetSendEndpoint(new Uri($"queue:{CarPoolEndpoints.VehicleRegistered}"));
            await vehicleRegisteredEp.Send<IVehicleRegistered>(new
            {
                DriverId = driverId,
                vehicle.Photo,
                vehicle.Brand,
                vehicle.Model,
                vehicle.LicensePlate,
                vehicle.VehicleClass
            }, cancellationToken);

            return result;
        }

    }
}
