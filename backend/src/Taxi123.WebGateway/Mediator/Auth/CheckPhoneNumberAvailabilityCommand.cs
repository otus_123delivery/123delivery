﻿using MediatR;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class CheckPhoneNumberAvailabilityCommand : IRequest<bool>
    {
        public CheckPhoneNumberAvailabilityCommand(string phone) => Phone = phone;
        public string Phone { get; }
    }
}
