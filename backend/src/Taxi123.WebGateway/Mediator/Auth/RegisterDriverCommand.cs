﻿using MediatR;
using System;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class RegisterDriverCommand : IRequest<(bool, IEnumerable<string>)>
    {
        public RegisterDriverCommand(DriverRegistrationRequest request) 
            => Data = request ?? throw new ArgumentNullException(nameof(request));
        public DriverRegistrationRequest Data { get; }
    }
}
