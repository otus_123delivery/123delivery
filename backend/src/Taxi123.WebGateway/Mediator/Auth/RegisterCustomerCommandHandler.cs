﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.Auth;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class RegisterCustomerCommandHandler : 
        IRequestHandler<RegisterCustomerCommand, (bool, IEnumerable<string>)>
    {
        private readonly IUserManagement _userManagement;
        private readonly IMapper _mapper;

        public RegisterCustomerCommandHandler(IUserManagement userManagement,
            IMapper mapper)
        {
            _userManagement = userManagement;
            _mapper = mapper;
        }

        public async Task<(bool, IEnumerable<string>)> Handle(RegisterCustomerCommand request, 
            CancellationToken cancellationToken)
        {
            var user = _mapper.Map<ApplicationUser>(request.Data);
            var result = await _userManagement.RegisterUserAsync(user,
                request.Data.Password, ApplicationRoles.Customer);

            return result;
        }
    }
}
