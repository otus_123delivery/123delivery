﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Orders
{
    public class GetArchiveOrderByIdCommandHandler :
        IRequestHandler<GetArchiveOrderByIdCommand, ArchivedOrder>
    {
        private readonly IOrderManagerService _orderManagerService;
        private readonly IMapper _mapper;

        public GetArchiveOrderByIdCommandHandler(IOrderManagerService orderManagerService,
            IMapper mapper)
        {
            _orderManagerService = orderManagerService;
            _mapper = mapper;
        }

        public async Task<ArchivedOrder> Handle(GetArchiveOrderByIdCommand request,
            CancellationToken cancellationToken)
        {
            var grpcResult = await _orderManagerService.GetArchiveOrderByIdAsync(new GrpcOrders.ArchiveOrderRequest
            {
                ArchiveOrderId = request.OrderId.ToString()
            });

            return grpcResult == null
                ? null
                : _mapper.Map<ArchivedOrder>(grpcResult);
        }
    }
}
