﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Orders
{
    public class GetCurrentOrdersCommandHandler :
        IRequestHandler<GetCurrentOrdersCommand, IEnumerable<CurrentOrderLookup>>
    {
        private readonly IOrderManagerService _orderManagerService;
        private readonly IMapper _mapper;

        public GetCurrentOrdersCommandHandler(IOrderManagerService orderManagerService,
            IMapper mapper)
        {
            _orderManagerService = orderManagerService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CurrentOrderLookup>> Handle(GetCurrentOrdersCommand request,
            CancellationToken cancellationToken)
        {
            return _mapper.Map<IEnumerable<CurrentOrderLookup>>((await _orderManagerService.GetCurrentOrdersAsync()).CurrentOrders);
        }
    }
}
