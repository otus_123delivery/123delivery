﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Orders
{
    public class GetCurrentOrderByIdCommandHandler :
        IRequestHandler<GetCurrentOrderByIdCommand, CurrentOrderResponse>
    {
        private readonly IOrderManagerService _orderManagerService;
        private readonly IMapper _mapper;

        public GetCurrentOrderByIdCommandHandler(IOrderManagerService orderManagerService,
            IMapper mapper)
        {
            _orderManagerService = orderManagerService;
            _mapper = mapper;
        }

        public async Task<CurrentOrderResponse> Handle(GetCurrentOrderByIdCommand request,
            CancellationToken cancellationToken)
        {
            var grpcResult = await _orderManagerService
                .GetCurrentOrderByIdAsync(new GrpcOrders.CurrentOrderRequest
                {
                    CurrentOrderId = request.OrderId.ToString()
                });

            return grpcResult == null
                ? null
                : _mapper.Map<CurrentOrderResponse>(grpcResult);
        }
    }
}
