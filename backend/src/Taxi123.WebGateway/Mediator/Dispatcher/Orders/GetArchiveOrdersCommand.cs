﻿using MediatR;
using System;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Orders
{
    public class GetArchiveOrdersCommand :
        IRequest<IEnumerable<ArchivedOrderLookup>>
    {
        public GetArchiveOrdersCommand(DateTime? from, DateTime? to)
        {
            From = from;
            To = to;
        }

        public DateTime? From { get; }
        public DateTime? To { get; }
    }
}
