﻿using MediatR;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Orders
{
    public class GetCurrentOrdersCommand :
        IRequest<IEnumerable<CurrentOrderLookup>>
    {

    }
}
