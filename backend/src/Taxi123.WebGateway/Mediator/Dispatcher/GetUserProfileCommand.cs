﻿using MediatR;
using Taxi123.WebGateway.DTO.Dispatcher;

namespace Taxi123.WebGateway.Mediator.Dispatcher
{
    public class GetUserProfileCommand : 
        IRequest<DispatcherUserProfile>
    {
        public GetUserProfileCommand(string dispatcherId) => DispatcherId = dispatcherId;

        public string DispatcherId { get; }
    }
}
