﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Tariff;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class GetTariffsCommandHandler :
        IRequestHandler<GetTariffsCommand, IEnumerable<TariffScaleDTO>>
    {
        private readonly ITariffService _tariffService;
        private readonly IMapper _mapper;

        public GetTariffsCommandHandler(ITariffService tariffService,
            IMapper mapper)
        {
            _tariffService = tariffService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TariffScaleDTO>> Handle(GetTariffsCommand request,
            CancellationToken cancellationToken)
        {
            var result = await _tariffService.GetTariffsAsync(
                        request.Data.From.HasValue ? request.Data.From.Value.ToString(CultureInfo.InvariantCulture) : "",
                        request.Data.To.HasValue ? request.Data.To.Value.ToString(CultureInfo.InvariantCulture) : "");

            return result.Tariffs.Select(d => _mapper.Map<TariffScaleDTO>(d)).ToArray();
        }
    }
}
