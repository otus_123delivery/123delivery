﻿using MediatR;
using System;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class DeleteTariffCommand :
        IRequest<(bool, string)>
    {
        public DeleteTariffCommand(Guid tariffId) => TariffId = tariffId;

        public Guid TariffId { get; }
    }
}
