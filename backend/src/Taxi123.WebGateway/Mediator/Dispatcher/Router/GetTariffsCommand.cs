﻿using MediatR;
using System;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Tariff;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class GetTariffsCommand :
        IRequest<IEnumerable<TariffScaleDTO>>
    {
        public GetTariffsCommand(TariffLookupRequest lookupRequest) 
            => Data = lookupRequest ?? throw new ArgumentNullException(nameof(lookupRequest));

        public TariffLookupRequest Data { get; }
    }
}
