﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class UpdateTariffCommandHandler :
        IRequestHandler<UpdateTariffCommand, (bool, string)>
    {
        private readonly ITariffService _tariffService;
        private readonly IMapper _mapper;

        public UpdateTariffCommandHandler(ITariffService tariffService,
            IMapper mapper)
        {
            _tariffService = tariffService;
            _mapper = mapper;
        }

        public async Task<(bool, string)> Handle(UpdateTariffCommand request, CancellationToken cancellationToken)
        {
            var mapped = _mapper.Map<GrpcRouter.TariffDescription>(request.Data);
            return await _tariffService.UpdateTariffAsync(mapped);
        }
    }
}
