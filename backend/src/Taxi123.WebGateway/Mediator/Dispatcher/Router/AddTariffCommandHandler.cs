﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class AddTariffCommandHandler :
        IRequestHandler<AddTariffCommand, (bool, string)>
    {
        private readonly ITariffService _tariffService;
        private readonly IMapper _mapper;

        public AddTariffCommandHandler(ITariffService tariffService,
            IMapper mapper)
        {
            _tariffService = tariffService;
            _mapper = mapper;
        }

        public async Task<(bool, string)> Handle(AddTariffCommand request,
            CancellationToken cancellationToken)
        {
            return await _tariffService.AddTariffAsync(_mapper.Map<GrpcRouter.TariffDescription>(request.Data));
        }
    }
}
