﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetDriverByIdCommand : 
        IRequest<DriverProfile>
    {
        public GetDriverByIdCommand(Guid driverId) => DriverId = driverId;
        public Guid DriverId { get; }
    }
}
