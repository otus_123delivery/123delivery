﻿using MediatR;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetDriversCommand : 
        IRequest<IEnumerable<DriverShortResponse>>
    {
    }
}
