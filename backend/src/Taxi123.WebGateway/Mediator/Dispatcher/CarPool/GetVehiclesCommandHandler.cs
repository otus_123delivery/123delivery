﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetVehiclesCommandHandler : 
        IRequestHandler<GetVehiclesCommand, IEnumerable<VehicleShortResponse>>
    {
        private readonly IVehicleManagementService _vehicleManagement;
        private readonly IMapper _mapper;

        public GetVehiclesCommandHandler(IVehicleManagementService vehicleManagement,
            IMapper mapper)
        {
            _vehicleManagement = vehicleManagement;
            _mapper = mapper;
        }

        public async Task<IEnumerable<VehicleShortResponse>> Handle(GetVehiclesCommand request, 
            CancellationToken cancellationToken)
        {
            var result = await _vehicleManagement.GetVehiclesAsync(new GrpcCarPool.Empty());
            return result != null ? _mapper.Map<IEnumerable<VehicleShortResponse>>(result.Vehicles) : null;
        }
    }
}
