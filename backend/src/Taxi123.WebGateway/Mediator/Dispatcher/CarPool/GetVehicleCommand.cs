﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetVehicleCommand :
        IRequest<VehicleFullResponse>
    {
        public GetVehicleCommand(Guid vehicleId)
            => VehicleId = vehicleId;
        public Guid VehicleId { get; }
    }
}
