﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class EditVehicleCommandHandler : 
        IRequestHandler<EditVehicleCommand, bool>
    {
        private readonly IVehicleManagementService _vehicleManagement;

        public EditVehicleCommandHandler(IVehicleManagementService vehicleManagement)
            => _vehicleManagement = vehicleManagement;

        public async Task<bool> Handle(EditVehicleCommand request, 
            CancellationToken cancellationToken)
        {
            var result = await _vehicleManagement.EditVehicleAsync(new GrpcCarPool.EditVehicleRequest
            {
                Brand = request.EditInfo.Brand,
                LicensePlate = request.EditInfo.LicensePlate,
                Model = request.EditInfo.Model,
                Photo = request.EditInfo.Photo,
                VehicleClass = request.EditInfo.VehicleClass,
                VehicleId = request.EditInfo.Id.ToString()
            });

            return result?.Success ?? false;
        }
    }
}
