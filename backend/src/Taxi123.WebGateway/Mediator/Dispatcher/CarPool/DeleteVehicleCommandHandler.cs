﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class DeleteVehicleCommandHandler : 
        IRequestHandler<DeleteVehicleCommand, bool>
    {
        private readonly IVehicleManagementService _vehicleManagementService;

        public DeleteVehicleCommandHandler(IVehicleManagementService vehicleManagementService)
            => _vehicleManagementService = vehicleManagementService;

        public async Task<bool> Handle(DeleteVehicleCommand request, 
            CancellationToken cancellationToken)
        {
            var result = await _vehicleManagementService.DeleteVehicleAsync(
                new GrpcCarPool.DeleteVehicleRequest
                {
                    VehicleId = request.VehicleId.ToString()
                });

            return result?.Success ?? false;
        }
    }
}
