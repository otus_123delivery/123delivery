﻿using MediatR;
using System;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class ChangeDriverStateCommand :
        IRequest<bool>
    {
        public ChangeDriverStateCommand(Guid driverId,
            Guid? vehicleId,
            bool canWork)
        {
            DriverId = driverId;
            VehicleId = vehicleId;
            CanWork = canWork;
        }
        public Guid DriverId { get; }
        public Guid? VehicleId { get; }
        public bool CanWork { get; }
    }
}
