﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetDriverByIdCommandHandler : 
        IRequestHandler<GetDriverByIdCommand, DriverProfile>
    {
        private readonly IMapper _mapper;
        private readonly ILogger<GetDriverByIdCommandHandler> _logger;
        private readonly IDriverManagementService _driverManagement;

        public GetDriverByIdCommandHandler(IMapper mapper,
            ILogger<GetDriverByIdCommandHandler> logger,
            IDriverManagementService driverManagement)
        {
            _mapper = mapper;
            _logger = logger;
            _driverManagement = driverManagement;
        }

        public async Task<DriverProfile> Handle(GetDriverByIdCommand request, 
            CancellationToken cancellationToken)
        {
            try
            {
                var driver = await _driverManagement.GetDriverByIdAsync(new GrpcCarPool.GetDriverRequest
                {
                    DriverId = request.DriverId.ToString()
                });
                return _mapper.Map<DriverProfile>(driver);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetDriverByIdCommandHandler: {@ex}", ex);
                throw;
            }
        }
    }
}
