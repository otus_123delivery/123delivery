﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetVehicleCommandHandler :
        IRequestHandler<GetVehicleCommand, VehicleFullResponse>
    {
        private readonly IVehicleManagementService _vehicleManagement;
        private readonly IMapper _mapper;

        public GetVehicleCommandHandler(IVehicleManagementService vehicleManagement,
            IMapper mapper)
        {
            _vehicleManagement = vehicleManagement;
            _mapper = mapper;
        }

        public async Task<VehicleFullResponse> Handle(GetVehicleCommand request, 
            CancellationToken cancellationToken)
        {
            var vehicle = await _vehicleManagement
                .GetVehicleAsync(new GrpcCarPool.GetVehicleRequest
            {
                VehicleId = request.VehicleId.ToString()
            });

            return _mapper.Map<VehicleFullResponse>(vehicle);
        }
    }
}
