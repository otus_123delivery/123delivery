﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Driver
{
    public class GetDriverShiftStateCommandHandler :
        IRequestHandler<GetDriverShiftStateCommand, int?>
    {
        private readonly IDriverShiftManagementService _shiftManagementService;

        public GetDriverShiftStateCommandHandler(IDriverShiftManagementService shiftManagementService)
        {
            _shiftManagementService = shiftManagementService;
        }

        public async Task<int?> Handle(GetDriverShiftStateCommand request, CancellationToken cancellationToken)
        {
            var result = await _shiftManagementService.CurrentShiftAsync(new GrpcCarPool.ShiftChangeOperationRequest { DriverId = request.DriverId.ToString() });
            return result == null ? null : (int)result.Result;
        }
    }
}
