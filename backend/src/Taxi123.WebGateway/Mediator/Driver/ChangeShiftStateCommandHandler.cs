﻿using GrpcCarPool;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Driver
{

    public class ChangeShiftStateCommandHandler :
        IRequestHandler<ChangeShiftStateCommand, bool?>
    {
        private readonly IDriverShiftManagementService _shiftManagement;

        public ChangeShiftStateCommandHandler(IDriverShiftManagementService shiftManagement)
        {
            _shiftManagement = shiftManagement;
        }

        public async Task<bool?> Handle(ChangeShiftStateCommand request, CancellationToken cancellationToken)
        {
            ShiftChangeOperationResponse result;

            switch (request.ShiftOperation)
            {
                case ChangeShiftStateCommand.Operation.Start:
                    result = await _shiftManagement.StartShiftAsync(new ShiftChangeOperationRequest { DriverId = request.DriverId.ToString() });
                    break;
                case ChangeShiftStateCommand.Operation.Resume:
                    result = await _shiftManagement.ResumeShiftAsync(new ShiftChangeOperationRequest { DriverId = request.DriverId.ToString() });
                    break;
                case ChangeShiftStateCommand.Operation.Break:
                    result = await _shiftManagement.TakeBreakAsync(new ShiftChangeOperationRequest { DriverId = request.DriverId.ToString() });
                    break;
                case ChangeShiftStateCommand.Operation.TechBreak:
                    result = await _shiftManagement.TakeTechBreakAsync(new ShiftChangeOperationRequest { DriverId = request.DriverId.ToString() });
                    break;
                case ChangeShiftStateCommand.Operation.Finish:
                    result = await _shiftManagement.FinishShiftAsync(new ShiftChangeOperationRequest { DriverId = request.DriverId.ToString() });
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(request.ShiftOperation));
            }

            return result?.Result;
        }
    }
}
