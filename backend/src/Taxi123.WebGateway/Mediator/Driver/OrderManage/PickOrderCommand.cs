using System;
using MediatR;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class PickOrderCommand : IRequest<bool?>
    {
        public PickOrderCommand(Guid orderId,
            string driverId)
        {
            OrderId = orderId;
            DriverId = Guid.Parse(driverId);
        }
        
        public Guid OrderId { get; }
        public Guid DriverId { get; }
    }
}