﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class GetCurrentOrderCommand : IRequest<AvailableOrderResponse>
    {
        public GetCurrentOrderCommand(string driverId) => DriverId = Guid.Parse(driverId);

        public Guid DriverId { get; }
    }
}
