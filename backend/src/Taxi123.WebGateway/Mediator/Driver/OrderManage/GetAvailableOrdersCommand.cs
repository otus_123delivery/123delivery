﻿using MediatR;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class GetAvailableOrdersCommand : 
        IRequest<IEnumerable<AvailableOrderShortResponse>>
    {
    }
}
