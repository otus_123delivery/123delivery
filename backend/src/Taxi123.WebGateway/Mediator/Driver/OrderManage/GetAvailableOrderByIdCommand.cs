﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class GetAvailableOrderByIdCommand :
        IRequest<AvailableOrderResponse>
    {
        public GetAvailableOrderByIdCommand(Guid orderId) => OrderId = orderId;

        public Guid OrderId { get; }
    }
}
