﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class GetAvailableOrdersCommandHandler : 
        IRequestHandler<GetAvailableOrdersCommand, IEnumerable<AvailableOrderShortResponse>>
    {
        private readonly IDriverOrderManagementService _orderManagementService;
        private readonly IMapper _mapper;

        public GetAvailableOrdersCommandHandler(IDriverOrderManagementService orderManagementService,
            IMapper mapper)
        {
            _orderManagementService = orderManagementService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<AvailableOrderShortResponse>> Handle(GetAvailableOrdersCommand request, 
            CancellationToken cancellationToken)
        {
            var orders = await _orderManagementService.GetAvailableOrdersAsync();
            return _mapper.Map<IEnumerable<AvailableOrderShortResponse>>(orders);
        }
    }
}
