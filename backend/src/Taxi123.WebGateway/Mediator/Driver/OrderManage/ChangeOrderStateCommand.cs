using System;
using MediatR;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class ChangeOrderStateCommand : IRequest<bool?>
    {
        public ChangeOrderStateCommand(Guid driverId,
            Guid orderId,
            DriverOrderStateOperation operation)
        {
            DriverId = driverId;
            OrderId = orderId;
            Operation = operation;
        }

        public Guid DriverId { get; }
        public Guid OrderId { get; }
        public DriverOrderStateOperation Operation { get; }
    }
    
    public enum DriverOrderStateOperation
    {
        Perform = 0,
        OnLocation = 1,
        Abandon = 2,
        Cancel = 3
    }
}