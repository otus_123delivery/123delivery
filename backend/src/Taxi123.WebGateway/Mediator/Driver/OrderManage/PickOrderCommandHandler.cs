using System.Threading;
using System.Threading.Tasks;
using GrpcCarPool;
using MediatR;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class PickOrderCommandHandler : 
        IRequestHandler<PickOrderCommand, bool?>
    {
        private readonly IDriverOrderManagementService _orderManagementService;

        public PickOrderCommandHandler(IDriverOrderManagementService orderManagementService)
        {
            _orderManagementService = orderManagementService;
        }
        
        public async Task<bool?> Handle(PickOrderCommand request, CancellationToken cancellationToken)
        {
            var result = await _orderManagementService.PickOrderAsync(new PickOrderRequest
            {
                DriverId = request.DriverId.ToString(),
                OrderId = request.OrderId.ToString()
            });

            return result?.Success;
        }
    }
}