﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{

    public class GetCurrentOrderCommandHandler :
        IRequestHandler<GetCurrentOrderCommand, AvailableOrderResponse>
    {
        private readonly IMapper _mapper;
        private readonly IOrderManagerService _orderManagementService;

        public GetCurrentOrderCommandHandler(IMapper mapper,
            IOrderManagerService orderManagementService)
        {
            _mapper = mapper;
            _orderManagementService = orderManagementService;
        }

        public async Task<AvailableOrderResponse> Handle(GetCurrentOrderCommand request, CancellationToken cancellationToken)
        {
            var order = await _orderManagementService.GetCurrentOrderByDriverAsync(new GrpcOrders.CurrentOrderByDriverRequest 
            { 
                DriverId = request.DriverId.ToString() 
            });

            return _mapper.Map<AvailableOrderResponse>(order);
        }
    }
}
