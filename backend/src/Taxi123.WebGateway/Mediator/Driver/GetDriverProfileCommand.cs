﻿using MediatR;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver
{
    public class GetDriverProfileCommand : IRequest<DriverUserProfile>
    {
        public GetDriverProfileCommand(string driverId) => DriverId = driverId;

        public string DriverId { get; }
    }
}
