﻿using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver
{
    public class GetDriverProfileCommandHandler : 
        IRequestHandler<GetDriverProfileCommand, DriverUserProfile>
    {
        private readonly IUserManagement _userManagement;
        private readonly IMapper _mapper;

        public GetDriverProfileCommandHandler(IUserManagement userManagement,
            IMapper mapper)
        {
            _userManagement = userManagement;
            _mapper = mapper;
        }

        public async Task<DriverUserProfile> Handle(GetDriverProfileCommand request, 
            CancellationToken cancellationToken)
        {
            var profile = await _userManagement.GetUserProfile(Guid.Parse(request.DriverId));
            return _mapper.Map<DriverUserProfile>(profile);
        }
    }
}
