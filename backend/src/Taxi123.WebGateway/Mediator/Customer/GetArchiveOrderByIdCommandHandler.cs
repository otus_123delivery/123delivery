﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetArchiveOrderByIdCommandHandler : IRequestHandler<GetArchiveOrderByIdCommand, ArchivedOrder>
    {
        private readonly IOrderManagerService _orderManagerService;
        private readonly IMapper _mapper;

        public GetArchiveOrderByIdCommandHandler(IOrderManagerService orderManagerService,
            IMapper mapper)
        {
            _orderManagerService = orderManagerService;
            _mapper = mapper;
        }

        public async Task<ArchivedOrder> Handle(GetArchiveOrderByIdCommand request, 
            CancellationToken cancellationToken)
        {
            var order = await _orderManagerService.GetArchiveOrderByIdAsync(new GrpcOrders.ArchiveOrderRequest
            {
                ArchiveOrderId = request.OrderId.ToString()
            });

            return order != null ? _mapper.Map<ArchivedOrder>(order) : null;
        }
    }
}
