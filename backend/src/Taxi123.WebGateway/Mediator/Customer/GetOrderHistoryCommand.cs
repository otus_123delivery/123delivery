﻿using MediatR;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetOrderHistoryCommand : 
        IRequest<IEnumerable<ArchivedOrderLookup>>
    {
        public GetOrderHistoryCommand(string customerId) => CustomerId = customerId;

        public string CustomerId { get; }
    }
}
