﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetArchiveOrderByIdCommand : IRequest<ArchivedOrder>
    {
        public GetArchiveOrderByIdCommand(Guid orderId) => OrderId = orderId;

        public Guid OrderId { get; }
    }
}
