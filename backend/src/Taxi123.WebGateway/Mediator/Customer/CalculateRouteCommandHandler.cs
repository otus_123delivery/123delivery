﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class CalculateRouteCommandHandler : 
        IRequestHandler<CalculateRouteCommand, CostResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRouterService _routerService;

        public CalculateRouteCommandHandler(IMapper mapper,
            IRouterService routerService)
        {
            _mapper = mapper;
            _routerService = routerService;
        }

        public async Task<CostResponse> Handle(CalculateRouteCommand request, CancellationToken cancellationToken) 
            => _mapper.Map<CostResponse>(await _routerService.CalculateRouteAsync(_mapper.Map<GrpcRouter.CostRequest>(request.Data)));
    }
}
