﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using static GrpcOrders.NewOrderResponse.Types;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class PlaceNewOrderCommandHandler : 
        IRequestHandler<PlaceNewOrderCommand, OrderOperationResult>
    {
        private readonly IOrderManagerService _orderManagerService;

        public PlaceNewOrderCommandHandler(IOrderManagerService orderManagerService)
        {
            _orderManagerService = orderManagerService;
        }

        public async Task<OrderOperationResult> Handle(PlaceNewOrderCommand request, 
            CancellationToken cancellationToken)
        {
            var grpcOrderRequest = new GrpcOrders.NewOrderRequest
            {
                ByTheTime = request.Data.ByTheTime.ToString(),
                FromLatitude = request.Data.FromLatitude,
                FromLongitude = request.Data.FromLongitude,
                ToLatitude = request.Data.ToLatitude,
                ToLongitude = request.Data.ToLongitude,
                VehicleClassPreference = request.Data.VehicleClassPreference,
                Cost = request.Cost,
                UserId = request.CustomerId,
                UserPhone = request.Phone
            };

            var orderResult = await _orderManagerService.PlaceNewOrderAsync(grpcOrderRequest);

            return (OrderOperationResult)orderResult.Result;
        }
    }
}
