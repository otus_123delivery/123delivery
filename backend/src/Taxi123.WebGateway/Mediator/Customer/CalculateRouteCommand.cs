﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class CalculateRouteCommand : IRequest<CostResponse>
    {
        public CalculateRouteCommand(CostRequest request) 
            => Data = request ?? throw new ArgumentNullException(nameof(request));

        public CostRequest Data { get; }
    }
}
