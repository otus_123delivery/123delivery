﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetOrderHistoryCommandHandler : 
        IRequestHandler<GetOrderHistoryCommand, IEnumerable<ArchivedOrderLookup>>
    {
        private readonly IOrderManagerService _orderManagerService;
        private readonly IMapper _mapper;

        public GetOrderHistoryCommandHandler(IOrderManagerService orderManagerService,
            IMapper mapper)
        {
            _orderManagerService = orderManagerService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ArchivedOrderLookup>> Handle(GetOrderHistoryCommand request, CancellationToken cancellationToken)
        {
            var orders = await _orderManagerService.GetOrderHistoryAsync(new GrpcOrders.CustomerOrderRequest { CustomerId = request.CustomerId });

            return orders.Orders.Count > 0 
                ? _mapper.Map<IEnumerable<ArchivedOrderLookup>>(orders.Orders)
                : Enumerable.Empty<ArchivedOrderLookup>();
        }
    }
}
