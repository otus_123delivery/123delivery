﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Order;
using static GrpcOrders.NewOrderResponse.Types;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class PlaceNewOrderCommand : 
        IRequest<OrderOperationResult>
    {
        public PlaceNewOrderCommand(NewOrderRequest newOrderRequest, 
            string customerId, string phone, int cost)
        {
            Data = newOrderRequest ?? throw new ArgumentNullException(nameof(newOrderRequest));
            Cost = cost;
            Phone = phone;
            CustomerId = customerId;
        }

        public string CustomerId { get; }
        public NewOrderRequest Data { get; }
        public string Phone { get; }
        public int Cost { get; }
    }
}
