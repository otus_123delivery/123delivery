﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Customer;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetProfileCommand : IRequest<CustomerUserProfile>
    {
        public GetProfileCommand(Guid customerId) => CustomerId = customerId;
        public Guid CustomerId { get; }
    }
}
