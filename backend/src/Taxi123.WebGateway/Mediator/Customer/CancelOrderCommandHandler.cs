﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class CancelOrderCommandHandler : IRequestHandler<CancelOrderCommand, bool>
    {
        private readonly IOrderManagerService _orderManagerService;

        public CancelOrderCommandHandler(IOrderManagerService orderManagerService) 
            => _orderManagerService = orderManagerService;

        public async Task<bool> Handle(CancelOrderCommand request, 
            CancellationToken cancellationToken)
        {
            var result = await _orderManagerService
                .CancelOrderByCustomerAsync(new GrpcOrders.CancelByCustomerRequest { UserId = request.CustomerId });

            return result.Result;
        }
    }
}
