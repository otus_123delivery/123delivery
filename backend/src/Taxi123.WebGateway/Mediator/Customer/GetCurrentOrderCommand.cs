﻿using MediatR;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetCurrentOrderCommand : 
        IRequest<CurrentOrderResponse>
    {
        public GetCurrentOrderCommand(string customerId) => CustomerId = customerId;

        public string CustomerId { get; }
    }
}
