﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Customer;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetProfileCommandHandler : 
        IRequestHandler<GetProfileCommand, CustomerUserProfile>
    {
        private readonly IUserManagement _userManagement;
        private readonly IMapper _mapper;

        public GetProfileCommandHandler(IUserManagement userManagement,
            IMapper mapper)
        {
            _userManagement = userManagement;
            _mapper = mapper;
        }

        public async Task<CustomerUserProfile> Handle(GetProfileCommand request, 
            CancellationToken cancellationToken)
        {
            return _mapper.Map<CustomerUserProfile>(await _userManagement.GetUserProfile(request.CustomerId));
        }
    }
}
