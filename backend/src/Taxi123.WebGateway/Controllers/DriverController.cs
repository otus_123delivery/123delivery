using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MassTransit.Futures.Contracts;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.DTO.Driver;
using Taxi123.WebGateway.Mediator.Driver;
using Taxi123.WebGateway.Mediator.Driver.OrderManage;

namespace Taxi123.WebGateway.Controllers
{
    [ApiController]
    [Route("api/v1/driver")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoles.Driver)]
    public class DriverController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DriverController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("profile")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DriverUserProfile))]
        public async Task<IActionResult> GetUserProfileAsync()
        {
            GetClaims(out _, out string id);
            return Ok(await _mediator.Send(new GetDriverProfileCommand(id)));
        }

        [HttpPost("shift/start")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> StartShiftAsync()
        {
            GetClaims(out _, out string id);
            var result = await _mediator.Send(new ChangeShiftStateCommand(Guid.Parse(id), 
                ChangeShiftStateCommand.Operation.Start));
            return result != null && result.Value ? Ok() : BadRequest();
        }

        [HttpPost("shift/tech-break")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> TakeTechnicalBreakAsync()
        {
            GetClaims(out _, out string id);
            var result = await _mediator.Send(new ChangeShiftStateCommand(Guid.Parse(id),
                ChangeShiftStateCommand.Operation.TechBreak));
            return result != null && result.Value ? Ok() : BadRequest();
        }

        [HttpPost("shift/break")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> TakeBreakAsync()
        {
            GetClaims(out _, out string id);
            var result = await _mediator.Send(new ChangeShiftStateCommand(Guid.Parse(id), 
                ChangeShiftStateCommand.Operation.Break));
            return result != null && result.Value ? Ok() : BadRequest();
        }

        [HttpPost("shift/resume")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ResumeShiftAsync()
        {
            GetClaims(out _, out string id);
            var result = await _mediator.Send(new ChangeShiftStateCommand(Guid.Parse(id), 
                ChangeShiftStateCommand.Operation.Resume));
            return result != null && result.Value ? Ok() : BadRequest();
        }

        [HttpGet("shift")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCurrentShiftAsync()
        {
            GetClaims(out _, out string id);
            var result = await _mediator.Send(new GetDriverShiftStateCommand(Guid.Parse(id)));
            return result != null
                ? Ok(new CurrentShiftStateResponse { State = result.Value })
                : BadRequest();
        }

        [HttpPost("shift/finish")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> FinishShiftAsync()
        {
            GetClaims(out _, out string id);
            var result = await _mediator.Send(new ChangeShiftStateCommand(Guid.Parse(id), 
                ChangeShiftStateCommand.Operation.Finish));
            return result != null && result.Value ? Ok() : BadRequest();
        }

        [HttpGet("orders/available")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAvailableOrdersAsync([FromHeader] double radius = 10000)
        {
            return Ok(await _mediator.Send(new GetAvailableOrdersCommand()));
        }

        [HttpGet("orders/current")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AvailableOrderResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCurrentOrderAsync()
        {
            GetClaims(out _, out string driverId);
            var order = await _mediator.Send(new GetCurrentOrderCommand(driverId));
            return order == null
                ? NotFound()
                : Ok(order);
        }

        [HttpGet("orders/{orderId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetOrderByIdAsync([FromRoute]Guid orderId)
        {
            var order = await _mediator.Send(new GetAvailableOrderByIdCommand(orderId));
            return order == null 
                ? NotFound()
                : Ok(order);
        }

        [HttpGet("orders/archive")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public Task<IActionResult> GetOrdersArchiveAsync([FromHeader] DateTime? from = null,
            [FromHeader] DateTime? to = null)
        {
            return Task.FromResult(Ok() as IActionResult);
        }

        [HttpPost("orders/manage/take/{orderId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> TakeOrderAsync([FromRoute]Guid orderId)
        {
            GetClaims(out _, out string driverId);
            var result = await _mediator.Send(new PickOrderCommand(orderId, driverId));
            return result == null 
                ? BadRequest() 
                : Ok(new DriverOrderActionResponse(result.Value));
        }

        [HttpPost("orders/manage/abandon/{orderId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AbandonCurrentOrderAsync([FromRoute]Guid orderId)
        {
            GetClaims(out _, out string driverId);
            var result =
                await _mediator.Send(new ChangeOrderStateCommand(Guid.Parse(driverId), orderId, 
                    DriverOrderStateOperation.Abandon));
            return result == null 
                ? BadRequest()
                : Ok(new DriverOrderActionResponse(result.Value));
        }

        [HttpPost("orders/manage/cancel/{orderId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CancelCurrentOrderAsync([FromRoute]Guid orderId)
        {
            GetClaims(out _, out string driverId);
            var result =
                await _mediator.Send(new ChangeOrderStateCommand(Guid.Parse(driverId), orderId,
                    DriverOrderStateOperation.Cancel));
            return result == null 
                ? BadRequest()
                : Ok(new DriverOrderActionResponse(result.Value));
        }

        [HttpPost("orders/manage/perform/{orderId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PerformOrderAsync([FromRoute] Guid orderId)
        {
            GetClaims(out _, out string driverId);
            var result =
                await _mediator.Send(new ChangeOrderStateCommand(Guid.Parse(driverId), orderId,
                    DriverOrderStateOperation.Perform));
            return result == null
                ? BadRequest()
                : Ok(new DriverOrderActionResponse(result.Value));
        }

        [HttpPost("orders/manage/on-location/{orderId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ArrivalReportAsync([FromRoute]Guid orderId)
        {
            GetClaims(out _, out string driverId);
            var result =
                await _mediator.Send(new ChangeOrderStateCommand(Guid.Parse(driverId), orderId,
                    DriverOrderStateOperation.OnLocation));
            return result == null 
                ? BadRequest()
                : Ok(new DriverOrderActionResponse(result.Value));
        }

        private void GetClaims(out string phone, out string id)
        {
            phone = User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            id = User.Claims.Single(c => c.Type == ClaimTypes.Name).Value;
        }
    }
}