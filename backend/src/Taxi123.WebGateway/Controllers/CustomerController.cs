﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.DTO.Customer;
using Taxi123.WebGateway.DTO.Order;
using Taxi123.WebGateway.Mediator.Customer;
using static GrpcOrders.NewOrderResponse.Types;

namespace Taxi123.WebGateway.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoles.Customer)]
    [Route("api/v1/customer")]
    public class CustomerController : ControllerBase
    {
        private readonly ILogger<CustomerController> _logger;
        private readonly IMediator _mediator;

        public CustomerController(ILogger<CustomerController> logger,
            IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost("order/calculate")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CostResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(DTO.ErrorDescriptionResponse))]
        public async Task<IActionResult> CalculateRouteAsync(CostRequest costRequest)
        {
            try
            {
                return Ok(await _mediator.Send(new CalculateRouteCommand(costRequest)));
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest(new DTO.ErrorDescriptionResponse { Error = "Произошла ошибка." });
            }
        }

        [HttpPost("order/new-order")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CostResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(DTO.ErrorDescriptionResponse))]
        public async Task<IActionResult> PlaceOrderAsync(NewOrderRequest orderRequest)
        {
            try
            {
                var calculationResult = await _mediator.Send(new CalculateRouteCommand(new CostRequest
                {
                    FromLatitude = orderRequest.FromLatitude,
                    FromLongitude = orderRequest.FromLongitude,
                    ToLatitude = orderRequest.ToLatitude,
                    ToLongitude = orderRequest.ToLongitude
                }));
                
                if (!calculationResult.Success) return BadRequest(new DTO.ErrorDescriptionResponse { Error = "Не удалось разместить заказ." });

                GetClaims(out string phone, out string id);

                var orderOperationResult = await _mediator.Send(new PlaceNewOrderCommand(orderRequest, id, phone, calculationResult.Cost));

                return orderOperationResult switch
                {
                    OrderOperationResult.Success => Ok(new CostResponse { Cost = calculationResult.Cost, Success = true }),
                    OrderOperationResult.AlreadyCreated => Accepted(new DTO.ErrorDescriptionResponse { Error = "На ваш номер зарегистрирован незавершенный заказ." }),
                    OrderOperationResult.Failure => BadRequest(new DTO.ErrorDescriptionResponse { Error = "Не удалось разместить заказ." }),
                    _ => throw new ArgumentOutOfRangeException(nameof(orderOperationResult)),
                };
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest(new DTO.ErrorDescriptionResponse { Error = "Произошла ошибка." });
            }
        }

        [HttpPost("order/cancel")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(DTO.ErrorDescriptionResponse))]
        public async Task<IActionResult> CancelOrderAsync()
        {
            try
            {
                GetClaims(out _, out string id);
                var result = await _mediator.Send(new CancelOrderCommand(id));

                return result ? Ok() : BadRequest(new DTO.ErrorDescriptionResponse { Error = "Не удалось отменить заказ." });
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest(new DTO.ErrorDescriptionResponse { Error = "Произошла ошибка при попытке отменить заказ." });
            }
        }

        [HttpPost("order/current")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CurrentOrderResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCurrentOrderAsync()
        {
            try
            {
                GetClaims(out _, out string id);
                var result = await _mediator.Send(new GetCurrentOrderCommand(id));

                return result != null ? Ok(result) : NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest();
            }
        }

        [HttpGet("profile")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerUserProfile))]
        public async Task<IActionResult> GetUserProfileAsync()
        {
            GetClaims(out _, out string id);
            return Ok(await _mediator.Send(new GetProfileCommand(Guid.Parse(id))));
        }

        [HttpGet("order-history")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ArchivedOrderLookup>))]
        public async Task<IActionResult> GetOrderHistoryAsync()
        {
            GetClaims(out _, out string id);
            return Ok(await _mediator.Send(new GetOrderHistoryCommand(id)));
        }

        [HttpGet("order-history/{orderId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ArchivedOrder))]
        public async Task<IActionResult> GetArchivedOrderById([FromRoute]Guid orderId)
        {
            var response = await _mediator.Send(new GetArchiveOrderByIdCommand(orderId));

            return response != null 
                ? Ok(response) 
                : NotFound();
        }

        private void GetClaims(out string phone, out string id)
        {
            phone = User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            id = User.Claims.Single(c => c.Type == ClaimTypes.Name).Value;
        }
    }
}

