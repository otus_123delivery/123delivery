﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.DTO.Auth;
using Taxi123.WebGateway.Mediator.Auth;

namespace Taxi123.WebGateway.Controllers
{
    [ApiController]
    [Route("api/v1")]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IMediator _mediator;

        public AuthController(ILogger<AuthController> logger,
            IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost("driver/register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(IEnumerable<string>))]
        public async Task<IActionResult> RegisterDriverAsync(DriverRegistrationRequest request)
        {
            try
            {
                var result = await _mediator.Send(new RegisterDriverCommand(request));
                return result.Item1 ? Ok() : BadRequest(result.Item2);
            }
            catch (Exception ex)
            {
                _logger.LogError("Registration error: {@ex}", ex);
                return BadRequest("Unknown error");
            }
        } 

        [HttpPost("customer/register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(IEnumerable<string>))]
        public async Task<IActionResult> RegisterCustomerAsync(CustomerRegistrationRequest request)
        {
            try
            {
                var result = await _mediator.Send(new RegisterCustomerCommand(request));
                return result.Item1 ? Ok() : BadRequest(result.Item2);
            }
            catch (Exception ex)
            {
                _logger.LogError("Registering error: {@ex}", ex);
                return BadRequest(new [] { "Unknown error" });
            }
        }

        [HttpPost("dispatcher/register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(IEnumerable<string>))]
        public async Task<IActionResult> RegisterDispatcherAsync(DispatcherRegistrationRequest request)
        {
            try
            {
                var result = await _mediator.Send(new RegisterDispatcherCommand(request));
                return result.Item1 ? Ok() : BadRequest(result.Item2);
            }
            catch (Exception ex)
            {
                _logger.LogError("Registration error: {@ex}", ex);
                return BadRequest("Unknown error");
            }
        }


        [HttpPost("driver/login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AuthInformationResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DriverLoginAsync(AuthRequest request)
        {
            try
            {
                var result = await _mediator.Send(new LoginCommand(request, ApplicationRoles.Driver));

                return result.Item1 ? Ok(result.Item2) : BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError("Authentication error: {@ex}", ex);
                return BadRequest();
            }
        }

        [HttpPost("dispatcher/login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AuthInformationResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DispatcherLoginAsync(AuthRequest request)
        {
            try
            {
                var result = await _mediator.Send(new LoginCommand(request, ApplicationRoles.Dispatcher));

                return result.Item1 ? Ok(result.Item2) : BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError("Authentication error: {@ex}", ex);
                return BadRequest();
            }
        }

        [HttpPost("customer/login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AuthInformationResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CustomerLoginAsync(AuthRequest request)
        {
            try
            {
                var result = await _mediator.Send(new LoginCommand(request, ApplicationRoles.Customer));

                return result.Item1 ? Ok(result.Item2) : BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError("Authentication error: {@ex}", ex);
                return BadRequest();
            }
        } 

        [HttpGet("auth/phone")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CheckPhoneAvailabilityAsync([FromHeader]string phone)
        {
            return await _mediator.Send(new CheckPhoneNumberAvailabilityCommand(phone))
                ? Ok() 
                : BadRequest();
        }

        [HttpGet("auth/claims")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserClaims))]
        public IActionResult GetUserClaims()
        {
            var phone = User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var id = User.Claims.Single(c => c.Type == ClaimTypes.Name).Value;
            return Ok(new UserClaims
            {
                Phone = phone,
                Id = Guid.Parse(id)
            });
        }
    }
}
