﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Taxi123.WebGateway.Auth
{
    public class ApplicationUser
        : IdentityUser<Guid>
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDate { get; set; }
        public string DriverLicense { get; set; }
        public string Passport { get; set; }
        public bool? CanWork { get; set; }
        public byte[] Photo { get; set; }
    }
}
