﻿using System.Collections.Generic;

namespace Taxi123.WebGateway.Auth
{
    public static class ApplicationRoles
    {
        public const string Customer = "customer";
        public const string Driver = "driver";
        public const string Dispatcher = "dispatcher";
        public static IEnumerable<string> Roles => new[] 
            {
                Customer,
                Driver,
                Dispatcher
            };
    }
}
