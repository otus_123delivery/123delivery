using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Taxi123.WebGateway.Extensions;

namespace Taxi123.WebGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .Build()
                .MigrateDatabase()
                .ApplyRequiredAuthDbData()
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
