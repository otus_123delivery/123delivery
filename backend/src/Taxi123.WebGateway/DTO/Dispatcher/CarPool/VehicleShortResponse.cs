﻿using System;

namespace Taxi123.WebGateway.DTO.Dispatcher.CarPool
{
    public class VehicleShortResponse
    {
        public Guid VehicleId { get; set; }
        public string LicensePlate { get; set; }
    }
}
