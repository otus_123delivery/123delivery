﻿using System;

namespace Taxi123.WebGateway.DTO.Dispatcher
{
    public class DispatcherUserProfile
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDate { get; set; }
        public byte[] Photo { get; set; }
    }
}
