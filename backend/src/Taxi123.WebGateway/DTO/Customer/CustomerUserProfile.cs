﻿namespace Taxi123.WebGateway.DTO.Customer
{
    public class CustomerUserProfile
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
    }
}
