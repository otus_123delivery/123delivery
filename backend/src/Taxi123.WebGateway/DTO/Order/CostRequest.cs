﻿namespace Taxi123.WebGateway.DTO.Order
{
    public class CostRequest
    {
        public double FromLatitude { get; set; }
        public double FromLongitude { get; set; }

        public double ToLongitude { get; set; }
        public double ToLatitude { get; set; }
    }

}
