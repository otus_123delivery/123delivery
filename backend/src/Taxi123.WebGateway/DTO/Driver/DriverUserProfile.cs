﻿using System;

namespace Taxi123.WebGateway.DTO.Driver
{
    public class DriverUserProfile
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }

        public Guid? VehicleId { get; set; }
        public string DriverLicense { get; set; }
        public string Passport { get; set; }
        public byte[] Photo { get; set; }
        public bool CanWork { get; set; }
    }
}
