﻿using System;

namespace Taxi123.WebGateway.DTO.Driver
{
    public class DriverShortResponse
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public Guid? VehicleId { get; set; }
        public bool CanWork { get; set; }
    }
}
