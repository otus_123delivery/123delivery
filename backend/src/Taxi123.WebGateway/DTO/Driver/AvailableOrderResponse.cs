﻿using System;

namespace Taxi123.WebGateway.DTO.Driver
{
    public class AvailableOrderResponse
    {
        public Guid OrderId { get; set; }

        public string CurrentState { get; set; }

        public int Cost { get; set; }
        public int CustomerCarTypePreference { get; set; }

        public double FromLatitude { get; set; }
        public double FromLongitude { get; set; }
        public double ToLatitude { get; set; }
        public double ToLongitude { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? PickedDate { get; set; }
        public DateTime? PerformingStartedDate { get; set; }
        public DateTime? ArrivingDate { get; set; }
    }
}
