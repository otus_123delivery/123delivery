namespace Taxi123.WebGateway.DTO.Driver
{
    public class DriverOrderActionResponse
    {
        public DriverOrderActionResponse(bool result)
        {
            Result = result;
        }

        public bool Result { get; }
    }
}