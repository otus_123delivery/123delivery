﻿using System;

namespace Taxi123.WebGateway.DTO.Driver
{
    public class AvailableOrderShortResponse
    {
        public Guid OrderId { get; set; }

        public int Cost { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
