﻿namespace Taxi123.WebGateway.DTO.Auth
{
    public class AuthRequest
    {
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}
