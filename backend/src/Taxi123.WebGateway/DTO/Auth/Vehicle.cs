﻿namespace Taxi123.WebGateway.DTO.Auth
{
    public class Vehicle
    {
        public string Photo { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string LicensePlate { get; set; }
        public int VehicleClass { get; set; }
    }
}
