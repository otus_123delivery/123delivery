﻿using System;

namespace Taxi123.WebGateway.DTO.Auth
{
    public class AuthInformationResponse
    {
        public string Jwt { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Role { get; set; }
    }
}
