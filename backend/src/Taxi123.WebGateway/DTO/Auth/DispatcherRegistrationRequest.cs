﻿using System;

namespace Taxi123.WebGateway.DTO.Auth
{
    public class DispatcherRegistrationRequest
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDate { get; set; }
        public string Phone { get; set; }
        public string Photo { get; set; }
        public string Password { get; set; }
    }
}
