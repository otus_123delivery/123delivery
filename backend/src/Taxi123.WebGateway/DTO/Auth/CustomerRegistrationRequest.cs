﻿namespace Taxi123.WebGateway.DTO.Auth
{
    public class CustomerRegistrationRequest
    {
        public string Phone { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
    }
}
