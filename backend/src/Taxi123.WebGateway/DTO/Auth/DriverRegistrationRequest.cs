﻿using System;

namespace Taxi123.WebGateway.DTO.Auth
{
    public class DriverRegistrationRequest
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDate { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }

        public string Passport { get; set; }
        public string DriverLicense { get; set; }
        public string Photo { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}
