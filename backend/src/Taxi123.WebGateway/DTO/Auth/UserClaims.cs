﻿using System;

namespace Taxi123.WebGateway.DTO.Auth
{
    public class UserClaims
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
    }
}
