﻿using System;

namespace Taxi123.WebGateway.DTO.Tariff
{
    public class TariffLookupRequest
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}
