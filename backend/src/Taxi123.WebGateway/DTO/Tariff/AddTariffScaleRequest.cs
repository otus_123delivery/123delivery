﻿using System;

namespace Taxi123.WebGateway.DTO.Tariff
{
    public class AddTariffScaleRequest
    {
        public decimal MinimalCost { get; set; }
        public decimal MinimalPaidDistance { get; set; }
        public decimal BaseRate { get; set; }
        public decimal DayRate { get; set; }
        public decimal NightRate { get; set; }
        public decimal TrafficRate { get; set; }
        public decimal DriversCountRate { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
