﻿using System;

namespace Taxi123.WebGateway.DTO.Tariff
{
    public class UpdateTariffScaleRequest
    {
        public Guid Id { get; set; }
        public decimal MinimalCost { get; set; }
        public decimal MinimalPaidDistance { get; set; }
        public decimal BaseRate { get; set; }
        public decimal DayRate { get; set; }
        public decimal NightRate { get; set; }
        public decimal TrafficRate { get; set; }
        public decimal DriversCountRate { get; set; }
    }
}
