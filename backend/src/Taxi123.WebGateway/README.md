Для локальной отладки требуются: rabbitmq, mssql/azure-sql-edge.

```docker run -d -p "5672:5672" --name rabbit rabbitmq:3-management```

```docker run -d -p "1433:1433" --name azure-sql-edge -e ACCEPT_EULA="Y" -e MSSQL_SA_PASSWORD="Secret_pas23sword123" mcr.microsoft.com/azure-sql-edge```