using HealthChecks.UI.Client;
using MassTransit;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Taxi123.SignalR.Abstraction;
using Taxi123.SignalR.Constants;
using Taxi123.SignalR.Hubs;
using Taxi123.SignalR.Infrastructure.EventBus;
using Taxi123.SignalR.Middlewares;
using Taxi123.SignalR.Services;

namespace Taxi123.SignalR
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private const string DevCorsPolicy = "DevCorsPolicy";

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddRabbitMQ(rabbitConnectionString: $"amqp://{Configuration["Rabbit:Host"]}");

            services.AddSignalR();
            services.AddCors(options =>
            {
                options.AddPolicy(name: DevCorsPolicy,
                                  builder =>
                                  {
                                      builder.WithOrigins(
                                          "http://127.0.0.1:4200",
                                          "http://127.0.0.1:3000",
                                          "http://127.0.0.1",
                                          "http://www.123-taxi.xyz",
                                          "http://123-taxi.xyz",
                                          "https://www.123-taxi.xyz",
                                          "https://123-taxi.xyz")
                                        .AllowCredentials()
                                        .AllowAnyHeader()
                                        .AllowAnyMethod();
                                  });
            });

            services.AddSingleton<IClientsStoreService, InMemoryClientsStoreService>();
            services.AddControllers();

            services.AddAuthentication(options => options.DefaultScheme = CustomAuthSchemes.GatewayJwtTokenScheme)
                .AddScheme<AuthenticationSchemeOptions, GatewayJWTAuthenticationHandler>(CustomAuthSchemes.GatewayJwtTokenScheme, 
                    _ => { });

            services.AddHttpClient();
            services.AddSingleton<IUserPopUpNotificationService, UserPopUpNotificationService>();

            services.AddMassTransit(cfg =>
            {
                cfg.AddConsumer<SimpleNotificationConsumer>();
                cfg.AddConsumer<OrderFinalizedConsumer>();
                cfg.AddConsumer<OrderStateChangedConsumer>();

                cfg.UsingRabbitMq((context, mqcfg) =>
                {
                    mqcfg.Host(Configuration["Rabbit:Host"]);
                    mqcfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseCors(DevCorsPolicy);
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapHub<SimpleNotificationHub>("/notifications");
            });
        }
    }
}
