﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Taxi123.SignalR.Abstraction;

namespace Taxi123.SignalR.Hubs
{
    [Authorize]
    public class SimpleNotificationHub : Hub
    {
        private readonly IClientsStoreService _clientsService;

        public SimpleNotificationHub(IClientsStoreService clientsService) => _clientsService = clientsService;

        public override Task OnConnectedAsync()
        {
            GetClaims(out _, out Guid id);
            _clientsService.Clients.AddOrUpdate(id, Context.ConnectionId, (_, _) => Context.ConnectionId);
            return Task.CompletedTask;
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            GetClaims(out _, out Guid id);
            if (_clientsService.Clients.TryGetValue(id, out _))
            {
                _clientsService.Clients.TryRemove(id, out _);
            }

            return Task.CompletedTask;
        }

        private void GetClaims(out string phone, out Guid id)
        {
            phone = Context.User.Claims.Single(c => c.Type == ClaimTypes.MobilePhone).Value;
            id = Guid.Parse(Context.User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);
        }
    }
}
