﻿using System;
using System.Collections.Concurrent;
using Taxi123.SignalR.Abstraction;

namespace Taxi123.SignalR.Services
{
    public class InMemoryClientsStoreService : IClientsStoreService
    {
        public InMemoryClientsStoreService() 
            => Clients = new ConcurrentDictionary<Guid, string>();

        public ConcurrentDictionary<Guid, string> Clients { get; }
    }
}
