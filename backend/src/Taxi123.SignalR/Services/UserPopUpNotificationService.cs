﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;
using Taxi123.SignalR.Abstraction;
using Taxi123.SignalR.Constants;
using Taxi123.SignalR.Hubs;

namespace Taxi123.SignalR.Services
{
    public class UserPopUpNotificationService : IUserPopUpNotificationService
    {
        private readonly IHubContext<SimpleNotificationHub> _testHub;
        private readonly IClientsStoreService _clientsService;

        public UserPopUpNotificationService(IHubContext<SimpleNotificationHub> testHub,
            IClientsStoreService clientsService)
        {
            _testHub = testHub;
            _clientsService = clientsService;
        }

        public async Task<bool> SendOrderStateChangedAsync(Guid userId)
        {
            if (_clientsService.Clients.TryGetValue(userId, out string connectionId))
            {
                await _testHub.Clients.Client(connectionId).SendAsync(SignalRMethods.OrderStateChanged);
                return true;
            }

            return false;
        }

        public async Task<bool> SendPopUpToUserAsync(Guid userId, string message)
        {
            if (_clientsService.Clients.TryGetValue(userId, out string connectionId))
            {
                await _testHub.Clients.Client(connectionId).SendAsync(SignalRMethods.SimpleNotification, message);
                return true;
            }

            return false;
        }
    }
}
