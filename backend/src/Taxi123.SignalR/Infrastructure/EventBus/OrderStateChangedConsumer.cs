﻿using MassTransit;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Taxi123.EventBus.Contracts.Order;
using Taxi123.SignalR.Abstraction;

namespace Taxi123.SignalR.Infrastructure.EventBus
{
    public class OrderStateChangedConsumer : IConsumer<IOrderStateChanged>
    {
        private readonly IUserPopUpNotificationService _userPopUpNotification;
        private readonly ILogger<OrderStateChangedConsumer> _logger;

        public OrderStateChangedConsumer(IUserPopUpNotificationService userPopUpNotification,
            ILogger<OrderStateChangedConsumer> logger)
        {
            _userPopUpNotification = userPopUpNotification;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IOrderStateChanged> context)
        {
            if (context.Message.DriverId.HasValue)
            {
                if (!await _userPopUpNotification.SendOrderStateChangedAsync(context.Message.DriverId.Value))
                {
                    _logger.LogInformation($"Can't notify the driver: {context.Message.DriverId.Value} about an order state-changing via signalr.");
                }
            }

            if (!await _userPopUpNotification.SendOrderStateChangedAsync(context.Message.CustomerId))
            {
                _logger.LogInformation($"Can't notify the customer: {context.Message.CustomerId} about an order state-changing via signalr.");
            }
        }
    }
}
