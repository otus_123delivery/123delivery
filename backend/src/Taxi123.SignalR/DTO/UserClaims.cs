﻿using System;

namespace Taxi123.SignalR.DTO
{
    public class UserClaims
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
    }
}
