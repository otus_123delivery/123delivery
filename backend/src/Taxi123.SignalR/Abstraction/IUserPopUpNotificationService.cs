﻿using System;
using System.Threading.Tasks;

namespace Taxi123.SignalR.Abstraction
{
    public interface IUserPopUpNotificationService
    {
        Task<bool> SendPopUpToUserAsync(Guid userId, string message);
        Task<bool> SendOrderStateChangedAsync(Guid userId);
    }
}
