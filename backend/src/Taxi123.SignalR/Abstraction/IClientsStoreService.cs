﻿using System;
using System.Collections.Concurrent;

namespace Taxi123.SignalR.Abstraction
{
    public interface IClientsStoreService
    {
        ConcurrentDictionary<Guid, string> Clients { get; }
    }
}
