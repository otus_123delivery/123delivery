﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Taxi123.SignalR.DTO;

namespace Taxi123.SignalR.Middlewares
{
    public class GatewayJWTAuthenticationHandler
        : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly HttpClient _httpClient;

        public GatewayJWTAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            HttpClient httpClient)
            : base(options, logger, encoder, clock) => _httpClient = httpClient;

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string token;

            if (Request.Headers.ContainsKey("Authorization"))
            {
                token = Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
            }
            else if (Request.Query["access_token"].ToArray().Any())
            {
                token = Request.Query["access_token"].First();
            }
            else
            {
                return AuthenticateResult.Fail("JWT token not found.");
            }

            var claims = await ValidateTokenAsync(token);

            if (claims == null) return AuthenticateResult.Fail("Can't get user claims");

            var claimsIdentity = new ClaimsIdentity(claims, Scheme.Name);
            var ticket = new AuthenticationTicket(new ClaimsPrincipal(claimsIdentity), Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }

        private async Task<Claim[]> ValidateTokenAsync(string token)
        {
            _httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
            var response = await _httpClient.GetAsync("http://webgateway/api/v1/auth/claims");
            if (response.IsSuccessStatusCode)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                var responseModel = JsonConvert.DeserializeObject<UserClaims>(responseData);
                return new[] {
                    new Claim(ClaimTypes.NameIdentifier, responseModel.Id.ToString()),
                    new Claim(ClaimTypes.MobilePhone, responseModel.Phone) };
            }

            return null;
        }
    }
}
